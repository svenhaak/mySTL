#include "cstring.h"

namespace mstd
{
	char* strcpy(char* dest, const char* src)
	{
		char* ret = dest;
		while (*src != '\0')
		{
			*dest = *src;
			++src; ++dest;
		}
		*dest = '\0';
		return ret;
	}

	char* strncpy(char* dest, const char* src, std::size_t count)
	{
		std::size_t i = 0;
		for (; i < count && src[i] != '\0'; ++i)
		{
			dest[i] = src[i];
		}
		for (; i < count; ++i)
		{
			dest[i] = '\0';
		}
		return dest;
	}

	char* strcat(char* dest, const char* src)
	{
		mstd::strcpy(dest + mstd::strlen(dest), src);
		return dest;
	}

	char* strncat(char* dest, const char* src, std::size_t count)
	{
		mstd::strncpy(dest + mstd::strlen(dest), src, count);
		return dest;
	}

	std::size_t strlen(const char* str)
	{
		std::size_t length = 0;
		while (str[length] != '\0')
			++length;
		return length;
	}

	int strcmp(const char* lhs, const char* rhs)
	{
		while (*lhs != '\0' && *rhs != '\0')
		{
			if (*lhs < *rhs)
				return -1;
			if (*lhs > *rhs)
				return +1;
			++lhs; ++rhs;
		}
		std::size_t lhs_len = mstd::strlen(lhs);
		std::size_t rhs_len = mstd::strlen(rhs);

		if (lhs_len < rhs_len)
			return -1;
		else if (lhs_len > rhs_len)
			return +1;
		else
			return 0;
	}

	int strncmp(const char* lhs, const char* rhs, std::size_t count)
	{
		if (count == 0)
			return 0;
		while (*lhs != '\0' && *rhs != '\0')
		{
			if (*lhs < *rhs)
				return -1;
			if (*lhs > *rhs)
				return +1;
			if (--count == 0)
				return 0;

			++lhs; ++rhs;
		}

		std::size_t lhs_len = mstd::strlen(lhs);
		std::size_t rhs_len = mstd::strlen(rhs);

		if (lhs_len < rhs_len)
			return -1;
		else if (lhs_len > rhs_len)
			return +1;
		else
			return 0;
	}

	const char* strchr(const char* str, int ch)
	{
		while (*str != '\0')
		{
			if (*str == static_cast<char>(ch))
				return str;
			++str;
		}
		return static_cast<char>(ch) == '\0' ? str : nullptr;
	}

	char* strchr(char* str, int ch)
	{
		return const_cast<char*>(mstd::strchr(const_cast<const char*>(str), ch));
	}

	const char* strrchr(const char* str, int ch)
	{
		const char* result = str + mstd::strlen(str);

		while (result != str)
		{
			if (*result == static_cast<char>(ch))
				return result;
			--result;
		}
		return nullptr;
	}

	char* strrchr(char* str, int ch)
	{
		return const_cast<char*>(mstd::strrchr(const_cast<const char*>(str), ch));
	}

	namespace detail
	{
		bool is_in(char ch, const char* src)
		{
			while (*src != '\0')
			{
				if (*src == ch)
					return true;
				++src;
			}
			return false;
		}
	}

	std::size_t strspn(const char* dest, const char* src)
	{
		std::size_t result = 0;

		while (*src != '\0' && detail::is_in(*dest, src))
		{
			++result; ++dest;
		}
		return result;
	}

	std::size_t strcspn(const char* dest, const char* src)
	{
		std::size_t result = 0;

		while (*src != '\0' && !detail::is_in(*dest, src))
		{
			++result; ++dest;
		}
		return result;
	}

	const char* strpbrk(const char* dest, const char* breakset)
	{
		while (*dest != '\0')
		{
			if (detail::is_in(*dest, breakset))
				return dest;
			++dest;
		}
		return nullptr;
	}

	char* strpbrk(char* dest, const char* breakset)
	{
		return const_cast<char*>(mstd::strpbrk(const_cast<const char*>(dest), breakset));
	}

	namespace detail
	{
		bool streq(const char* lhs, const char* rhs, std::size_t len)
		{
			while (len > 0)
			{
				if (*lhs != *rhs)
					return false;
				++lhs; ++rhs; --len;
			}
			/*for (; len > 0; --len)
			{
				if (lhs[len - 1] != rhs[len - 1])
					return false;
			}*/
			return true;
		}
	}

	const char* strstr(const char* str, const char* target)
	{
		const std::size_t target_len = mstd::strlen(target);
		if (target_len == 0)
			return str;

		const std::size_t str_len = mstd::strlen(str);

		const char* const end = str + str_len - target_len;

		for (; str < end; ++str)
		{
			if (detail::streq(str, target, target_len))
				return str;
		}
		return nullptr;
	}

	char* strtok(char* str, const char* delim)
	{
		static char* last_str = nullptr;

		if (str == nullptr)
			str = last_str;

		if (str == nullptr)
			return nullptr;

		while (*str != '\0' && detail::is_in(*str, delim))
		{
			++str;
		}

		if (*str == '\0')
			return nullptr;

		char* token_end = str + 1;

		while (*token_end != '\0' && !detail::is_in(*token_end, delim))
		{
			++token_end;
		}

		if (*token_end != '\0')
		{
			*token_end = '\0';
			last_str = token_end + 1;
		}
		else
			last_str = nullptr;

		return str;
	}

	const void* memchr(const void* ptr, int ch, std::size_t count)
	{
		for (; count > 0; --count)
		{
			if (static_cast<unsigned char>(ch) == *static_cast<const unsigned char*>(ptr))
				return ptr;
			ptr = static_cast<const unsigned char*>(ptr) + 1;
		}
		return nullptr;
	}

	void* memchr(void* ptr, int ch, std::size_t count)
	{
		return const_cast<void*>(mstd::memchr(const_cast<const void*>(ptr), ch, count));
	}

	int memcmp(const void* lhs, const void* rhs, std::size_t count)
	{
		for (; count > 0; --count)
		{
			if (*static_cast<const unsigned char*>(lhs) != *static_cast<const unsigned char*>(rhs))
				return (*static_cast<const unsigned char*>(lhs) < *static_cast<const unsigned char*>(rhs))
				? -1 : +1;
			lhs = static_cast<const unsigned char*>(lhs) + 1;
			rhs = static_cast<const unsigned char*>(rhs) + 1;
		}
		return 0;
	}

	void* memset(void* dest, int ch, std::size_t count)
	{
		void* const result = dest;
		for (; count > 0; --count)
		{
			*static_cast<unsigned char*>(dest) = static_cast<unsigned char>(ch);
			dest = static_cast<unsigned char*>(dest) + 1;
		}
		return result;
	}

	void* memcpy(void* dest, const void* src, std::size_t count)
	{
		for (std::size_t i = 0; i < count; ++i)
		{
			*(static_cast<unsigned char*>(dest) + i) = *(static_cast<const unsigned char*>(src) + i);
		}
		return dest;
	}

	void* memmove(void* dest, const void* src, std::size_t count)
	{
		if (src < dest) //copy from the end
		{
			while (count > 0)
			{
				*(static_cast<unsigned char*>(dest) + count - 1) = *(static_cast<const unsigned char*>(src) + count - 1);
				--count;
			}
		}
		else if (src > dest) //copy from the begin
		{
			mstd::memcpy(dest, src, count);
		}
		return dest;
	}
}
