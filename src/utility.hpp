#pragma once

#include <cstddef>
#include "type_traits.hpp"
#include "utility/move_and_forward.hpp"
#include "utility/swap_and_exchange.hpp"

namespace mstd
{
	template <class T, T... Ints>
	struct integer_sequence
	{
		using value_type = T;

		static constexpr std::size_t size() noexcept
		{
			return sizeof...(Ints);
		}
	};

	namespace detail
	{
		template <bool failure, bool zero, class constant, class sequence>
		struct make_seq
		{
			static_assert(!failure, "N in make_integer_sequence should not be negative or T no integral type!!");
		};

		template <class T, T... Ns>
		struct make_seq<false, true, integral_constant<T, 0>, integer_sequence<T, Ns...>>
			: integer_sequence<T, Ns...>
		{
		};

		template <class T, T Index, T... Ns>
		struct make_seq<false, false, integral_constant<T, Index>, integer_sequence<T, Ns...>>
			: make_seq<false, Index == 1, integral_constant<T, Index - 1>, integer_sequence<T, Index - 1, Ns...>>
		{
		};
	}

	template <class T, T n>
	using make_integer_sequence = detail::make_seq < !is_integral<T>::value && n < 0, n == 0, integral_constant<T, n>, integer_sequence<T>>;

	template<std::size_t... Ints>
	using index_sequence = integer_sequence<std::size_t, Ints...>;

	template <std::size_t N>
	using make_index_sequence = make_integer_sequence<std::size_t, N>;

	template <class... T>
	using index_sequence_for = make_index_sequence<sizeof...(T)>;

	struct piecewise_construct_t
	{
		constexpr piecewise_construct_t() noexcept = default;
	};

	constexpr piecewise_construct_t piecewise_construct{};

	template <class...>
	class tuple;

	template <class T1, class T2>
	class pair
	{
	public:
		using first_type = T1;
		using second_type = T2;

		first_type first;
		second_type second;

		constexpr pair() noexcept : first(), second()
		{
		}

		template<class U, class V>
		pair(const pair<U, V>& pr)
			: first(pr.first), second(pr.second)
		{
		}

		template<class U, class V>
		pair(pair<U, V>&& pr)
			: first(mstd::move(pr.first)), second(mstd::move(pr.second))
		{
		}

		pair(const pair& pr) = default;

		pair(pair&& pr) = default;

		pair(const first_type& a, const second_type& b)
			: first(a), second(b)
		{
		}

		template<class U, class V,
		class = enable_if_t < is_convertible<const U&, first_type>::value
			&& is_convertible<const V&, second_type>::value, void >>
			pair(U&& a, V&& b)
			: first(mstd::forward<U>(a)), second(mstd::forward<V>(b))
		{
		}

	private:
		template <class... A1, class... A2, std::size_t... I1, std::size_t... I2>
		pair(tuple<A1...>& a1, tuple<A2...>& a2, index_sequence<I1...>, index_sequence<I2...>);

	public:
		template <class... Args1, class... Args2>
		pair(piecewise_construct_t, tuple<Args1...> first_args, tuple<Args2...> second_args); //defined in tuple.hpp

		pair& operator= (const pair& pr)
		{
			if (this != &pr)
			{
				first = pr.first;
				second = pr.second;
			}
			return *this;
		}


		template <class U, class V>
		pair& operator=(const pair<U, V>& pr)
		{
			first = pr.first;
			second = pr.second;
			return *this;
		}

		pair& operator=(pair&& pr)
			noexcept (is_nothrow_move_assignable<first_type>::value &&
				is_nothrow_move_assignable<second_type>::value)
		{
			if (this != &pr)
			{
				first = mstd::move(pr.first);
				second = mstd::move(pr.second);
			}
			return *this;
		}

		template <class U, class V>
		pair& operator=(pair<U, V>&& pr)
		{
			first = mstd::move(pr.first);
			second = mstd::move(pr.second);
			return *this;
		}

		void swap(pair& pr) noexcept (noexcept(swap(first, pr.first)) &&
			noexcept(swap(second, pr.second)))
		{
			swap(first, pr.first);
			swap(second, pr.second);
		}
	};

	template <class T1, class T2>
	bool operator==(const pair<T1, T2>& lhs, const pair<T1, T2>& rhs)
	{
		return lhs.first == rhs.first && lhs.second == rhs.second;
	}

	template <class T1, class T2>
	bool operator!=(const pair<T1, T2>& lhs, const pair<T1, T2>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class T1, class T2>
	bool operator<(const pair<T1, T2>& lhs, const pair<T1, T2>& rhs)
	{
		return lhs.first < rhs.first || (!(rhs.first < lhs.first) && lhs.second < rhs.second);
	}

	template <class T1, class T2>
	bool operator<=(const pair<T1, T2>& lhs, const pair<T1, T2>& rhs)
	{
		return !(rhs<lhs);
	}

	template <class T1, class T2>
	bool operator>(const pair<T1, T2>& lhs, const pair<T1, T2>& rhs)
	{
		return rhs < lhs;
	}

	template <class T1, class T2>
	bool operator>=(const pair<T1, T2>& lhs, const pair<T1, T2>& rhs)
	{
		return !(lhs < rhs);
	}

	//todo get

	template <class T1, class T2>
	void swap(pair<T1, T2>& lhs, pair<T1, T2>& rhs) noexcept(noexcept(lhs.swap(rhs)))
	{
		lhs.swap(rhs);
	}

	template <class T1, class T2>
	pair<detail::remove_ref_wrap_t<T1>, detail::remove_ref_wrap_t<T2>>
		make_pair(T1&& x, T2&& y)
	{
		using V1 = detail::remove_ref_wrap_t<T1>;
		using V2 = detail::remove_ref_wrap_t<T2>;

		return pair<V1, V2>{ mstd::forward<T1>(x), mstd::forward<T2>(y) };
	}

	template <class Tpl>
	struct tuple_size;

	template <class T1, class T2>
	struct tuple_size <pair<T1, T2>> : integral_constant<std::size_t, 2>
	{
	};

	template <std::size_t I, class Tpl>
	struct tuple_element;

	template <std::size_t I, class Tpl>
	using tuple_element_t = typename tuple_element<I, Tpl>::type;

	template <class T1, class T2>
	struct tuple_element <0, pair<T1, T2>>
	{
		using type = T1;
	};

	template <class T1, class T2>
	struct tuple_element <1, pair<T1, T2>>
	{
		using type = T2;
	};

	namespace detail
	{
		template <class T1, class T2>
		T1& pair_get(pair<T1, T2>& p, integral_constant<std::size_t, 0>) noexcept
		{
			return p.first;
		}

		template  <class T1, class T2>
		T2& pair_get(pair<T1, T2>& p, integral_constant<std::size_t, 1>) noexcept
		{
			return p.second;
		}

		template <class T1, class T2>
		const T1& pair_get(const pair<T1, T2>& p, integral_constant<std::size_t, 0>) noexcept
		{
			return p.first;
		}

		template  <class T1, class T2>
		const T2& pair_get(const pair<T1, T2>& p, integral_constant<std::size_t, 1>) noexcept
		{
			return p.second;
		}

		template <class T1, class T2>
		T1&& pair_get(pair<T1, T2>&& p, integral_constant<std::size_t, 0>) noexcept
		{
			return mstd::move(p.first);
		}

		template  <class T1, class T2>
		T2&& pair_get(pair<T1, T2>&& p, integral_constant<std::size_t, 1>) noexcept
		{
			return mstd::move(p.second);
		}
	}

	template <std::size_t I, class T1, class T2>
	constexpr tuple_element_t<I, pair<T1, T2> >& get(pair<T1, T2>& p) noexcept
	{
		return detail::pair_get(p, integral_constant<std::size_t, I>{});
	}

	template <std::size_t I, class T1, class T2 >
	constexpr const tuple_element_t<I, pair<T1, T2>>& get(const pair<T1, T2>& p) noexcept
	{
		return detail::pair_get(p, integral_constant<std::size_t, I>{});
	}

	template <std::size_t I, class T1, class T2>
	constexpr tuple_element_t<I, pair<T1, T2> >&& get(pair<T1, T2>&& p) noexcept
	{
		return detail::pair_get(mstd::move(p), integral_constant<std::size_t, I>{});
	}
}
