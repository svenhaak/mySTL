#pragma once

#include "../functional.hpp"
#include "../type_traits.hpp"

namespace mstd
{
	namespace experimental
	{
		template <class Func>
		auto not_fn(Func&& f)
		{
			return[F = mstd::forward<Func>(f)](auto&&... args)
			{
				return !mstd::invoke(F, mstd::forward<decltype(args)>(args)...);
			};
		}
	}
}
