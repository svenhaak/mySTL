#pragma once

#include "../memory.hpp"
#include "../type_traits.hpp"
#include "../utility.hpp"

namespace mstd
{
	namespace experimental
	{
		template <class T>
		class observer_ptr
		{
		public:
			using element_type = T;
			using pointer = add_pointer_t<T>;

			constexpr observer_ptr() noexcept
				: ptr_{ nullptr }
			{
			}

			constexpr observer_ptr(nullptr_t) noexcept
				: ptr_{ nullptr }
			{
			}

			explicit observer_ptr(pointer p) noexcept
				: ptr_{ p }
			{
			}

			template <class U>
			observer_ptr(observer_ptr<U> other) noexcept //TODO overload resolution
				: ptr_{ other.get() }
			{
			}

			constexpr pointer release() noexcept
			{
				return mstd::exchange(ptr_, nullptr);
			}

			constexpr void reset(pointer p = nullptr) noexcept
			{
				ptr_ = p;
			}

			/*constexpr*/ void swap(observer_ptr& other) noexcept //todo reason default const
			{
				mstd::swap(ptr_, other.ptr_);
			}

			constexpr pointer get() const noexcept
			{
				return ptr_;
			}

			constexpr explicit operator bool() const noexcept
			{
				return ptr_ != nullptr;
			}

			constexpr add_lvalue_reference_t<T> operator*() const noexcept
			{
				return *ptr_;
			}

			constexpr pointer operator->() const noexcept
			{
				return ptr_;
			}

			constexpr explicit operator pointer() const noexcept
			{
				return ptr_;
			}

		private:
			pointer ptr_;
		};

		template <class T>
		observer_ptr<T> make_observer(T* p) noexcept
		{
			return observer_ptr<T>{ p };
		}

		template <class T, class U>
		bool operator==(const observer_ptr<T>& p1, const observer_ptr<U>& p2)
		{
			return p1.get() == p2.get();
		}

		template <class T, class U>
		bool operator!=(const observer_ptr<T>& p1, const observer_ptr<U>& p2)
		{
			return !(p1 == p2);
		}

		template <class T>
		bool operator==(const observer_ptr<T>& p, nullptr_t) noexcept
		{
			return !p;
		}

		template <class T>
		bool operator==(nullptr_t, const observer_ptr<T>& p) noexcept
		{
			return !p;
		}

		template <class T>
		bool operator!=(const observer_ptr<T>& p, nullptr_t) noexcept
		{
			return p;
		}

		template <class T>
		bool operator!=(nullptr_t, const observer_ptr<T>& p) noexcept
		{
			return p;
		}

		template <class T, class U>
		bool operator<(const observer_ptr<T>& p1, const observer_ptr<U>& p2)
		{
			return less<common_type_t<T*, U*>>{}(p1.get(), p2.get());
		}

		template <class T, class U>
		bool operator>(const observer_ptr<T>& p1, const observer_ptr<U>& p2)
		{
			return p2 < p1;
		}

		template <class T, class U>
		bool operator<=(const observer_ptr<T>& p1, const observer_ptr<U>& p2)
		{
			return !(p2 < p1);
		}

		template <class T, class U>
		bool operator>=(const observer_ptr<T>& p1, const observer_ptr<U>& p2)
		{
			return !(p1 < p2);
		}

		template <class T>
		void swap(observer_ptr<T>& lhs, observer_ptr<T>& rhs) noexcept
		{
			lhs.swap(rhs);
		}

		//todo hash<observer<T>>
	}
}
