#pragma once

#include "../compressed_pair.hpp"

namespace mstd
{
	namespace experimental
	{
		template <class F>
		class scoped_exit
		{
		public:
			explicit scoped_exit(F&& f)
				: function_and_invoke_{ mstd::detail::construct_both, mstd::move(f), true }
			{
			}

			scoped_exit(const scoped_exit&) = delete;

			scoped_exit(scoped_exit&& other) noexcept
				: function_and_invoke_{ mstd::move(other.function_and_invoke_) }
			{
				other.release();
			}

			scoped_exit& operator=(const scoped_exit&) = delete;

			scoped_exit& operator=(scoped_exit&&) = delete;
			/*scoped_exit& operator=(scoped_exit&& other) noexcept
			{
			function_and_invoke_ = mstd::move(other.function_and_invoke_);
			other.release();
			}*/

			void release() noexcept
			{
				function_and_invoke_.second() = false;
			}

			~scoped_exit()
			{
				if (function_and_invoke_.second())
					mstd::invoke(function_and_invoke_.first());
			}

		private:
			mstd::detail::compressed_pair<F, bool> function_and_invoke_;
		};

		template <class F>
		scoped_exit<remove_reference_t<F>> make_scoped_exit(F&& f) noexcept
		{
			return scoped_exit<remove_reference_t<F>>{ mstd::forward<F>(f) };
		}

		template <class R, class D>
		class unique_resource
		{
		public:
			explicit unique_resource(R&& resource, D&& deleter, bool should_run = true) noexcept
				: resource_{ mstd::forward<R>(resource) },
				deleter_and_invoke_{ mstd::forward<D>(deleter), should_run }
			{
			}

			unique_resource(const unique_resource&) = delete;

			unique_resource(unique_resource&& other) noexcept
				: resource_{ mstd::move(other.resource_) },
				deleter_and_invoke_{ mstd::move(deleter_and_invoke_) }
			{
				other.release();
			}

			unique_resource& operator=(const unique_resource&) = delete;

			unique_resource& operator=(unique_resource&& other) noexcept
			{
				reset(mstd::move(other.resource_));
				other.release();
			}

			~unique_resource() noexcept(noexcept(this->reset()))
			{
				reset();
			}

			void reset()// noexcept(noexcept(mstd::invoke(deleter_and_invoke_.first(),resource_)))
			{
				if (deleter_and_invoke_.second())
				{
					mstd::invoke(deleter_and_invoke_.first(), resource_);
					deleter_and_invoke_.second() = false;
				}
			}

			void reset(R&& new_resource) noexcept(noexcept(this->reset()))
			{
				reset();
				resource_ = mstd::move(new_resource);
				deleter_and_invoke_.second() = true;
			}

			const R& release() noexcept
			{
				deleter_and_invoke_.second() = false;
				return resource_;
			}

			const R& get() const noexcept
			{
				return resource_;
			}

			operator const R&() const noexcept
			{
				return resource_;
			}

			//enable_if_t<is_pointer<R>::value &&
			//(is_class<remove_pointer_t<R>>::value || is_union<remove_pointer_t<R>>::value), R>
			R
				operator->() const noexcept
			{
				return resource_;
			}

			//enable_if_t<is_pointer<R>::value, add_lvalue_reference_t<remove_pointer_t<R>>>
			add_lvalue_reference_t<remove_pointer_t<R>>
				operator*() const noexcept
			{
				return *resource_;
			}

			const D& get_deleter() const noexcept
			{
				return deleter_and_invoke_.first();
			}

		private:
			R resource_;
			mstd::detail::compressed_pair<D, bool> deleter_and_invoke_;
		};

		template <class R, class D>
		unique_resource<R, remove_reference_t<D>> make_unique_resource(R&& r, D&& d) noexcept
		{
			return unique_resource<R, remove_reference_t<D>>{ mstd::move(r), mstd::forward<D>(d) };
		}

		template <class R, class D>
		unique_resource<R, D> make_unique_resource_checked(R r, R invalid, D d) noexcept
		{
			bool shoud_run = r != invalid;
			return unique_resource<R, D>{ mstd::move(r), mstd::move(d), shoud_run };
		}
	}
}
