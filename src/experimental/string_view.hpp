#pragma once

#if __cpluplus >= 201400L
#define CONSTEXPR_SINCE_CPP14 constexpr
#else
#define CONSTEXPR_SINCE_CPP14 
#endif

#include <string>
#include "../stdexcept.hpp"
#include "../iterator.hpp"
#include "../utility.hpp"
namespace mstd
{
	namespace experimental
	{
		template <class CharT, class Traits = std::char_traits<CharT>>
		class basic_string_view
		{
		public:
			using traits_type = Traits;
			using value_type = CharT;
			using pointer = value_type*;
			using const_pointer = const value_type*;
			using reference = value_type&;
			using const_reference = const value_type&;
			using const_iterator = const_pointer;
			using iterator = const_iterator;
			using const_reverse_iterator = reverse_iterator<const_iterator>;
			using reverse_iterator = const_reverse_iterator;
			using size_type = std::size_t;
			using difference_type = std::ptrdiff_t;

		public:
			static constexpr size_type npos = -1;

		public:
			constexpr basic_string_view() noexcept
				: data_{ nullptr }, size_{ 0 }
			{
			}

			constexpr basic_string_view(const basic_string_view& other) noexcept = default;

			template <class Allocator>
			basic_string_view(const std::basic_string<value_type, traits_type, Allocator>& str) noexcept
				: data_{ str.data() }, size_{ str.size() }
			{
			}

			constexpr basic_string_view(const_pointer s, size_type count)
				: data_{ s }, size_{ count }
			{
			}

			constexpr basic_string_view(const_pointer s)
				: data_{ s }, size_{ traits_type::length(s) }
			{
			}

			basic_string_view& operator=(const basic_string_view& other) noexcept = default;

			constexpr const_iterator begin() const noexcept
			{
				return cbegin();
			}

			constexpr const_iterator cbegin() const noexcept
			{
				return data_;
			}

			constexpr const_iterator end() const noexcept
			{
				return cend();
			}

			constexpr const_iterator cend() const noexcept
			{
				return data_ + size_;
			}

			constexpr const_reverse_iterator rbegin() const noexcept
			{
				return const_reverse_iterator{ cend() };
			}

			constexpr const_reverse_iterator rend() const noexcept
			{
				return const_reverse_iterator{ cbegin() };
			}

			constexpr const_reverse_iterator crbegin() const noexcept
			{
				return const_reverse_iterator{ cend() };
			}

			constexpr const_reverse_iterator crend() const noexcept
			{
				return const_reverse_iterator{ cbegin() };
			}

			constexpr const_reference operator[](size_type pos) const
			{
				return data_[pos];
			}

			constexpr const_reference at(size_type pos) const
			{
				if (pos >= size_)
					throw out_of_range{ "string_view::at" };

				return data_[pos];
			}

			constexpr const_reference front() const
			{
				return data_[0];
			}

			constexpr const_reference back() const
			{
				return data_[size_ - 1];
			}

			constexpr const_pointer data() const noexcept
			{
				return data_;
			}

			constexpr size_type size() const noexcept
			{
				return size_;
			}

			constexpr size_type length() const noexcept
			{
				return size_;
			}

			constexpr size_type max_size() const noexcept
			{
				return std::numeric_limits<size_type>::max();
			}

			constexpr bool empty() const noexcept
			{
				return size_ == 0;
			}

			constexpr void remove_prefix(size_type n)
			{
				data_ += n;
				size_ -= n;
			}

			constexpr void remove_suffix(size_type n)
			{
				size_ -= n;
			}

			constexpr void swap(basic_string_view& other)
			{
				mstd::swap(data_, other.data_);
				mstd::swap(size_, other.size_);
			}

			template <class Allocator = std::allocator<value_type>>
			std::basic_string<value_type, traits_type, Allocator>
				to_string(const Allocator& a) const
			{
				return{ begin(), end() };
			}

			template <class Allocator>
			explicit operator std::basic_string<value_type, traits_type, Allocator>()
			{
				return{ begin(), end() };
			}

			size_type copy(pointer dest, size_type count, size_type pos = 0) const
			{
				if (pos >= size())
					throw out_of_range{ "string_view::copy" };

				count = mstd::min(count, size() - pos);
				mstd::copy_n(begin() + pos, count);

				return count;
			}

			constexpr basic_string_view substr(size_type pos = 0, size_type count = npos) const
			{
				if (pos >= size())
					throw out_of_range{ "basic_string_view::substr" };

				count = mstd::min(count, size() - pos);
				return{ begin() + pos, count };
			}

			/*constexpr*/ int compare(basic_string_view v) const noexcept
			{
				int res = traits_type::compare(data_, v.data_, mstd::min(size_, v.size_));

				if (res == 0)
					res = (size_ == v.size_) ? 0 : ((size_ < v.size_) ? -1 : 1);

				return res;
			}

			/*constexpr*/ int compare(size_type pos1, size_type count1, basic_string_view v) const noexcept
			{
				return substr(pos1, count1).compare(v);
			}

			/*constexpr*/ int compare(size_type pos1, size_type count1,
				size_type pos2, size_type count2, basic_string_view v) const noexcept
			{
				return substr(pos1, count1).compare(v.substr(pos2, count2));
			}

			/*constexpr*/ int compare(const_pointer s) const noexcept
			{
				return compare(basic_string_view{ s });
			}

			/*constexpr*/ int compare(size_type pos1, size_type count1, const_pointer s) const noexcept
			{
				return substr(pos1, count1).compare(basic_string_view{ s });
			}

			/*constexpr*/ int compare(size_type pos1, size_type count1,
				const_pointer s, size_type count2) const noexcept
			{
				return substr(pos1, count1).compare(s, count2);
			}

			constexpr size_type find(basic_string_view v, size_type pos = 0) const; //TODO

			constexpr size_type find(CharT c, size_type pos = 0) const; //TODO

			constexpr size_type find(const CharT* s, size_type pos, size_type count) const; //TODO

			constexpr size_type find(const CharT* s, size_type pos = 0) const; //TODO


		private:
			const_pointer data_;
			size_type size_;
		};

		template <class CharT, class Traits>
		/*constexpr*/ bool operator==(basic_string_view<CharT, Traits> lhs,
			basic_string_view<CharT, Traits> rhs) noexcept
		{
			if (lhs.size() != rhs.size())
				return false;

			return lhs.compare(rhs) == 0;
		}

		template <class CharT, class Traits>
		/*constexpr*/ bool operator!=(basic_string_view<CharT, Traits> lhs,
			basic_string_view<CharT, Traits> rhs) noexcept
		{
			if (lhs.size() != rhs.size())
				return true;

			return lhs.compare(rhs) != 0;
		}

		template <class CharT, class Traits>
		/*constexpr*/ bool operator<(basic_string_view<CharT, Traits> lhs,
			basic_string_view<CharT, Traits> rhs) noexcept
		{
			return lhs.compare(rhs) < 0;
		}

		template <class CharT, class Traits>
		/*constexpr*/ bool operator<=(basic_string_view<CharT, Traits> lhs,
			basic_string_view<CharT, Traits> rhs) noexcept
		{
			return lhs.compare(rhs) <= 0;
		}

		template <class CharT, class Traits>
		/*constexpr*/ bool operator>(basic_string_view<CharT, Traits> lhs,
			basic_string_view<CharT, Traits> rhs) noexcept
		{
			return lhs.compare(rhs) > 0;
		}

		template <class CharT, class Traits>
		/*constexpr*/ bool operator>=(basic_string_view<CharT, Traits> lhs,
			basic_string_view<CharT, Traits> rhs) noexcept
		{
			return lhs.compare(rhs) >= 0;
		}

		template <class CharT, class Traits>
		std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& os,
			basic_string_view<CharT, Traits> v)
		{
			os << v.to_string(); //TODO?? ineffizient??
			return os;
		}

		using string_view = basic_string_view<char>;
		using wstring_view = basic_string_view<wchar_t>;
		using u16string_view = basic_string_view<char16_t>;
		using u32string_view = basic_string_view<char32_t>;
	}
}
