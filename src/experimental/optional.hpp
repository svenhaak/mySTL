#pragma once

#include <initializer_list>
#include "../stdexcept.hpp"
#include "../type_traits.hpp"
#include "../utility.hpp"

namespace mstd
{
	namespace experimental
	{
		struct nullopt_t
		{
			constexpr nullopt_t() noexcept = default;
		};

		constexpr nullopt_t nullopt{};

		struct in_place_t
		{
			constexpr in_place_t() noexcept = default;
		};

		constexpr in_place_t in_place{};

		struct bad_optional_access : public logic_error
		{
			using logic_error::logic_error;
		};

		namespace detail
		{
			template <class T, bool = mstd::is_trivially_destructible<T>::value>
			class optional_storage_t
			{
			protected:
				using value_type = T;
				union
				{
					char null_state_;
					value_type value_;
				};
				bool engaged_ = false;

				constexpr optional_storage_t() noexcept
					: null_state_(0)
				{
				}

				optional_storage_t(const optional_storage_t& other)
					: engaged_{ other.engaged_ }
				{
					if (engaged_)
						::new(mstd::addressof(value_)) value_type{ other.value_ };
				}

				optional_storage_t(optional_storage_t&& other) noexcept(is_nothrow_move_constructible<value_type>::value)
					: engaged_{ other.engaged_ }
				{
					if (engaged_)
						::new (mstd::addressof(value_)) value_type{ mstd::move(other.value_) };
				}

				constexpr optional_storage_t(const value_type& v)
					: value_{ v }, engaged_{ true }
				{
				}

				constexpr optional_storage_t(value_type&& v)
					: value_{ mstd::move(v) }, engaged_{ true }
				{
				}

				template <class... Args>
				constexpr explicit optional_storage_t(in_place_t, Args&&... args)
					: value_{ mstd::forward<Args>(args)... }, engaged_{ true }
				{
				}

				~optional_storage_t()
				{
					if (engaged_)
						value_.~value_type();
				}
			};

			template <class T>
			class optional_storage_t<T, true>
			{
			protected:
				using value_type = T;
				union
				{
					char null_state_;
					value_type value_;
				};
				bool engaged_ = false;

				constexpr optional_storage_t() noexcept
					: null_state_(0)
				{
				}

				optional_storage_t(const optional_storage_t& other)
					: engaged_{ other.engaged_ }
				{
					if (engaged_)
						::new(mstd::addressof(value_)) value_type{ other.value_ };
				}

				optional_storage_t(optional_storage_t&& other) noexcept(is_nothrow_move_constructible<value_type>::value)
					: engaged_{ other.engaged_ }
				{
					if (engaged_)
						::new (mstd::addressof(value_)) value_type{ mstd::move(other.value_) };
				}

				constexpr optional_storage_t(const value_type& v)
					: value_{ v }, engaged_{ true }
				{
				}

				constexpr optional_storage_t(value_type&& v)
					: value_{ mstd::move(v) }, engaged_{ true }
				{
				}

				template <class... Args>
				constexpr explicit optional_storage_t(in_place_t, Args&&... args)
					: value_{ mstd::forward<Args>(args)... }, engaged_{ true }
				{
				}
			};
		}

		template <class T>
		class optional : private detail::optional_storage_t<T>
		{
		public:
			using value_type = T;

			static_assert(!is_reference<value_type>::value, "Instatiation of optional with a reference type is ill-formed.");
			static_assert(!is_same<remove_cv_t<value_type>, nullopt_t>::value, "Instantiation of optional with a nullopt_t type is ill-formed.");
			static_assert(!is_same<remove_cv_t<value_type>, in_place_t>::value, "Instantiation of optional with a in_place_t type is ill-formed.");
			static_assert(is_object<value_type>::value, "Instantiation of optional with a non-object type is undefined behavior.");
			static_assert(is_nothrow_destructible<value_type>::value,
				"Instantiation of optional with an object type that is not noexcept destructible is undefined behavior.");

			constexpr optional()
			{
			}

			constexpr optional(nullopt_t)
			{
			}

			optional(const optional& other) = default;

			optional(optional&&) = default;

			optional(const value_type& value)
				: detail::optional_storage_t<T>(value)
			{
			}

			optional(value_type&& value)
				: detail::optional_storage_t<T>(mstd::move(value))  //TODO: test if braced-init list works again
			{
			}

			template <class... Args>
			constexpr explicit optional(in_place_t, Args&&... args)
				: detail::optional_storage_t<T>(in_place, mstd::forward<Args>(args)...)
			{
			}

			template <class U, class... Args,
			class = enable_if_t < is_constructible<T, std::initializer_list<U>&, Args&&...>::value >>
				constexpr explicit optional(in_place_t, std::initializer_list<U> ilist, Args&&... args)
				: detail::optional_storage_t<T>(in_place, ilist, mstd::forward<Args>(args)...)
			{
			}

			optional& operator=(nullopt_t) noexcept
			{
				if (this->engaged_)
				{
					this->value_.~value_type();
					this->engaged_ = false;
				}
				return *this;
			}

			optional& operator=(const optional& other)
			{
				if (this->engaged_ == other.engaged_)
				{
					if (this->engaged_)
					{
						this->value_ = other.value_;
					}
				}
				else
				{
					if (this->engaged_)
					{
						this->value_.~value_type();
						this->engaged_ = false;
					}
					else
					{
						::new (mstd::addressof(this->value_)) value_type{ other.value_ };
						this->engaged_ = other.engaged_;
					}
				}
				return *this;
			}

			optional& operator=(optional&& other)
				noexcept(is_nothrow_move_assignable<value_type>::value
					&& is_nothrow_move_constructible<value_type>::value)
			{
				if (this->engaged_ == other.engaged_)
				{
					if (this->engaged_)
					{
						this->value_ = mstd::move(other.value_);
					}
				}
				else
				{
					if (this->engaged_)
					{
						this->value_.~value_type();
						this->engaged_ = false;
					}
					else
					{
						::new (mstd::addressof(this->value_)) value_type{ mstd::move(other.value_) };
						this->engaged_ = other.engaged_;
					}
				}
				return *this;
			}

			template <class U, class = enable_if_t<is_same<decay_t<U>, T>::value>>
			optional& operator=(U&& value)
			{
				if (this->engaged_)
				{
					this->value_ = value;
				}
				else
				{
					::new (mstd::addressof(this->value_)) value_type{ mstd::forward<U>(value) };
				}
				return *this;
			}

			constexpr const value_type* operator->() const
			{
				return mstd::addressof(this->value_);
			}

			/*constexpr*/ value_type* operator->()
			{
				return mstd::addressof(this->value_);
			}

			constexpr const T& operator*() const &
			{
				return this->value_;
			}

			/*constexpr*/ T& operator*() &
			{
				return this->value_;
			}

			constexpr const T&& operator*() const &&
			{
				return mstd::move(this->value_);
			}

			/*constexpr*/ T&& operator*() &&
			{
				return mstd::move(this->value_);
			}

			constexpr explicit operator bool() const noexcept
			{
				return this->engaged_;
			}

			/*constexpr*/ T& value() &
			{
				return this->engaged_ ? this->value_ : throw bad_optional_access{ "optional::value" }, this->value_;
			}

			constexpr const T& value() const &
			{
				return this->engaged_ ? this->value_ : throw bad_optional_access{ "optional::value" }, this->value_;
			}

			/*constexpr*/ T&& value() &&
			{
				return this->engaged_ ? mstd::move(this->value_) : throw bad_optional_access{ "optional::value" }, this->value_;
			}

			constexpr const T&& value() const &&
			{
				return this->engaged_ ? mstd::move(this->value_) : throw bad_optional_access{ "optional::value" }, this->value_;
			}

			template <class U>
			constexpr value_type value_or(U&& default_value) const &
			{
				return this->engaged_ ? this->value_ : T{ mstd::forward<U>(default_value) };
			}

			template <class U>
			value_type value_or(U&& default_value) &&
			{
				return this->engaged_ ? mstd::move(this->value_) : T{ mstd::forward<U>(default_value) };
			}

			void swap(optional& other) noexcept(is_nothrow_move_constructible<T>::value
				&& noexcept(swap(declval<T&>(), declval<T&>())))
			{
				if (this->engaged_ == other.engaged_)
				{
					if (this->engaged_)
					{
						swap(this->value_, other.value_);
					}
				}
				else
				{
					if (this->engaged_)
					{
						::new (mstd::addressof(other.value_)) T{ mstd::move(this->value_) };
						this->value_.~value_type();
						swap(this->engaged_, other.engaged_);
					}
					else
					{
						::new (mstd::addressof(this->value_)) T{ mstd::move(other.value_) };
						other.value_.~value_type();
						swap(this->engaged_, other.engaged_);
					}
				}
			}

			template <class... Args>
			void emplace(Args&&... args)
			{
				if (this->engaged_)
					this->value_.~value_type();

				::new (mstd::addressof(this->value_)) value_type(mstd::forward<Args>(args)...); //direct-list-initalization not allowed
				this->engaged_ = true;
			}

			template <class U, class... Args>
			enable_if_t<is_constructible<T, std::initializer_list<U>&, Args&&...>::value>
				emplace(std::initializer_list<U> ilist, Args&&... args)
			{
				if (this->engaged_)
					this->value_.~value_type();

				::new (mstd::addressof(this->value_)) value_type(ilist, mstd::forward<Args>(args)...);
			}
		};

		template <class T>
		constexpr bool operator==(const optional<T>& lhs, const optional<T>& rhs)
		{
			if (static_cast<bool>(lhs) != static_cast<bool>(rhs))
				return false;

			if (!static_cast<bool>(lhs))
				return true;
			return *lhs == *rhs;
		}

		template <class T>
		constexpr bool operator!=(const optional<T>& lhs, const optional<T>& rhs)
		{
			return !(lhs == rhs);
		}

		template <class T>
		constexpr bool operator<(const optional<T>& lhs, const optional<T>& rhs)
		{
			if (!static_cast<bool>(lhs))
				return false;
			if (!static_cast<bool>(rhs))
				return true;
			return *lhs < *rhs;
		}

		template <class T>
		constexpr bool operator<=(const optional<T>& lhs, const optional<T>& rhs)
		{
			return !(rhs < lhs);
		}

		template <class T>
		constexpr bool operator>(const optional<T>& lhs, const optional<T>& rhs)
		{
			return rhs < lhs;
		}

		template <class T>
		constexpr bool operator>=(const optional<T>& lhs, const optional<T>& rhs)
		{
			return !(lhs < rhs);
		}

		template <class T>
		constexpr bool operator==(const optional<T>& lhs, nullopt_t) noexcept
		{
			return !static_cast<bool>(lhs);
		}

		template <class T>
		constexpr bool operator==(nullopt_t, const optional<T>& rhs) noexcept
		{
			return !static_cast<bool>(rhs);
		}

		template <class T>
		constexpr bool operator!=(const optional<T>& lhs, nullopt_t) noexcept
		{
			return static_cast<bool>(lhs);
		}

		template <class T>
		constexpr bool operator!=(nullopt_t, const optional<T>& rhs) noexcept
		{
			return static_cast<bool>(rhs);
		}

		template <class T>
		constexpr bool operator<(const optional<T>& lhs, nullopt_t) noexcept
		{
			return false;
		}

		template <class T>
		constexpr bool operator<(nullopt_t, const optional<T>& rhs) noexcept
		{
			return static_cast<bool>(rhs);
		}

		template <class T>
		constexpr bool operator<=(const optional<T>& lhs, nullopt_t) noexcept
		{
			return !static_cast<bool>(lhs);
		}

		template <class T>
		constexpr bool operator<=(nullopt_t, const optional<T>& rhs) noexcept
		{
			return true;
		}

		template <class T>
		constexpr bool operator>(const optional<T>& lhs, nullopt_t) noexcept
		{
			return static_cast<bool>(lhs);
		}

		template <class T>
		constexpr bool operator>(nullopt_t, const optional<T>& rhs) noexcept
		{
			return false;
		}

		template <class T>
		constexpr bool operator>=(const optional<T>& lhs, nullopt_t) noexcept
		{
			return true;
		}

		template <class T>
		constexpr bool operator>=(nullopt_t, const optional<T>& rhs) noexcept
		{
			return !static_cast<bool>(rhs);
		}

		//rel ops with T

		template <class T>
		constexpr bool operator==(const optional<T>& lhs, const T& rhs)
		{
			return static_cast<bool>(lhs) ? *lhs == rhs : false;
		}

		template <class T>
		constexpr bool operator==(const T& lhs, const optional<T>& rhs)
		{
			return static_cast<bool>(rhs) ? lhs == *rhs : false;
		}

		template <class T>
		constexpr bool operator!=(const optional<T>& lhs, const T& rhs)
		{
			return !(lhs == rhs);
		}

		template <class T>
		constexpr bool operator!=(const T& lhs, const optional<T>& rhs)
		{
			return !(lhs == rhs);
		}

		template <class T>
		constexpr bool operator<(const optional<T>& lhs, const T& rhs)
		{
			return static_cast<bool>(lhs) ? *lhs < rhs : true;
		}

		template <class T>
		constexpr bool operator<(const T& lhs, const optional<T>& rhs)
		{
			return static_cast<bool>(rhs) ? lhs < *rhs : false;
		}

		template <class T>
		constexpr bool operator<=(const optional<T>& lhs, const T& rhs)
		{
			return !(rhs < lhs);
		}

		template <class T>
		constexpr bool operator<=(const T& lhs, const optional<T>& rhs)
		{
			return !(rhs < lhs);
		}

		template <class T>
		constexpr bool operator>(const optional<T>& lhs, const T& rhs)
		{
			return rhs < lhs;
		}

		template <class T>
		constexpr bool operator>(const T& lhs, const optional<T>& rhs)
		{
			return rhs < lhs;
		}

		template <class T>
		constexpr bool operator>=(const optional<T>& lhs, const T& rhs)
		{
			return !(lhs < rhs);
		}

		template <class T>
		constexpr bool operator>=(const T& lhs, const optional<T>& rhs)
		{
			return !(lhs < rhs);
		}

		template <class T>
		constexpr optional<decay_t<T>> make_optional(T&& value)
		{
			return optional<decay_t<T>>{ mstd::forward<T>(value) };
		}

		template <class T>
		void swap(optional<T>&lhs, optional<T>& rhs) noexcept(noexcept(lhs.swap(rhs)))
		{
			lhs.swap(rhs);
		}
	}
}
