#pragma once
#include "../type_traits.hpp"

namespace mstd
{
	namespace experimental
	{
		template <class... Ts>
		using void_t = void;

		struct nonesuch
		{
			nonesuch() = delete;
			~nonesuch() = delete;
			nonesuch(nonesuch const&) = delete;
			void operator=(nonesuch const&) = delete;
		};

		namespace detail
		{
			template <class Default, class AlwaysVoid, template<class...> class Op, class... Args>
			struct detector
			{
				using value_t = false_type;
				using type = Default;
			};

			template <class Default, template<class...> class Op, class... Args>
			struct detector<Default, void_t<Op<Args...>>, Op, Args...>
			{
				using value_t = true_type;
				using type = Op<Args...>;
			};
		}

		template <template<class...> class Op, class... Args>
		using is_detected = typename detail::detector<nonesuch, void, Op, Args...>::value_t;

		template <template <class...> class Op, class... Args>
		constexpr bool is_detected_v = is_detected<Op, Args...>::value;

		template <template<class...> class Op, class... Args>
		using detected_t = typename detail::detector<nonesuch, void, Op, Args...>::type;

		template <class Default, template<class...> class Op, class... Args>
		using detected_or = detail::detector<Default, void, Op, Args...>;

		template <class Default, template<class...> class Op, class... Args>
		using detected_or_t = typename detected_or<Default, Op, Args...>::type;

		template <class	Expected, template<class...> class Op, class...	Args>
		using is_detected_exact = is_same<Expected, detected_t<Op, Args...>>;

		template <class	Expected, template<class...> class Op, class... Args>
		constexpr bool is_detected_exact_v = is_detected_exact<Expected, Op, Args...>::value;

		template <class	To, template<class...> class Op, class...Args>
		using is_detected_convertible = is_convertible<detected_t<Op, Args...>, To>;

		template <class	To, template<class...> class Op, class... Args>
		constexpr bool is_detected_convertible_v = is_detected_convertible<To, Op, Args...>::value;

		template <class...>
		struct conjunction : true_type
		{
		};

		template <class B1>
		struct conjunction<B1> : B1
		{
		};

		template <class B1, class... Bn>
		struct conjunction<B1, Bn...>
			: conditional_t<B1::value != false, conjunction<Bn...>, B1>
		{
		};

		template <class... B>
		constexpr bool conjunction_v = conjunction<B...>::value;

		template <class...>
		struct disjunction : false_type
		{
		};

		template <class B1>
		struct disjunction<B1> : B1
		{
		};

		template <class B1, class... Bn>
		struct disjunction<B1, Bn...> : conditional_t<B1::value != false, B1, disjunction<Bn...>>
		{
		};

		template <class... B>
		constexpr bool disjunction_v = disjunction<B...>::value;

		template <class B>
		struct negation : bool_constant<!B::value>
		{
		};

		template <class B>
		constexpr bool negation_v = negation<B>::value;
	}
}