#pragma once
#include "../array.hpp"
#include "../functional.hpp"
#include "type_traits.hpp"

namespace mstd
{
	namespace experimental
	{
		namespace detail
		{
			template <class T, class... Us>
			struct array_deduced_type
			{
				using type = T;
			};

			template <class... Ts>
			struct array_deduced_type<void, Ts...>
			{
				static_assert(conjunction<mstd::detail::is_not_reference_wrapper<decay_t<Ts>>...>::value, "Tralala");
				using type = common_type_t<Ts...>;
			};

			template <class... Ts>
			using array_deduced_type_t = typename array_deduced_type<Ts...>::type;
		}
		
		template <class D = void, class... Ts>
		constexpr array<detail::array_deduced_type_t<D, Ts...>, sizeof...(Ts)> make_array(Ts&&... vs)
		{
			return{ mstd::forward<Ts>(vs)... };
		}
	}

	//TODO
	//template <class T, std::size_t N>
	//constexpr array<remove_cv_t<T>, N> to_array(T(&a)[N])
}