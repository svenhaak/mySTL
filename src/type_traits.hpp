#pragma once

#ifdef __GLIBCXX__
//#define _mystl_no_trivially_traits
#endif

#include <cstddef> // max_align_t

namespace mstd
{
	using nullptr_t = decltype(nullptr);
	template <class T, T v>
	struct integral_constant
	{
		static constexpr T value = v;

		using value_type = T;
		using type = integral_constant<T, v>;

		constexpr operator T() const noexcept
		{
			return v;
		}

		constexpr T operator()() const noexcept
		{
			return v;
		}
	};

	template <bool v>
	using bool_constant = integral_constant<bool, v>;

	using false_type = bool_constant<false>;
	using true_type = bool_constant<true>;

	template <bool Cond, class T, class F>
	struct conditional
	{
		using type = F;
	};

	template <class T, class F>
	struct conditional<true, T, F>
	{
		using type = T;
	};

	template <bool Cond, class T, class F>
	using conditional_t = typename conditional<Cond, T, F>::type;


	template <class T>
	struct add_const
	{
		using type = const T;
	};

	template <class T>
	using add_const_t = typename add_const<T>::type;

	template <class T>
	struct add_volatile
	{
		using type = volatile T;
	};

	template <class T>
	using add_volatile_t = typename add_volatile<T>::type;

	template <class T>
	struct add_cv
	{
		using type = add_const_t<add_volatile_t<T>>;
	};

	template <class T>
	using add_cv_t = typename add_cv<T>::type;

	template <class T>
	struct remove_const
	{
		using type = T;
	};

	template <class T>
	struct remove_const<const T>
	{
		using type = T;
	};

	template <class T>
	struct remove_const<const T[]>
	{
		using type = T[];
	};

	template <class T, std::size_t N>
	struct remove_const<const T[N]>
	{
		using type = T[N];
	};

	template <class T>
	using remove_const_t = typename remove_const<T>::type;

	template <class T>
	struct remove_volatile
	{
		using type = T;
	};

	template <class T>
	struct remove_volatile<volatile T>
	{
		using type = T;
	};

	template <class T>
	struct remove_volatile<volatile T[]>
	{
		using type = T[];
	};

	template <class T, std::size_t N>
	struct remove_volatile<volatile T[N]>
	{
		using type = T[N];
	};

	template <class T>
	using remove_volatile_t = typename remove_volatile<T>::type;

	template <class T>
	struct remove_cv
	{
		using type = remove_const_t<remove_volatile_t<T>>;
	};

	template <class T>
	using remove_cv_t = typename remove_cv<T>::type;


	template <class T>
	struct add_pointer
	{
		using type = T*;
	};

	template <class T>
	using add_pointer_t = typename add_pointer<T>::type;

	template <class T>
	struct add_lvalue_reference
	{
		using type = T&;
	};

	template <>
	struct add_lvalue_reference<void>
	{
		using type = void;
	};

	template <>
	struct add_lvalue_reference<const void>
	{
		using type = const void;
	};

	template <>
	struct add_lvalue_reference<volatile void>
	{
		using type = volatile void;
	};

	template <>
	struct add_lvalue_reference<const volatile void>
	{
		using type = const volatile void;
	};

	template <class T>
	using add_lvalue_reference_t = typename add_lvalue_reference<T>::type;

	template <class T>
	struct add_rvalue_reference
	{
		using type = T&&;
	};

	template <>
	struct add_rvalue_reference<void>
	{
		using type = void;
	};

	template <>
	struct add_rvalue_reference<const void>
	{
		using type = const void;
	};

	template <>
	struct add_rvalue_reference<volatile void>
	{
		using type = volatile void;
	};

	template <>
	struct add_rvalue_reference<const volatile void>
	{
		using type = volatile void;
	};

	template <class T>
	using add_rvalue_reference_t = typename add_rvalue_reference<T>::type;

	template <class T>
	struct remove_extent
	{
		using type = T;
	};

	template <class T>
	struct remove_extent<T[]>
	{
		using type = T;
	};

	template <class T, std::size_t N>
	struct remove_extent<T[N]>
	{
		using type = T;
	};

	template <class T>
	using remove_extent_t = typename remove_extent<T>::type;

	template <class Base, class Derived>
	using is_base_of = std::is_base_of<Base, Derived>;

	template <class Base, class Derived>
	constexpr bool is_base_of_v = is_base_of<Base, Derived>::value;

	template <class To, class From>
	using is_convertible = std::is_convertible<To, From>;

	template <class To, class From>
	constexpr bool is_convertible_v = is_convertible<To, From>::value;

	template <class T, class U>
	struct is_same : false_type
	{
	};

	template <class T>
	struct is_same<T, T> : true_type
	{
	};

	template <class T, class U>
	constexpr bool is_same_v = is_same<T, U>::value;

	template <class T>
	using has_virtual_destructor = std::has_virtual_destructor<T>;

	template <class T>
	constexpr bool has_virtual_destructor_v = has_virtual_destructor<T>::value;

	template <class T>
	add_rvalue_reference_t<T> declval() noexcept;
	
	//namespace detail
	//{
	//	template <class To, class From>
	//	using assign_t = decltype(declval<To&>() = declval<const From&>());
	//}

	template <class To, class From>
	using is_assignable = /*experimental::is_detected<detail::assign_t, To, From>; */ std::is_assignable<To, From>;

	template <class To, class From>
	constexpr bool is_assignable_v = is_assignable<To, From>::value;

	template <class T, class... Args>
	using is_constructible = std::is_constructible<T, Args...>;

	template <class T>
	constexpr bool is_constructible_v = is_constructible<T>::value;

	template <class T>
	struct is_copy_assignable
		: is_assignable<add_lvalue_reference_t<T>,
		add_lvalue_reference_t<add_const_t<T>>>
	{
	};

	template <class T>
	constexpr bool is_copy_assignable_v = is_copy_assignable<T>::value;

	template <class T>
	struct is_copy_constructible
		: is_constructible<T, add_lvalue_reference_t<add_const_t<T>>>
	{
	};

	template <class T>
	constexpr bool is_copy_constructible_v = is_copy_constructible<T>::value;

	template <class T>
	using is_destructible = std::is_destructible<T>;

	template <class T>
	constexpr bool is_destructible_v = is_destructible<T>::value;

	template <class T>
	struct is_default_constructible : is_constructible<T>
	{
	};

	template <class T>
	constexpr bool is_default_constructible_v = is_constructible<T>::value;

	template <class T>
	struct is_move_assignable
		: is_assignable<add_lvalue_reference_t<T>, add_rvalue_reference_t<T>>
	{
	};

	template <class T>
	constexpr bool is_move_assignable_v = is_move_assignable<T>::value;

	template <class T>
	struct is_move_constructible
		: is_constructible<T, add_rvalue_reference_t<T>>
	{
	};

	template <class T>
	constexpr bool is_move_constructible_v = is_move_constructible<T>::value;

#ifndef _mystl_no_trivially_traits
	template <class T, class U>
	using is_trivially_assignable = std::is_trivially_assignable<T, U>;

	template <class T, class U>
	constexpr bool is_trivially_assignable_v = is_trivially_assignable<T, U>::value;

	template <class T, class... Args>
	using is_trivially_constructible = std::is_trivially_constructible<T, Args...>;

	template <class T>
	constexpr bool is_trivially_constructible_v = is_trivially_constructible<T>::value;

	template <class T>
	struct is_trivially_copy_assignable
		: is_trivially_assignable<add_lvalue_reference_t<T>, add_lvalue_reference_t<add_const_t<T>>>
	{
	};

	template <class T>
	constexpr bool is_trivially_copy_assignable_v = is_trivially_copy_assignable<T>::value;

	template <class T>
	struct is_trivially_copy_constructible
		: is_trivially_constructible<T, add_lvalue_reference_t<add_const_t<T>>>
	{
	};

	template <class T>
	constexpr bool is_trivially_copy_constructible_v = is_trivially_copy_constructible<T>::value;

	template <class T>
	using is_trivially_destructible = std::is_trivially_destructible<T>;

	template <class T>
	constexpr bool is_trivially_destructible_v = is_trivially_destructible<T>::value;

	template <class T>
	struct is_trivially_default_constructible
		: is_trivially_constructible<T>
	{
	};

	template <class T>
	constexpr bool is_trivially_default_constructible_v = is_trivially_default_constructible<T>::value;

	template <class T>
	struct is_trivially_move_assignable
		: is_trivially_assignable < add_lvalue_reference_t<T>,
		add_rvalue_reference_t < T >>
	{
	};

	template <class T>
	constexpr bool is_trivially_move_assignable_v = is_trivially_move_assignable<T>::value;

#endif

	template <class T, class U>
	using is_nothrow_assignable = std::is_nothrow_assignable<T, U>;

	template <class T, class U>
	constexpr bool is_nothrow_assignable_v = is_nothrow_assignable<T, U>::value;

	template <class T, class... Args>
	using is_nothrow_constructible = std::is_nothrow_constructible<T, Args...>;

	template <class T>
	constexpr bool is_nothrow_constructible_v = is_nothrow_constructible<T>::value;

	template <class T>
	struct is_nothrow_copy_assignable
		: is_nothrow_assignable<add_lvalue_reference_t<T>,
		add_lvalue_reference_t<add_const_t<T>>>
	{
	};

	template <class T>
	constexpr bool is_nothrow_copy_assignable_v = is_nothrow_copy_assignable<T>::value;

	template <class T>
	struct is_nothrow_copy_constructible
		: is_nothrow_constructible<T, add_lvalue_reference_t<add_const_t<T>>>
	{
	};

	template <class T>
	constexpr bool is_nothrow_copy_constructible_v = is_nothrow_copy_constructible<T>::value;

	template <class T>
	using is_nothrow_destructible = std::is_nothrow_destructible<T>;

	template <class T>
	constexpr bool is_nothrow_destructible_v = is_nothrow_destructible<T>::value;

	template <class T>
	struct is_nothrow_default_constructible
		: is_nothrow_constructible<T>
	{
	};

	template <class T>
	constexpr bool is_nothrow_default_constructible_v = is_nothrow_destructible<T>::value;

	template <class T>
	struct is_nothrow_move_assignable
		: is_nothrow_assignable<add_lvalue_reference_t<T>, add_rvalue_reference_t<T>>
	{
	};

	template <class T>
	constexpr bool is_nothrow_move_assignable_v = is_nothrow_move_assignable<T>::value;

	template <class T>
	struct is_nothrow_move_constructible
		: is_nothrow_constructible<T, add_rvalue_reference_t<T>>
	{
	};

	template <class T>
	constexpr bool is_nothrow_move_constructible_v = is_nothrow_move_constructible<T>::value;

	template <class T>
	struct is_array : false_type
	{
	};

	template <class T>
	struct is_array<T[]> : true_type
	{
	};

	template <class T, std::size_t N>
	struct is_array<T[N]> : true_type
	{
	};

	template <class T>
	constexpr bool is_array_v = is_array<T>::value;

	template <class T>
	using is_class = std::is_class<T>;

	template <class T>
	constexpr bool is_class_v = is_class<T>::value;

	template <class T>
	using is_enum = std::is_enum<T>;

	template <class T>
	constexpr bool is_enum_v = is_enum<T>::value;

	template <class T>
	struct is_floating_point : false_type
	{
	};

	template <>
	struct is_floating_point<float> : true_type
	{
	};

	template <>
	struct is_floating_point<double> : true_type
	{
	};

	template <>
	struct is_floating_point<long double> : true_type
	{
	};

	template <class T>
	constexpr bool is_floating_point_v = is_floating_point<T>::value;

	template <class T>
	using is_function = std::is_function<T>;

	template <class T>
	constexpr bool is_function_v = is_function<T>::value;

	template <class T>
	struct is_integral : false_type
	{
	};

	template <>
	struct is_integral<bool> : true_type
	{
	};

	template <>
	struct is_integral<char> : true_type
	{
	};

	template <>
	struct is_integral<char16_t> : true_type
	{
	};

	template <>
	struct is_integral<char32_t> : true_type
	{
	};

	template <>
	struct is_integral<wchar_t> : true_type
	{
	};

	template <>
	struct is_integral<signed char> : true_type
	{
	};

	template <>
	struct is_integral<short int> : true_type
	{
	};

	template <>
	struct is_integral<int> : true_type
	{
	};

	template <>
	struct is_integral<long int> : true_type
	{
	};

	template <>
	struct is_integral<long long int> : true_type
	{
	};

	template <>
	struct is_integral<unsigned char> : true_type
	{
	};

	template <>
	struct is_integral<unsigned short int> : true_type
	{
	};

	template <>
	struct is_integral<unsigned int> : true_type
	{
	};

	template <>
	struct is_integral<unsigned long int> : true_type
	{
	};

	template <>
	struct is_integral<unsigned long long int> : true_type
	{
	};

	template <class T>
	constexpr bool is_integral_v = is_integral<T>::value;

	template <class T>
	struct is_lvalue_reference : false_type
	{
	};

	template <class T>
	struct is_lvalue_reference<T&> : true_type
	{
	};

	template <class T>
	constexpr bool is_lvalue_reference_v = is_lvalue_reference<T>::value;

	template <class T>
	using is_member_function_pointer = std::is_member_function_pointer<T>;

	template <class T>
	constexpr bool is_member_function_pointer_v = is_member_function_pointer<T>::value;

	template <class T>
	using is_member_object_pointer = std::is_member_object_pointer<T>;

	template <class T>
	constexpr bool is_member_object_pointer_v = is_member_object_pointer<T>::value;

	template <class T>
	struct is_pointer : false_type
	{
	};

	template <class T>
	struct is_pointer<T*>
		: bool_constant<!is_member_object_pointer<T*>::value
		&& !is_member_function_pointer<T*>::value>
	{
	};

	template <class T>
	constexpr bool is_pointer_v = is_pointer<T>::value;

	template <class T>
	struct is_rvalue_reference : false_type
	{
	};

	template <class T>
	struct is_rvalue_reference<T&&> : true_type
	{
	};

	template <class T>
	constexpr bool is_rvalue_reference_v = is_rvalue_reference<T>::value;

	template <class T>
	using is_union = std::is_union<T>;

	template <class T>
	constexpr bool is_union_v = is_union<T>::value;

	namespace detail
	{
		template <class T>
		struct is_void_helper : false_type
		{
		};

		template <>
		struct is_void_helper<void> : true_type
		{
		};
	}

	template <class T>
	struct is_void : detail::is_void_helper<remove_cv_t<T>>
	{
	};
	
	template <class T>
	constexpr bool is_void_v = is_void<T>::value;

	template <class T>
	struct remove_all_extents
	{
		using type = T;
	};

	template <class T>
	using remove_all_extents_t = typename remove_all_extents<T>::type;

	template <class T>
	struct remove_all_extents<T[]>
	{
		using type = remove_all_extents_t<T>;
	};

	template <class T, std::size_t N>
	struct remove_all_extents<T[N]>
	{
		using type = remove_all_extents_t<T>;
	};

	template <class T>
	struct remove_pointer
	{
		using type = T;
	};

	template <class T>
	struct remove_pointer<T*>
	{
		using type = T;
	};

	template <class T>
	using remove_pointer_t = typename remove_pointer<T>::type;

	template <class T>
	struct remove_reference
	{
		using type = T;
	};

	template <class T>
	struct remove_reference<T&>
	{
		using type = T;
	};

	template <class T>
	struct remove_reference<T&&>
	{
		using type = T;
	};

	template <class T>
	using remove_reference_t = typename remove_reference<T>::type;

	template <class T>
	struct decay
	{
	private:
		using U = remove_reference_t<T>;

	public:
		using type = conditional_t < is_array<U>::value, remove_extent_t<U>*,
			conditional_t < is_function<U>::value,
			add_pointer_t<U>, remove_cv_t<U >> >;
	};

	template <class T>
	using decay_t = typename decay<T>::type;

	template <class T>
	struct make_signed
	{
		static_assert(is_integral<T>::value || is_enum<T>::value, "wrong type for make_signed");

		using type = conditional_t < is_same<T, signed char>::value
			|| is_same<T, unsigned char>::value, signed char,
			conditional_t < is_same<T, short>::value
			|| is_same<T, unsigned short>::value, short,
			conditional_t < is_same<T, int>::value
			|| is_same<T, unsigned int>::value, int,
			conditional_t < is_same<T, long>::value
			|| is_same<T, unsigned long>::value, long,
			conditional_t < is_same<T, long long>::value
			|| is_same<T, unsigned long long>::value, long long,
			conditional_t < sizeof(T) == sizeof(signed char), signed char,
			conditional_t < sizeof(T) == sizeof(short), short,
			conditional_t < sizeof(T) == sizeof(int), int,
			conditional_t<sizeof(T) == sizeof(long), long, long long >> >> >> >> >;
	};

	template <class T>
	using make_signed_t = typename make_signed<T>::type;

	template <class T>
	struct make_unsigned
	{
	private:
		using signed_t = make_signed_t<T>;

	public:
		using type = conditional_t < is_same<signed_t, signed char>::value, unsigned char,
			conditional_t < is_same<signed_t, short>::value, unsigned short,
			conditional_t < is_same<signed_t, int>::value, unsigned int,
			conditional_t < is_same<signed_t, long>::value, unsigned long, unsigned long long >> >> ;
	};

	template <class T>
	using make_unsigned_t = typename make_unsigned<T>::type;



	template <class T>
	using underlying_type = std::underlying_type<T>;

	template <class T>
	using underlying_type_t = typename underlying_type<T>::type;


	template <class T>
	struct alignment_of : integral_constant <size_t, alignof(T)>
	{
	};

	namespace detail
	{
		template <class T, unsigned I>
		struct extent_helper : integral_constant<size_t, 0>
		{
		};

		template <class T, unsigned N>
		struct extent_helper<T[N], 0> : integral_constant<size_t, N>
		{
		};

		template <class T, unsigned I, unsigned N>
		struct extent_helper<T[N], I> : extent_helper<T, I - 1>
		{
		};

		template <class T, unsigned I>
		struct extent_helper<T[], I> : extent_helper<T, I - 1>
		{
		};
	}

	template <class T, unsigned I = 0>
	struct extent : detail::extent_helper<T, I>
	{
	};

	template <class T>
	using is_abstract = std::is_abstract<T>;

	template <class T>
	constexpr bool is_abstract_v = is_abstract<T>::value;

	template <class T>
	struct is_arithmetic
		: bool_constant<is_integral<T>::value || is_floating_point<T>::value>
	{
	};

	template <class T>
	constexpr bool is_arithmetic_v = is_arithmetic<T>::value;

	template <class T>
	struct is_const : false_type
	{
	};

	template <class T>
	struct is_const<const T> : true_type
	{
	};

	template <class T>
	struct is_const<const T[]> : true_type
	{
	};

	template <class T, std::size_t N>
	struct is_const<const T[N]> : true_type
	{
	};

	template <class T>
	constexpr bool is_const_v = is_const<T>::value;

	template <class T>
	struct is_null_pointer : false_type
	{
	};

	template <>
	struct is_null_pointer<nullptr_t> : true_type
	{
	};

	template <class T>
	constexpr bool is_null_pointer_v = is_null_pointer<T>::value;

	template <class T>
	struct is_reference
		: bool_constant<is_lvalue_reference<T>::value || is_rvalue_reference<T>::value>
	{
	};

	template <class T>
	constexpr bool is_reference_v = is_reference<T>::value;

	template <class T>
	struct is_signed : bool_constant<is_arithmetic<T>::value && T(-1) < T(0)>
	{
	};

	template <class T>
	constexpr bool is_signed_v = is_signed<T>::value;

	template <class T>
	struct is_unsigned : bool_constant<is_arithmetic<T>::value && T(0) < T(-1)>
	{
	};

	template <class T>
	constexpr bool is_unsigned_v = is_unsigned<T>::value;

	template <class T>
	struct rank : integral_constant<size_t, 0>
	{
	};

	template <class T, unsigned I>
	struct rank<T[I]> : integral_constant<size_t, rank<T>::value + 1>
	{
	};

	template <class T>
	struct rank<T[]> : integral_constant<size_t, rank<T>::value + 1>
	{
	};

	template <class... Types>
	struct common_type;

	template <class... Types>
	using common_type_t = typename common_type<Types...>::type;

	template <class T>
	struct common_type<T>
	{
		using type = decay_t<T>;
	};

	template <class T, class U>
	struct common_type<T, U>
	{
		using type = decay_t<decltype(true ? declval<T>() : declval<U>())>;
	};

	template <class T, class U, class... V>
	struct common_type<T, U, V...>
	{
		using type = common_type_t<common_type_t<T, U>, V...>;
	};

	template <bool Cond, class T = void>
	struct enable_if
	{
	};

	template <class T>
	struct enable_if<true, T>
	{
		using type = T;
	};

	template <bool Cond, class T = void>
	using enable_if_t = typename enable_if<Cond, T>::type;

	template <class Fn, class... Args>
	struct result_of;

	template <class Fn, class... Args>
	struct result_of<Fn(Args...)>
	{
		using type = decltype(declval<Fn>()(declval<Args>()...));
	};

	template<class T>
	using result_of_t = typename result_of<T>::type;

	namespace detail
	{
		template <std::size_t I0, std::size_t... In>
		struct static_max;

		template <std::size_t I0>
		struct static_max<I0>
		{
			static constexpr std::size_t value = I0;
		};

		template <std::size_t I0, std::size_t I1, std::size_t... In>
		struct static_max<I0, I1, In...>
		{
			static constexpr std::size_t value = (I0 >= I1) ? static_max<I0, In...>::value : static_max<I1, In...>::value;
		};
	}

	template <std::size_t Len, class... Types>
	struct aligned_union
	{
		static constexpr std::size_t alignment_value = detail::static_max<alignof(Types)...>::value;

		struct type
		{
			alignas(alignment_value) char _s[detail::static_max<Len, sizeof(Types)...>::value];
		};
	};

	template <std::size_t Len, class... Types>
	using aligned_union_t = typename aligned_union<Len, Types...>::type;

	template<std::size_t Len, std::size_t Align = alignof(std::max_align_t)>
	struct aligned_storage
	{
		struct type
		{
			alignas(Align) unsigned char data[Len];
		};
	};

	template<std::size_t Len, std::size_t Align = alignof(std::max_align_t)>
	using aligned_storage_t = typename aligned_storage<Len, Align>::type;

	template <class T>
	using is_empty = std::is_empty<T>;

	template <class T>
	constexpr bool is_empty_v = is_empty<T>::value;

	template <class T>
	using is_final = std::is_final<T>;

	template <class T>
	constexpr bool is_final_v = is_final<T>::value;

	template <class T>
	using is_object = std::is_object<T>;

	template <class T>
	constexpr bool is_object_v = is_object<T>::value;

	/*template <class T, class R = void, class = void>
	struct is_callable : false_type
	{
	};

	template <class T>
	struct is_callable<T, void, experimental::void_t<result_of_t<T>>> : true_type
	{
	};

	template <class T, class R>
	struct is_callable<T, R, experimental::void_t<result_of_t<T>>> : is_convertible<result_of_t<T>, R>
	{
	};*/

	namespace detail
	{
		template <class T>
		class reference_wrapper;

		template <class T>
		struct remove_ref_wrap_helper
		{
			using type = T;
		};

		template <class T>
		struct remove_ref_wrap_helper<reference_wrapper<T>>
		{
			using type = T&;
		};

		template <class T>
		using remove_ref_wrap_t = typename remove_ref_wrap_helper<decay_t<T>>::type;
	}
}
