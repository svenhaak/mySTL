#pragma once

#include "compressed_pair.hpp"
//#include "memory.hpp" //addressof
#include "experimental/type_traits.hpp"
#include "exception.hpp"
#include "functional/operator_functors.hpp"
#include "utility/move_and_forward.hpp"
#include "utility/swap_and_exchange.hpp"

namespace mstd
{
	namespace detail
	{
		template <class F, class... Args>
		inline auto invoke_helper(F&& f, Args&&... args) ->	decltype(mstd::forward<F>(f)(mstd::forward<Args>(args)...))
		{
			return mstd::forward<F>(f)(mstd::forward<Args>(args)...);
		}

		template <class Base, class T, class Derived>
		inline auto invoke_helper(T Base::*pmd, Derived&& ref)
			-> decltype(mstd::forward<Derived>(ref).*pmd)
		{
			return mstd::forward<Derived>(ref).*pmd;
		}

		template <class PMD, class Pointer>
		inline auto invoke_helper(PMD pmd, Pointer&& ptr)
			-> decltype((*mstd::forward<Pointer>(ptr)).*pmd)
		{
			return (*mstd::forward<Pointer>(ptr)).*pmd;
		}

		template <class Base, class T, class Derived, class... Args>
		inline auto invoke_helper(T Base::*pmf, Derived&& ref, Args&&... args)
			-> decltype((mstd::forward<Derived>(ref).*pmf)(mstd::forward<Args>(args)...))
		{
			return (mstd::forward<Derived>(ref).*pmf)(mstd::forward<Args>(args)...);
		}

		template <class PMF, class Pointer, class... Args>
		inline auto invoke_helper(PMF pmf, Pointer&& ptr, Args&&... args)
			-> decltype(((*mstd::forward<Pointer>(ptr)).*pmf)(mstd::forward<Args>(args)...))
		{
			return ((*mstd::forward<Pointer>(ptr)).*pmf)(mstd::forward<Args>(args)...);
		}
	}

	template<class F, class... ArgTypes>
	decltype(auto) invoke(F&& f, ArgTypes&&... args)
	{
		return detail::invoke_helper(mstd::forward<F>(f), mstd::forward<ArgTypes>(args)...);
	}

	template <class T>
	class reference_wrapper
	{
	public:
		using type = T;

		reference_wrapper(type& ref) noexcept
			: pointer_{ mstd::addressof(ref) }
		{
		}

		reference_wrapper(type&&) = delete;

		reference_wrapper(const reference_wrapper& x) noexcept = default;

		reference_wrapper& operator=(const reference_wrapper& rhs) noexcept = default;

		type& get() const noexcept
		{
			return *pointer_;
		}

		operator type&() const noexcept
		{
			return *pointer_;
		}

		template <class... Args>
		result_of_t<type&(Args&&...)> operator() (Args&&... args) const
		{
			mstd::invoke(get(), mstd::forward<Args>(args)...);
		}

	private:
		type* pointer_;
	};

	namespace detail
	{
		template <class T>
		struct is_reference_wrapper : false_type
		{
		};

		template <class T>
		struct is_reference_wrapper<reference_wrapper<T>> : true_type
		{
		};

		template <class T>
		using is_not_reference_wrapper = experimental::negation<is_reference_wrapper<T>>;
	}

	template <class T>
	reference_wrapper<T> ref(T& elem) noexcept
	{
		return reference_wrapper<T>{ elem };
	}

	template <class T>
	reference_wrapper<T> ref(reference_wrapper<T> other) noexcept
	{
		return reference_wrapper<T>{ other.get() };
	}

	template <class T>
	reference_wrapper<T> ref(const T&&) = delete;

	template <class T>
	reference_wrapper<const T> cref(const T& elem) noexcept
	{
		return reference_wrapper<const T>(elem);
	}

	template <class T>
	reference_wrapper<const T> cref(reference_wrapper<T>& other) noexcept
	{
		return cref(other.get());
	}

	template <class T>
	void cref(const T&&) = delete;
	
	struct bad_function_call : exception
	{
	};

	template <class>
	class function; //undefiened

	namespace detail
	{
		template <class>
		struct maybe_arg_type
		{
		};

		template <class R, class Arg>
		struct maybe_arg_type<R(Arg)>
		{
			using argument_type = Arg;
		};

		template <class>
		struct maybe_first_and_second_arg_type
		{
		};

		template <class R, class First, class Second>
		struct maybe_first_and_second_arg_type<R(First, Second)>
		{
			using first_argument_type = First;
			using second_argument_type = Second;
		};


		template <class>
		struct function_imp_base;

		template <class R, class... Args>
		struct function_imp_base<R(Args...)>
		{
			virtual R operator()(Args&&... args) = 0;
			virtual void delete_this() = 0;
			virtual const std::type_info& target_type() noexcept = 0;
			virtual void* target() noexcept = 0;
			virtual function_imp_base* copy() = 0;
			virtual ~function_imp_base() noexcept = default;
		};

		template <class, class>
		struct function_imp;

		template <class F, class R, class... Args>
		struct function_imp<F, R(Args...)> : public function_imp_base<R(Args...)>
		{
			F callee_;
		public:
			function_imp(F&& f) : callee_{ mstd::move(f) }
			{
			}

			R operator()(Args&&... args) override
			{
				return mstd::invoke(callee_, mstd::forward<Args>(args)...);
			}

			void delete_this() override
			{
				delete this;
			}

			const std::type_info& target_type() noexcept override
			{
				return typeid(F);
			}

			void* target() noexcept override
			{
				return mstd::addressof(callee_);
			}

			function_imp_base<R(Args...)>* copy() override
			{
				return new function_imp{ *this };
			}

			~function_imp() noexcept
			{
			}
		};

		template <class, class, class>
		struct function_imp_alloc;

		template <class F, class Alloc, class R, class... Args>
		struct function_imp_alloc<F, Alloc, R(Args...)> : public function_imp_base<R(Args...)>
		{
			using Alloc_this = typename std::allocator<Alloc>::template rebind_alloc<function_imp_alloc<F, Alloc, R(Args...)>>;
			using Traits = std::allocator_traits<Alloc_this>;
			using Ptr_traits = pointer_traits<typename Traits::pointer>;

			compressed_pair<F, Alloc_this> callee_and_alloc_;

			function_imp_alloc(F&& f, Alloc& alloc) : callee_and_alloc_{ mstd::move(f), alloc }
			{
			}

			R operator()(Args&&... args) override
			{
				return mstd::invoke(callee_and_alloc_.first(), mstd::forward<Args>(args)...);
			}

			void delete_this() override
			{
				Traits::destroy(callee_and_alloc_.second(), this);
				Traits::deallocate(callee_and_alloc_.second(), Ptr_traits::pointer_to(*this), 1);
			}

			const std::type_info& target_type() noexcept override
			{
				return typeid(F);
			}

			void* target() noexcept
			{
				return mstd::addressof(callee_and_alloc_.first());
			}

			function_imp_base<R(Args...)>* copy() override
			{
				auto ret = Traits::allocate(callee_and_alloc_.second(), 1);
				Traits::construct(callee_and_alloc_.second(), mstd::addressof(*ret), *this);
				return mstd::addressof(*ret);
			}

			~function_imp_alloc() noexcept
			{
			}
		};
	}

	template <class R, class... Args>
	class function<R(Args...)> : public detail::maybe_arg_type<R(Args...)>, public detail::maybe_first_and_second_arg_type<R(Args...)>
	{
	public:
		using result_type = R;

		function() noexcept = default;

		function(nullptr_t) noexcept
		{
		}

		function(const function& other)
		{
			if (other.func_)
				func_ = other.func_->copy();
		}

		function(function&& other) : func_{ other.func_ }
		{
			other.func_ = nullptr;
		}

		template <class F/*, class = enable_if_t<is_callable<F>>*/>
		function(F f)
		{
			func_ = new detail::function_imp<F, R(Args...)>{ mstd::move(f) };
		}


		template <class Alloc>
		function(allocator_arg_t, const Alloc& alloc)
		{
			//using Imp_alloc = std::allocator_traits<Alloc>::template rebind_alloc<detail::function_imp_alloc<
			//func_ = mstd::addressof(std::all)
		}

		/*template <class alloc>
		function(allocator_arg_t, const alloc& alloc, nullptr_t);

		template< class alloc >
		function(allocator_arg_t, const alloc& alloc, const function& other);

		template <class alloc>
		function(allocator_arg_t, const alloc& alloc, function&& other);

		template <class f, class alloc>
		function(allocator_arg_t, const alloc& alloc, f f);*/

		~function()
		{
			if (func_)
				func_->delete_this();
			func_ = nullptr;
		}

		function& operator=(const function& other)
		{
			function{ other }.swap(*this);
			return *this;
		}

		function& operator=(function&& other)
		{
			function{ mstd::move(other) }.swap(*this);
			return *this;
		}

		function& operator=(nullptr_t) noexcept
		{
			function{ nullptr }.swap(*this);
			return *this;
		}

		template <class F/*, class = enable_if_t<is_callable<F>::value && is_same<R, result_of_t<F>>*/>
		function& operator=(F&& f)
		{
			function{ mstd::forward<F>(f) }.swap(*this);
			return *this;
		}

		template <class F>
		function& operator=(reference_wrapper<F> f)
		{
			function{ f }.swap(*this);
			return *this;
		}

		void swap(function& other) noexcept(noexcept(mstd::swap(func_, other.func_)))
		{
			mstd::swap(func_, other.func_);
		}

		template <class F, class Alloc>
		void assign(F&& f, Alloc& alloc)
		{
			function{ allocator_arg, alloc, mstd::forward<F>(f) }.swap(*this);
		}

		explicit operator bool() const noexcept
		{
			return func_ != nullptr;
		}

		R operator()(Args... args) const
		{
			if (static_cast<bool>(*this) == false)
				throw bad_function_call{};

			if (is_same<R, void>::value)
				mstd::invoke(*func_, mstd::move(args)...);
			else
				return mstd::invoke(*func_, mstd::move(args)...);
		}

		const std::type_info& target_type() const noexcept
		{
			return func_->target_type();
		}

		template <class T>
		T* target() noexcept
		{
			if (typeid(T) == target_type())
				return static_cast<T*>(func_->target());
			else
				return nullptr;
		}

		template <class T>
		const T* target() const noexcept
		{
			if (typeid(T) == target_type())
				return static_cast<const T*>(func_->target());
			else
				return nullptr;
		}

	private:
		detail::function_imp_base<R(Args...)>* func_ = nullptr;
	};

	template <class R, class... Args>
	void swap(function<R(Args...)>& lhs, function<R(Args...)>& rhs)// noexcept(lhs.swap(rhs))
	{
		lhs.swap(rhs);
	}

	template <class R, class... Args>
	bool operator==(const function<R(Args...)>& f, nullptr_t) noexcept
	{
		return !f;
	}

	template <class R, class... Args>
	bool operator==(nullptr_t, const function<R(Args...)>& f) noexcept
	{
		return !f;
	}

	template <class R, class... Args>
	bool operator!=(const function<R(Args...)>& f, nullptr_t) noexcept
	{
		return !(f == nullptr);
	}

	template <class R, class... Args>
	bool operator!=(nullptr_t, const function<R(Args...)>& f) noexcept
	{
		return !(nullptr == f);
	}

	//TODO uses_allocator
}
