#pragma warning (disable: 4814)

#include <algorithm>
#include <iostream>
#include <chrono>
#include <functional>
#include <string>
#include <vector>
#include "algorithm.hpp"
#include "cstring.h"
#include "exception.hpp"
#include "stdexcept.hpp"
#include "experimental/array.hpp"
#include "experimental/functional.hpp"
#include "experimental/memory.hpp"
#include "experimental/optional.hpp"
#include "experimental/scope.hpp"
#include "experimental/string_view.hpp"
#include "experimental/type_traits.hpp"
#include "functional.hpp"
#include "iterator.hpp"
#include "list.hpp"
#include "memory.hpp"
#include "numeric.hpp"
#include "ratio.hpp"
#include "stack.hpp"
#include "string.hpp"
#include "tuple.hpp"
#include "type_traits.hpp"
#include "utility.hpp"
#include "vector.hpp"

using namespace mstd;
using namespace std::literals;


template<class container>
void showAll(const container& con) noexcept
{
	std::cout << "{ ";
	for (const auto& x : con)
	{
		std::cout << x << ", ";
	}
	std::cout << "}";
}

int main()
{	
	
}
