#pragma once

namespace mstd
{
	struct exception
	{
		virtual ~exception();

		virtual const char* what() const noexcept;
	};

	struct bad_exception : exception
	{
		using exception::exception;
	};

	/*struct nested_exception
	{
		nested_exception() noexcept;
		nested_exception(const nested_exception&) noexcept = default;
		nested_exception& operator= (const nested_exception&) noexcept = default;
		virtual ~nested_exception() = default;

		[[noreturn]] void rethrow_nested() const;
		exception_ptr nested_ptr() const noexcept;
	};*/
}
