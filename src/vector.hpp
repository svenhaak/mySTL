#pragma once

#include "memory.hpp"
#include "algorithm.hpp"
#include "iterator.hpp"
#include <initializer_list>
#include "utility/move_and_forward.hpp"


namespace mstd
{
	template <class T, class Allocator = allocator<T>>
	class vector
	{
	public:
		using value_type = T;
		using allocator_type = Allocator;
		using reference = value_type&;
		using const_reference = const value_type&;
		using pointer = typename allocator_traits<allocator_type>::pointer;
		using const_pointer = typename allocator_traits<allocator_type>::const_pointer;
		using difference_type = typename allocator_traits<allocator_type>::difference_type;
		using size_type = typename allocator_traits<allocator_type>::size_type;
		using iterator = value_type*;
		using const_iterator = const value_type*;
		using const_reverse_iterator = reverse_iterator<const_iterator>;
		using reverse_iterator = reverse_iterator<iterator>;


	private:
		pointer m_begin{ nullptr };
		size_type m_size{ 0u };
		size_type m_capacity{ 0u };
		allocator_type m_alloc{};

	private:
		size_type next_capacity(const size_type n_new_elements)//computes next capacity (~ capacity()^1,6)
		{
			const size_type m3 = max_size() / 3;

			if (capacity() < m3)
			{
				return capacity() + mstd::max(3 * (capacity() + 1) / 5, n_new_elements);
			}

			if (capacity() < m3 * 2)
			{
				return capacity() + mstd::max((capacity() + 1) / 2, n_new_elements);
			}

			return max_size();
		}

		void change_capacity(size_type n)//ensures: capacity() == n
		{
			pointer new_begin = allocator_traits<allocator_type>::allocate(m_alloc, n, m_begin);
			pointer new_current = new_begin;
			pointer current = m_begin;

			try
			{
				for (pointer new_end = new_begin + size(); new_current != new_end; ++current, ++new_current)
				{
					allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*new_current), mstd::move_if_noexcept(*current));
				}
			}
			catch (...)
			{
				while (current != new_begin)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*--current));
				}
				allocator_traits<allocator_type>::deallocate(m_alloc, new_begin, n);
				throw;
			}

			for (size_type i = 0u; i < size(); ++i)
			{
				allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*(m_begin + i)));
			}

			allocator_traits<allocator_type>::deallocate(m_alloc, m_begin, capacity());
			m_begin = new_begin;
			m_capacity = n;
		}

		inline void check_max(size_type n) const
		{
			if (n > max_size())
			{
				throw length_error{ "vector::capacity > vector::max_size" };
			}
		}

	public:
		void reserve(size_type n)
		{
			check_max(n);

			if (n > capacity())
			{
				change_capacity(n);
			}
		}

	private:
		void _reserve_exp(size_type number_of_required_new_elements)
		{
			check_max(number_of_required_new_elements); //todo: testet das falsche
			if (number_of_required_new_elements + size() > capacity())
			{
				change_capacity(next_capacity(number_of_required_new_elements));
			}
		}

		void _init_reserve(size_type n)
		{
			check_max(n);

			size_type sz = next_capacity(n);

			m_begin = allocator_traits<allocator_type>::allocate(m_alloc, sz);
			m_capacity = sz;
		}

		void clear_all()
		{
			clear();
			allocator_traits<allocator_type>::deallocate(m_alloc, m_begin, capacity());
			m_capacity = 0u;
		}

	public:
		vector() = default;

		explicit vector(const allocator_type& alloc) : m_alloc{ alloc }
		{
		}

		explicit vector(size_type n, const allocator_type& alloc = allocator_type{}) : m_alloc{ alloc }
		{
			_init_reserve(n);

			pointer tmp_current = m_begin;
			try
			{
				for (size_type i = 0; i < n; ++i)
				{
					allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*tmp_current));
					++tmp_current;
				}
			}
			catch (...)
			{
				while (tmp_current != m_begin)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*--tmp_current));
				}
				allocator_traits<allocator_type>::deallocate(m_alloc, m_begin, m_capacity);
				throw;
			}
			m_size = n;
		}

		vector(size_type n, const value_type& value, const allocator_type& alloc = allocator_type{}) : m_alloc{ alloc }
		{
			_init_reserve(n);

			pointer ptr = m_begin;
			try
			{
				for (size_type i = 0; i < n; ++i)
				{
					allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*ptr), value);
					++ptr;
				}
			}
			catch (...)
			{
				while (ptr != m_begin)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*--ptr));
				}
				allocator_traits<allocator_type>::deallocate(m_alloc, m_begin, capacity());
				throw;
			}
			m_size = n;
		}

		template <class InputIterator, class = enable_if_t<!is_convertible<InputIterator, size_type>::value>>
		vector(InputIterator first, InputIterator last, const allocator_type& alloc = allocator_type{}) : m_alloc{ alloc }
		{
			size_type sz = mstd::distance(first, last);
			_init_reserve(sz);

			pointer tmp_current = m_begin;
			try
			{
				while (first != last)
				{
					allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*tmp_current), *first);
					++tmp_current; ++first;
				}
			}
			catch (...)
			{
				while (tmp_current != m_begin)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*--tmp_current));
				}
				allocator_traits<allocator_type>::deallocate(m_alloc, m_begin, capacity());
				throw;
			}

			m_size = sz;
		}

		vector(std::initializer_list<value_type> il, const allocator_type& alloc = allocator_type{}) : vector{ il.begin(), il.end(), alloc }
		{
		}

		vector(const vector& other) : vector{ other, allocator_traits<allocator_type>::select_on_container_copy_construction(other.m_alloc) }
		{
		}

		vector(const vector& other, const allocator_type& alloc) : vector{ other.begin(), other.end(), alloc }
		{
			/*_init_reserve(other.size());

			pointer ptr = m_begin;
			try
			{
				for (size_type i = 0; i < size(); i++)
				{
					allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*ptr), other.m_begin[i]);
					++ptr;
				}
			}
			catch (...)
			{
				while (ptr != m_begin)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*--ptr));
				}

				allocator_traits<allocator_type>::deallocate(m_alloc, m_begin, capacity());
				throw;
			}
			m_size = other.size();*/
		}

		vector(vector&& other) : // TODO: Propagate??
			m_begin{ mstd::move(other.m_begin) },
			m_size{ mstd::move(other.size()) },
			m_capacity{ mstd::move(other.capacity()) },
			m_alloc{ mstd::move(other.m_alloc) }
		{
			other.m_size = other.m_capacity = 0u;
			other.m_begin = nullptr;
		}

		vector& operator= (const vector& other)
		{
			if (this != &other)
			{
				clear();

				if (allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value)
				{
					m_alloc = other.m_alloc;
				}

				reserve(other.size());

				for (size_type i = 0; i < size(); ++i)
				{
					allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*(m_begin + i)), other.m_begin[i]);
				}
				m_size = other.size();
			}
			return *this;
		}

		vector& operator= (vector&& other)
		{
			if (this != &other)
			{
				clear_all();

				m_begin = mstd::move(other.m_begin);
				m_size = mstd::move(other.size());
				m_capacity = mstd::move(other.capacity());
				if (allocator_traits<allocator_type>::propagate_on_container_move_assignment::value)
				{
					m_alloc = mstd::move(other.m_alloc);
				}
			}
			return *this;
		}

		vector& operator= (std::initializer_list<value_type> il)
		{
			assign(il.begin(), il.end());
			return *this;
		}

		~vector()
		{
			clear_all();
		}

		void assign(size_type n, const value_type& val)
		{
			clear();
			resize(n, val);
		}

		template <class InputIterator>
		void assign(InputIterator first, InputIterator last)
		{
			clear();
			_reserve_exp(mstd::distance(first, last));
			while (first != last)
			{
				m_begin[size()] = *first;
				++first; ++m_size;
			}
		}

		void assign(std::initializer_list<value_type> il)
		{
			assign(il.begin(), il.end());
		}

		template<class... Args>
		iterator emplace(const_iterator position, Args&&... args)
		{
			size_type off = position/*.getPtr()*/ - m_begin;

			emplace_back(mstd::forward<Args>(args)...);

			rotate(begin() + off, end() - 1, end());

			return{ begin() + off };
		}

		template<class... Args>
		void emplace_back(Args&&... args)
		{
			_reserve_exp(1);

			allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*(m_begin + size())), mstd::forward<Args>(args)...);
			++m_size;
		}

		iterator insert(const_iterator position, const value_type& val)
		{
			return emplace(position, val);
		}

		iterator insert(const_iterator position, value_type&& val)
		{
			return emplace(position, mstd::move(val));
		}

		iterator insert(const_iterator position, size_type n, const value_type& val)
		{
			if (size() + n > capacity())
			{
				size_type pos = position - cbegin();
				_reserve_exp(n);
				position = cbegin() + pos;
			}

			for (const_iterator i = cend(); i != position + n; --i)
			{
				*i = *(i - 1);
			}

			for (const_iterator i = position; i != position + n; ++i)
			{
				allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*(i/*.getPtr()*/)), val);
			}

			return{ position/*.getPtr()*/ + n };
		}

		template <class InputIterator>
		iterator insert(const_iterator position, InputIterator first, InputIterator last)
		{
			difference_type n = mstd::distance(first, last);

			if (size() + n > capacity())
			{
				difference_type pos = position - cbegin();
				_reserve_exp(n);
				position = cbegin() + pos;
			}

			for (iterator i = end(); i != position + n; --i)
			{
				*i = *(i - 1);
			}

			while (first != last)
			{
				*position = *first;
				++position; ++first;
			}

			return{ position/*.getPtr()*/ };
		}

		iterator insert(const_iterator position, std::initializer_list<value_type> il)
		{
			return insert(position, il.begin(), il.end());
		}

		bool empty() const noexcept
		{
			return size() == 0;
		}

		iterator erase(const_iterator position)
		{
			size_type pos = mstd::distance(cbegin(), position);

			for (auto i = pos; i < size() - 1; ++i)
			{
				m_begin[i] = move_if_noexcept(m_begin[i + 1]);
			}

			pop_back();

			return{ m_begin + pos };
		}

		iterator erase(const_iterator first, const_iterator last)
		{
			size_type n = mstd::distance(first, last);

			for (iterator i{ first/*.getPtr()*/ }; i != end() - n; ++i)
			{
				*i = *(i + n);
			}

			m_size -= n;

			for (size_type i = 0; i < n; ++i)
			{
				allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*(m_begin + size())));
			}
			return{ last/*.getPtr()*/ };
		}

		void push_back(const value_type& val)
		{
			emplace_back(val);
		}

		void push_back(value_type&& val)
		{
			emplace_back(mstd::move(val));
		}

		void pop_back()
		{
			allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*(m_begin + --m_size)));
		}

		reference front()
		{
			return m_begin[0];
		}

		const_reference front() const
		{
			return m_begin[0];
		}

		reference back()
		{
			return m_begin[size() - 1];
		}

		const_reference back() const
		{
			return m_begin[size() - 1];
		}

		reference at(size_type n)
		{
			if (n >= size())
			{
				throw out_of_range{ "vector::at" };
			}
			return m_begin[n];
		}

		const_reference at(size_type n) const
		{
			if (n >= size())
			{
				throw out_of_range{ "vector::at" };
			}
			return m_begin[n];
		}

		reference operator[](size_type n)
		{
			return m_begin[n];
		}

		const_reference operator[](size_type n) const
		{
			return m_begin[n];
		}

		size_type size() const noexcept
		{
			return m_size;
		}

		size_type capacity() const noexcept
		{
			return m_capacity;
		}

		size_type max_size() const noexcept
		{
			return allocator_traits<allocator_type>::max_size(m_alloc);
		}

		void swap(vector& other)
		{
			if (this != &other)
			{
				mstd::swap(m_begin, other.m_begin);
				mstd::swap(m_size, other.m_size);
				mstd::swap(m_capacity, other.capacity());
				if (std::allocator_traits<allocator_type>::propagate_on_container_swap::value)
				{
					mstd::swap(m_alloc, other.m_alloc);
				}
			}
		}

		void resize(size_type n, const value_type& val)
		{
			if (n < size())
			{
				for (size_type i = n; i < size(); ++i)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*(m_begin + i)));
				}
			}

			else if (n > size())
			{
				_reserve_exp(n);

				pointer except = m_begin + size();
				try
				{
					for (size_type i = size(); i < n; ++i)
					{
						allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*except++), val);
					}
				}
				catch (...)
				{
					while (except != m_begin + size())
					{
						allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*--except));
					}
					throw;
				}
			}
			m_size = n;
		}

		void resize(size_type n)
		{
			if (n < size())
			{
				for (size_type i = n; i < size(); ++i)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*(m_begin + i)));
				}
			}

			else if (n > size())
			{
				reserve(n);

				//TODO: exception safety
				for (size_type i = size(); i < n; ++i)
				{
					allocator_traits<allocator_type>::construct(m_alloc, mstd::addressof(*(m_begin + i)));
				}
			}
			m_size = n;
		}

		void shrink_to_fit()
		{
			if (size() != capacity())
			{
				change_capacity(size());
			}
		}

		void clear() noexcept
		{
			if (m_size)
			{
				for (pointer p = m_begin + size() - 1; p >= m_begin; --p)
				{
					allocator_traits<allocator_type>::destroy(m_alloc, mstd::addressof(*p));
				}
				m_size = 0;
			}
		}

		value_type* data() noexcept
		{
			return mstd::addressof(*m_begin);
		}

		const value_type* data() const noexcept
		{
			return mstd::addressof(*m_begin);
		}

		allocator_type get_allocator() const noexcept
		{
			return m_alloc;
		}

		iterator begin() noexcept
		{
			return{ m_begin };
		}

		const_iterator begin() const noexcept
		{
			return cbegin();
		}

		const_iterator cbegin() const noexcept
		{
			return{ m_begin };
		}

		iterator end() noexcept
		{
			return{ m_begin + size() };
		}

		const_iterator end() const noexcept
		{
			return cend();
		}

		const_iterator cend() const noexcept
		{
			return{ m_begin + size() };
		}

		reverse_iterator rbegin() noexcept
		{
			return{ end() };
		}

		const_reverse_iterator rbegin() const noexcept
		{
			return crbegin();
		}

		const_reverse_iterator crbegin()const
		{
			return{ end() };
		}

		reverse_iterator rend() noexcept
		{
			return{ begin() };
		}

		const_reverse_iterator rend() const noexcept
		{
			return crend();
		}

		const_reverse_iterator crend() const noexcept
		{
			return{ begin() };
		}
	};

	template <class T, class Alloc>
	bool operator== (const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs)
	{
		return (lhs.size() != rhs.size()) ? false : equal(lhs.cbegin(), lhs.cend(), rhs.cbegin(), rhs.cend());
	}

	template <class T, class Alloc>
	bool operator!= (const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class T, class Alloc>
	bool operator<(const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs)
	{
		return std::lexicographical_compare(lhs.cbegin(), lhs.cend(), rhs.cbegin(), rhs.cend());
	}

	template <class T, class Alloc>
	bool operator<= (const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs)
	{
		return !(rhs < lhs);
	}

	template <class T, class Alloc>
	bool operator>(const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs)
	{
		return rhs < lhs;
	}

	template <class T, class Alloc>
	bool operator>= (const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs)
	{
		return !(lhs < rhs);
	}

	template <class T, class Alloc>
	void swap(vector<T, Alloc>& x, vector<T, Alloc>& y)
	{
		x.swap(y);
	}
}
