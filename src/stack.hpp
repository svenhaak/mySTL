#pragma once

#include <deque>
#include <initializer_list>
#include "memory.hpp"
#include "utility/move_and_forward.hpp"

namespace mstd
{
	template <class T, class Container = std::deque<T>>
	class stack
	{
	public:
		using container_type = Container;
		using value_type = typename Container::value_type;
		using size_type = typename Container::size_type;
		using reference = typename Container::reference;
		using const_reference = typename Container::const_reference;

		explicit stack(const container_type& cont)
			: c{ cont }
		{ }

		explicit stack(container_type&& cont = container_type{ })
			: c{ mstd::move(cont) }
		{ }

		template <class Alloc, class = enable_if_t<uses_allocator<container_type, Alloc>::value>>
		explicit stack(const Alloc& alloc)
			: c{ alloc }
		{ }

		template <class Alloc/*, class = enable_if_t<uses_allocator<container_type, Alloc>::value>*/>
		stack(const Container& cont, const Alloc& alloc)
			: c{ cont, alloc }
		{ }

		template <class Alloc/*, class = enable_if_t<uses_allocator<container_type, Alloc>::value>*/>
		stack(Container&& cont, const Alloc& alloc)
			: c{ mstd::move(cont), alloc }
		{ }

		template <class Alloc/*, class = enable_if_t<uses_allocator<container_type, Alloc>::value>*/>
		stack(const stack& other, const Alloc& alloc)
			: c{ other.c, alloc }
		{ }

		template <class Alloc/*, class = enable_if_t<uses_allocator<container_type, Alloc>::value>*/>
		stack(stack&& other, const Alloc& alloc)
			: c{ mstd::move(other.c), alloc }
		{ }

		reference top()
		{
			return c.back();
		}

		const_reference top() const
		{
			return c.back();
		}

		bool empty() const
		{
			return c.empty();
		}

		size_type size() const
		{
			return c.size();
		}

		void push(const value_type& value)
		{
			c.push_back(value);
		}

		void push(value_type&& value)
		{
			c.push_back(mstd::move(value));
		}

		template <class... Args>
		void emplace(Args&&... args)
		{
			c.emplace_back(mstd::forward<Args>(args)...);
		}

		void pop()
		{
			c.pop_back();
		}

		void swap(stack& other) noexcept(noexcept(mstd::swap(c, other.c)))
		{
			using mstd::swap;
			swap(c, other.c);
		}

		template <class U, class C>
		friend bool operator==(const stack<U, C>& lhs, const stack<U, C>& rhs);

		template <class U, class C>
		friend bool operator<(const stack<U, C>& lhs, const stack<U, C>& rhs);

	protected:
		container_type c;
	};

	template <class T, class Container>
	bool operator==(const stack<T, Container>& lhs, const stack<T, Container>& rhs)
	{
		return lhs.c == rhs.c;
	}

	template <class T, class Container>
	bool operator!=(const stack<T, Container>& lhs, const stack<T, Container>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class T, class Container>
	bool operator<(const stack<T, Container>& lhs, const stack<T, Container>& rhs)
	{
		return lhs.c < rhs.c;
	}

	template <class T, class Container>
	bool operator<=(const stack<T, Container>& lhs, const stack<T, Container>& rhs)
	{
		return !(lhs > rhs);
	}

	template <class T, class Container>
	bool operator>(const stack<T, Container>& lhs, const stack<T, Container>& rhs)
	{
		return rhs < lhs;
	}

	template <class T, class Container>
	bool operator>=(const stack<T, Container>& lhs, const stack<T, Container>& rhs)
	{
		return !(lhs < rhs);
	}

	template <class T, class Container>
	void swap(stack<T, Container>& lhs, stack<T, Container>& rhs) noexcept(noexcept(lhs.swap(rhs)))
	{
		return lhs.swap(rhs);
	}

	template <class T, class Container, class Alloc>
	struct uses_allocator<stack<T, Container>, Alloc> : uses_allocator<Container, Alloc>::type
	{ };
}
