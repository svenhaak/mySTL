#include "exception.hpp"

namespace mstd
{
	exception::~exception() = default;

	const char* exception::what() const noexcept
	{
		return "mstd::exception";
	}
}
