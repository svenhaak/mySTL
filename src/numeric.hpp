#pragma once

namespace mstd
{
	template <class ForwardIterator, class T>
	void iota(ForwardIterator first, ForwardIterator last, T val)
	{
		while (first != last)
		{
			*first++ = val++;
		}
	}

	template <class InputIterator, class T>
	T accumulate(InputIterator first, InputIterator last, T init)
	{
		while (first != last)
		{
			init += *first++;
		}
	}

	template <class InputIterator, class T, class BinaryOperation>
	T accumulate(InputIterator first, InputIterator last, T init,
		BinaryOperation binary_op)
	{
		while (first != last)
		{
			init = binary_op(init, *first);
		}
	}
}
