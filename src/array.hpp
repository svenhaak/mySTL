#pragma once

#include "stdexcept.hpp"
#include "algorithm.hpp"
#include "utility.hpp"
#include "iterator.hpp"
#include "type_traits.hpp"
namespace mstd
{
	template <class T, std::size_t N>
	struct array
	{
		using value_type = T;
		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;
		using reference = value_type&;
		using const_reference = const value_type&;
		using pointer = value_type*;
		using const_pointer = const value_type*;
		using iterator = value_type*;
		using const_iterator = const value_type*;
		using reverse_iterator = mstd::reverse_iterator<iterator>;
		using const_reverse_iterator = mstd::reverse_iterator<const_iterator>;

		T arr_[N > 0 ? N : 1];

		constexpr bool empty() const noexcept
		{
			return N != 0;
		}

		constexpr size_type size() const noexcept
		{
			return N;
		}

		constexpr size_type max_size() const noexcept
		{
			return N;
		}

		reference at(size_type pos)
		{
			if (N < pos)
				throw out_of_range{ "array::at" };
			return arr_[pos];
		}

		constexpr const_reference at(size_type pos) const
		{
			if (N < pos)
				throw out_of_range{ "array::at" };
			return arr_[pos];
		}

		reference operator[](size_type pos)
		{
			return arr_[pos];
		}

		constexpr const_reference operator[](size_type pos) const
		{
			return arr_[pos];
		}

		reference front()
		{
			return arr_[0];
		}

		constexpr const_reference front() const
		{
			return arr_[0];
		}

		reference back()
		{
			return arr_[N > 0 ? N - 1 : 0];
		}

		constexpr const_reference back() const noexcept
		{
			return arr_[N > 0 ? N - 1 : 0];
		}

		pointer data()
		{
			return arr_;
		}

		const_pointer data() const
		{
			return arr_;
		}

		iterator begin() noexcept
		{
			return arr_;
		}

		const_iterator begin() const noexcept
		{
			return arr_;
		}

		const_iterator cbegin() const noexcept
		{
			return begin();
		}

		iterator end() noexcept
		{
			return arr_ + N;
		}

		const_iterator end() const noexcept
		{
			return arr_ + N;
		}

		const_iterator cend() const noexcept
		{
			return end();
		}

		reverse_iterator rbegin() noexcept
		{
			return reverse_iterator(end());
		}

		const_reverse_iterator rbegin() const noexcept
		{
			return const_reverse_iterator(end());
		}

		const_reverse_iterator crbegin() const noexcept
		{
			return rbegin();
		}

		reverse_iterator rend() noexcept
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rend() const noexcept
		{
			return const_reference(begin());
		}

		const_reverse_iterator crend() const noexcept
		{
			return rend();
		}

		void fill(const T& value)
		{
			for (T& x : arr_)
				x = value;
		}

		void swap(array& other) noexcept(noexcept(swap(declval<T&>(), declval<T&>())))
		{
			using mstd::swap;
			swap(arr_, other.arr_);
		}
	};

	template <class T, std::size_t N>
	bool operator==(const array<T, N>& lhs, const array<T, N>& rhs)
	{
		return equal(lhs.begin(), lhs.end(), rhs.begin());
	}

	template <class T, std::size_t N>
	bool operator!=(const array<T, N>& lhs, const array<T, N>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class T, std::size_t N>
	bool operator<(const array<T, N>& lhs, const array<T, N>& rhs)
	{
		return mstd::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

	template <class T, std::size_t N>
	bool operator<=(const array<T, N>& lhs, const array<T, N>& rhs)
	{
		return !(lhs > rhs);
	}

	template <class T, std::size_t N>
	bool operator>(const array<T, N>& lhs, const array<T, N>& rhs)
	{
		return rhs < lhs;
	}

	template <class T, std::size_t N>
	bool operator>=(const array<T, N>& lhs, const array<T, N>& rhs)
	{
		return !(lhs < rhs);
	}

	template <class T1, std::size_t N1, class T2, std::size_t N2>
	void swap(array<T1, N1>& lhs, array<T2, N2>& rhs)
	{
		lhs.swap(rhs);
	}

	template <class T>
	struct tuple_size;

	template <class T, std::size_t N>
	struct tuple_size<array<T, N>> : integral_constant<std::size_t, N>
	{ };

	template <std::size_t I, class T>
	struct tuple_element;

	template <std::size_t I, class T, std::size_t N>
	struct tuple_element<I, array<T, N>>
	{
		using type = T;
	};

	template <std::size_t I, class T, std::size_t N>
	constexpr T& get(array<T, N>& a) noexcept
	{
		static_assert(I < N, "array index out of range");
		return a[I];
	}

	template <std::size_t I, class T, std::size_t N>
	constexpr T&& get(array<T, N>&& a) noexcept
	{
		static_assert(I < N, "array index out of range");
		return mstd::move(a[I]);
	}

	template <std::size_t I, class T, std::size_t N>
	constexpr const T& get(const array<T, N>& a) noexcept
	{
		static_assert(I < N, "array index out of range");
		return a[I];
	}

	template <std::size_t I, class T, std::size_t N>
	constexpr const T&& get(const array<T, N>&& a) noexcept
	{
		static_assert(I < N, "array index out of range");
		return mstd::move(a[I]);
	}
}
