#pragma once

#include "memory.hpp" //allocator
#include "type_traits.hpp"
#include "utility.hpp"

namespace mstd
{
	template <class T>
	struct tuple_size;

	template <std::size_t I, class T>
	struct tuple_element;

	template <std::size_t I, class T>
	using tuple_element_t = typename tuple_element<I, T>::type;

	template <class... RestTypes>
	class tuple;

	template <class... Types>
	struct tuple_size<tuple<Types...>> : integral_constant<std::size_t, sizeof...(Types)>
	{
	};

	template <class T>
	struct tuple_size<const T> : integral_constant<std::size_t, tuple_size<T>::value>
	{
	};

	template <class T>
	struct tuple_size<volatile T> : integral_constant<std::size_t, tuple_size<T>::value>
	{
	};

	template <class T>
	struct tuple_size<const volatile T> : integral_constant<std::size_t, tuple_size<T>::value>
	{
	};

	template <class Head, class... Tail>
	struct tuple_element<0, tuple<Head, Tail...>>
	{
		using type = Head;
	};

	template <std::size_t I, class Head, class... Tail>
	struct tuple_element<I, tuple<Head, Tail...>> : tuple_element<I - 1, tuple<Tail...>>
	{
	};

	namespace detail
	{
		template<class src, class dest>
		struct tuple_enable
		{
		};

		template<>
		struct tuple_enable<tuple<>, tuple<> >
		{
			using type = void**;
		};

		template<class Src, class... Types1, class Dest, class... Types2>
		struct tuple_enable<tuple<Src, Types1...>, tuple<Dest, Types2...>>
			: conditional_t<is_convertible<Src, Dest>::value, tuple_enable<tuple<Types1...>, tuple<Types2...>>, tuple_enable<int, int>>
		{
		};
	}

	template <>
	class tuple<>
	{
	public:
		constexpr tuple() noexcept = default;

		constexpr tuple(const tuple&) noexcept = default;

		template <class Alloc>
		constexpr tuple(allocator_arg_t, const Alloc&) noexcept
		{
		}

		template <class Alloc>
		constexpr tuple(allocator_arg_t, const Alloc&, const tuple&) noexcept
		{
		}

		void swap(tuple &) noexcept
		{
		}
	};


	template <class Type, class... RestTypes>
	class tuple<Type, RestTypes...> : private tuple<RestTypes...>
	{
	private:
		using Base_ = tuple<RestTypes...>;

		Base_& getMyRest() noexcept
		{
			return static_cast<Base_&>(*this);
		}

		const Base_& getMyRest() const noexcept
		{
			return static_cast<const Base_&>(*this);
		}

		template <class...>
		friend class tuple;

		template <std::size_t I, class... Types>
		friend constexpr tuple_element_t<I, tuple<Types...>>&	get(tuple<Types...>& t);

		template <std::size_t I, class... Types>
		friend constexpr tuple_element_t<I, tuple<Types...>>&& get(tuple<Types...>&& t);

		template <std::size_t I, class... Types>
		friend constexpr const tuple_element_t<I, tuple<Types...>>& get(const tuple<Types...>& t);

	public:
		constexpr tuple()
			: Base_{}, element_{}
		{
		}

		explicit constexpr tuple(const Type& first, const RestTypes&... rest)
			: Base_{ rest... }, element_{ first }
		{
		}

		template <class U, class... URest, class = detail::tuple_enable<tuple<U, URest...>, tuple<Type, RestTypes...>>>
		explicit constexpr tuple(U&& value, URest&&... rest)
			: Base_{ mstd::forward<URest>(rest)... }, element_{ mstd::forward<U>(value) }
		{
		}

		tuple(const tuple& other) = default;

		tuple(tuple&& other) = default;

		template <class... UTypes/*, class = enable_if_t<sizeof...(RestTypes)+1 == sizeof...(UTypes)>*/>
		constexpr tuple(const tuple<UTypes...>& other)
			: element_{ other.element_ }, Base_{ other.getMyRest() }
		{
		}

		template <class... UTypes/*, class = enable_if_t<sizeof...(RestTypes) + 1 == sizeof...(UTypes)>*/>
		constexpr tuple(tuple<UTypes...>&& other)
			: element_{ mstd::move(other.element_) }, Base_{ mstd::move(other.getMyRest()) }
		{
		}

		template <class U1, class U2/*, class = enable_if_t<sizeof...(RestTypes) + 1 == 2>*/>
		constexpr tuple(const pair<U1, U2>& p)
			: element_{ p.first }, Base_{ p.second }
		{
		}

		template <class U1, class U2>
		constexpr tuple(pair<U1, U2>&& p)
			: element_{ mstd::move(p.first) }, Base_{ mstd::move(p.second) }
		{
		}

		//todo: allocator_arg_t constructoren

		tuple& operator=(const tuple& other)
		{
			if (this != &other)
			{
				element_ = other.element_;
				getMyRest() = other.getMyRest();
			}

			return *this;
		}

		tuple& operator=(tuple&& other)
		{
			if (this != &other)
			{
				element_ = mstd::move(other.element_);
				getMyRest() = other.getMyRest();
			}

			return *this;
		}

		template< class... UTypes >
		tuple& operator=(const tuple<UTypes...>& other)
		{
			element_ = other.element_;
			getMyRest() = other.getMyRest();

			return *this;
		}

		template< class... UTypes >
		tuple& operator=(tuple<UTypes...>&& other)
		{
			element_ = mstd::move(other.element_);
			getMyRest() = mstd::move(other.getMyRest());

			return *this;
		}

		template <class U1, class U2>
		tuple& operator=(const pair<U1, U2>& p)
		{
			static_assert(sizeof...(RestTypes)+1 == 2, "tuple with less or more then 2 elements assigned from pair");
			element_ = p.first;
			getMyRest() = tuple<U2>{ p.second };

			return *this;
		}

		template <class U1, class U2>
		tuple& operator=(pair<U1, U2>&& p) noexcept
		{
			static_assert(sizeof...(RestTypes)+1 == 2, "tuple with less or more then 2 elements assigned from pair");
			element_ = mstd::move(p.first);
			getMyRest() = tuple<U2>{ mstd::move(p.second) };

			return *this;
		}

		void swap(tuple& other) //noexcept
		{
			swap(element_, other.element_);
			getMyRest().swap(other.getMyRest());
		}

	private:
		Type element_;
	};


	namespace detail
	{
		struct ignore_type
		{
			template <class T>
			void operator=(const T&) const
			{
			}
		};
	}

	constexpr detail::ignore_type ignore{};

	template <class... Types>
	constexpr tuple<detail::remove_ref_wrap_t<Types>...>make_tuple(Types&&... args)
	{
		return tuple<detail::remove_ref_wrap_t<Types>...>{ mstd::forward<Types>(args)... };
	}

	template <class... Types>
	constexpr tuple<Types&...> tie(Types&... args)
	{
		return tuple<Types&...>{ args... };
	}

	template <class... Types>
	constexpr tuple<Types&&...> forward_as_tuple(Types&&... args) noexcept
	{
		return tuple<Types&&...>{ mstd::forward<Types>(args)... };
	}

	namespace detail
	{
		template <std::size_t I, class T>
		struct tuple_base;

		template <class T, class... Rest>
		struct tuple_base<0, tuple<T, Rest...>>
		{
			using type = tuple<T, Rest...>;
		};

		template <std::size_t I, class T, class... Rest>
		struct tuple_base<I, tuple<T, Rest...>> : tuple_base<I - 1, tuple<Rest...>>
		{
		};

		template <std::size_t I, class T>
		using tuple_base_t = typename tuple_base<I, T>::type;
	}

	template <std::size_t I, class... Types>
	constexpr tuple_element_t<I, tuple<Types...>>&	get(tuple<Types...>& t)
	{
		return static_cast<detail::tuple_base_t<I, tuple<Types...>>&>(t).element_;
	}

	template <std::size_t I, class... Types>
	constexpr tuple_element_t<I, tuple<Types...>>&& get(tuple<Types...>&& t)
	{
		return mstd::forward<tuple_element_t<I, tuple<Types...>>&&>(static_cast<detail::tuple_base_t<I, tuple<Types...>>&>(t).element_);
	}

	template <std::size_t I, class... Types>
	constexpr const tuple_element_t<I, tuple<Types...>>& get(const tuple<Types...>& t)
	{
		return static_cast<const detail::tuple_base_t<I, tuple<Types...>>&>(t).element_;
	}

	//TODO Type based get functions

	template <class... Types>
	void swap(tuple<Types...>& lhs, tuple<Types...>& rhs) noexcept(noexcept(lhs.swap(rhs)))
	{
		lhs.swap(rhs);
	}

	namespace detail
	{
		template <std::size_t I>
		struct tuple_equal
		{
			template <class tuple1, class tuple2>
			constexpr bool operator()(const tuple1& lhs, const tuple2& rhs) const
			{
				return tuple_equal<I - 1>{}(lhs, rhs) && get<I - 1>(lhs) == get<I - 1>(rhs);
			}
		};

		template<>
		struct tuple_equal<0>
		{
			template <class tuple1, class tuple2>
			constexpr bool operator()(const tuple1&, const tuple2&) const noexcept
			{
				return true;
			}
		};
	}

	template <class... TTypes, class... UTypes>
	constexpr bool operator==(const tuple<TTypes...>& lhs, const tuple<UTypes...>& rhs)
	{
		return detail::tuple_equal<sizeof...(TTypes)>{}(lhs, rhs);
	}

	template <class T1, class T2>
	template <class... A1, class... A2, std::size_t... I1, std::size_t... I2>
	pair<T1, T2>::pair(tuple<A1...>& a1, tuple<A2...>& a2, index_sequence<I1...>, index_sequence<I2...>)
		: first(mstd::get<I1>(mstd::move(a1))...), second(mstd::get<I2>(mstd::move(a2))...)
	{
	}

	template <class T1, class T2>
	template <class... Args1, class... Args2>
	pair<T1, T2>::pair(piecewise_construct_t, tuple<Args1...> first_args, tuple<Args2...> second_args)
		: pair(first_args, second_args, index_sequence_for<Args1...>{}, index_sequence_for<Args2...>{})
	{
	}
}
