#pragma once

#include "exception.hpp"
#include <string>

namespace mstd
{
	struct logic_error : exception
	{
		logic_error(const char* what_arg);

		logic_error(const std::string& what_arg);

		const char* what() const noexcept override;

	private:
		const char* message;
	};

	struct invalid_argument : logic_error
	{
		using logic_error::logic_error;
	};

	struct domain_error : logic_error
	{
		using logic_error::logic_error;
	};

	struct length_error : logic_error
	{
		using logic_error::logic_error;
	};

	struct out_of_range : logic_error
	{
		using logic_error::logic_error;
	};

	struct runtime_error : exception
	{
		runtime_error(const char* what_arg);

		runtime_error(const std::string& what_arg);

		const char* what() const noexcept override;

	private:
		const char* message;
	};

	struct range_error : runtime_error
	{
		using runtime_error::runtime_error;
	};

	struct overflow_error : runtime_error
	{
		using runtime_error::runtime_error;
	};

	struct underflow_error : runtime_error
	{
		using runtime_error::runtime_error;
	};
}
