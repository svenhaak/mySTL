#pragma once

#include "type_traits.hpp"
#include "memory.hpp"

namespace mstd
{
	struct output_iterator_tag
	{
	};

	struct input_iterator_tag
	{
	};

	struct forward_iterator_tag : input_iterator_tag
	{
	};

	struct bidirectional_iterator_tag : forward_iterator_tag
	{
	};

	struct random_access_iterator_tag : bidirectional_iterator_tag
	{
	};

	template<
		class Category,
		class T,
		class Distance = std::ptrdiff_t,
		class Pointer = T*,
		class Reference = T&
	>
	struct iterator
	{
		using value_type = T;
		using difference_type = Distance;
		using pointer = Pointer;
		using reference = Reference;
		using iterator_category = Category;
	};

	namespace detail
	{
		template <class Iterator, class = void>
		struct is_iterator : false_type
		{
		};

		template <class T>
		struct is_iterator<T*> : true_type
		{
		};

		template <class Iterator>
		struct is_iterator < Iterator, experimental::void_t<typename Iterator::difference_type,
			typename Iterator::value_type,
			typename Iterator::pointer,
			typename Iterator::reference,
			typename Iterator::iterator_category >>
			: true_type
		{
		};
	}

	template <class Iterator>
	struct iterator_traits
	{
		static_assert (detail::is_iterator<Iterator>::value, "missing typs for iterator_traits");
		using difference_type = typename Iterator::difference_type;
		using value_type = typename Iterator::value_type;
		using pointer = typename Iterator::pointer;
		using reference = typename Iterator::reference;
		using iterator_category = typename Iterator::iterator_category;
	};

	template <class T>
	struct iterator_traits<T*>
	{
		using difference_type = std::ptrdiff_t;
		using value_type = T;
		using pointer = T*;
		using reference = T&;
		using iterator_category = random_access_iterator_tag;
	};

	template <class T>
	struct iterator_traits<const T*>
	{
		using difference_type = std::ptrdiff_t;
		using value_type = T;
		using pointer = const T*;
		using reference = const T&;
		using iterator_category = random_access_iterator_tag;
	};


	template <class Iterator>
	class reverse_iterator
	{
	protected:
		Iterator current;

	public:
		using value_type = typename iterator_traits<Iterator>::value_type;
		using difference_type = typename iterator_traits<Iterator>::difference_type;
		using pointer = typename iterator_traits<Iterator>::pointer;
		using reference = typename iterator_traits<Iterator>::reference;
		using iterator_category = typename iterator_traits<Iterator>::iterator_category;
		using iterator_type = Iterator;

		reverse_iterator() = default;

		explicit reverse_iterator(Iterator x) : current{ x }
		{
		}

		template <class U>
		reverse_iterator(const reverse_iterator<U>& other)
			: current{ other.current }
		{
		}

		template <class U>
		reverse_iterator& operator=(const reverse_iterator<U>& other)
		{
			current = other.current;
			return *this;
		}

		Iterator base() const
		{
			return current;
		}

		reference operator*() const
		{
			iterator_type tmp = current;
			return *--tmp; //return *mstd::prev()
		}

		pointer operator->() const
		{
			return mstd::addressof(operator*());
		}

		reference operator[](difference_type n) const
		{
			return current[-n - 1];
		}

		reverse_iterator& operator++()
		{
			--current;
			return *this;
		}

		reverse_iterator operator++(int)
		{
			reverse_iterator tmp = *this;
			--current;
			return tmp;
		}

		reverse_iterator& operator--()
		{
			++current;
			return *this;
		}

		reverse_iterator operator--(int)
		{
			reverse_iterator tmp = *this;
			++current;
			return tmp;
		}

		reverse_iterator& operator+=(difference_type n)
		{
			current -= n;
			return *this;
		}

		reverse_iterator operator+(difference_type n) const
		{
			reverse_iterator tmp = *this;
			tmp += n;
			return tmp;
		}

		reverse_iterator& operator-=(difference_type n)
		{
			current += n;
			return *this;
		}

		reverse_iterator operator-(difference_type n) const
		{
			reverse_iterator tmp = *this;
			tmp -= n;
			return tmp;
		}
	};

	template <class Iterator1, class Iterator2>
	bool operator==(const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() == rhs.base();
	}

	template <class Iterator1, class Iterator2>
	bool operator!=(const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() != rhs.base();
	}

	template <class Iterator1, class Iterator2>
	bool operator<(const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() > rhs.base();
	}

	template <class Iterator1, class Iterator2>
	bool operator<=(const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() >= rhs.base();
	}

	template <class Iterator1, class Iterator2>
	bool operator>(const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() < rhs.base();
	}

	template <class Iterator1, class Iterator2>
	bool operator>=(const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() <= rhs.base();
	}

	template <class Iterator>
	reverse_iterator<Iterator>
		operator+(typename reverse_iterator<Iterator>::difference_type n,
			const reverse_iterator<Iterator>& it)
	{
		return it + n;
	}

	template <class Iterator1, class Iterator2>
	auto operator-(const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs) -> decltype(rhs.base() - lhs.base())
	{
		return rhs.base() - lhs.base();
	}

	template <class Iterator>
	reverse_iterator<Iterator> make_reverse_iterator(Iterator i)
	{
		return reverse_iterator<Iterator>(i);
	}

	namespace detail
	{
		template <class ForwardIterator, class Distance>
		void advance_helper(ForwardIterator& it, Distance n, forward_iterator_tag)
		{
			for (; 0 < n; --n)
			{
				++it;
			}
			for (; n < 0; ++n)
			{
				--it;
			}
		}

		template <class RandomAccessIterator, class Distance>
		void advance_helper(RandomAccessIterator& it, Distance n, random_access_iterator_tag)
		{
			it += n;
		}
	}

	template <class InputIterator, class Distance>
	void advance(InputIterator& it, Distance n)
	{
		detail::advance_helper(it, n, typename iterator_traits<InputIterator>::iterator_category{});
	}

	namespace detail
	{
		template<class RandomAccessIterator>
		typename iterator_traits<RandomAccessIterator>::difference_type
			distance_helper(RandomAccessIterator first, RandomAccessIterator last, random_access_iterator_tag)
		{
			return last - first;
		}

		template<class InputIterator>
		typename iterator_traits<InputIterator>::difference_type
			distance_helper(InputIterator first, InputIterator last, input_iterator_tag)
		{
			typename iterator_traits<InputIterator>::difference_type n = 0;
			while (first != last)
			{
				++n;
			}
			return n;
		}
	}

	template<class InputIterator>
	typename iterator_traits<InputIterator>::difference_type
		distance(InputIterator first, InputIterator last)
	{
		return detail::distance_helper(first, last, typename iterator_traits<InputIterator>::iterator_category{});
	}

	template<class ForwardIt>
	ForwardIt next(ForwardIt it, typename iterator_traits<ForwardIt>::difference_type n = 1)
	{
		advance(it, n);
		return it;
	}

	template<class BidirIt>
	BidirIt prev(BidirIt it, typename iterator_traits<BidirIt>::difference_type n = 1)
	{
		advance(it, -n);
		return it;
	}

	template <class Container>
	auto begin(Container& cont) -> decltype (cont.begin())
	{
		return cont.begin();
	}

	template <class Container>
	auto begin(const Container& cont) -> decltype (cont.begin())
	{
		return cont.begin();
	}

	template <class T, std::size_t N>
	constexpr T* begin(T(&array)[N]) noexcept
	{
		return array;
	}

	template <class Container>
	constexpr auto cbegin(const Container& cont) noexcept(noexcept(mstd::begin(cont)))
		-> decltype(mstd::begin(cont))
	{
		return mstd::begin(cont);
	}

	template <class Container>
	auto end(Container& cont) -> decltype(cont.end())
	{
		return cont.end();
	}

	template <class Container>
	auto end(const Container& cont) -> decltype(cont.end())
	{
		return cont.end();
	}

	template <class T, std::size_t N>
	constexpr T* end(T(&array)[N]) noexcept
	{
		return array + N;
	}

	template <class Container>
	auto cend(const Container& cont) noexcept(noexcept(cont.end()))
		-> decltype(mstd::end(cont))
	{
		return mstd::end(cont);
	}

	template <class Container>
	auto rbegin(Container& cont) -> decltype (cont.rbegin())
	{
		return cont.rbegin();
	}

	template <class Container>
	auto rbegin(const Container& cont) -> decltype (cont.rbegin())
	{
		return cont.rbegin();
	}

	template <class T, std::size_t N>
	constexpr reverse_iterator<T*> rbegin(T(&array)[N]) noexcept
	{
		return reverse_iterator<T*>{ array + N };
	}

	template <class Container>
	constexpr auto crbegin(const Container& cont)  noexcept(noexcept(mstd::rbegin(cont)))
		-> decltype(mstd::rbegin(cont))
	{
		return mstd::rbegin(cont);
	}

	template <class Container>
	auto rend(Container& cont) -> decltype(cont.rend())
	{
		return cont.rend();
	}

	template <class Container>
	auto rend(const Container& cont) -> decltype(cont.rend())
	{
		return cont.rend();
	}

	template <class T, std::size_t N>
	constexpr reverse_iterator<T*> rend(T(&array)[N]) noexcept
	{
		return reverse_iterator<T*>{ array };
	}

	template <class Container>
	auto crend(const Container& cont) noexcept(noexcept(cont.rend()))
		-> decltype(mstd::rend(cont))
	{
		return mstd::rend(cont);
	}

	template <class Container>
	constexpr auto size(const Container& c) -> decltype(c.size())
	{
		return c.size();
	}

	template <class T, std::size_t N>
	constexpr std::size_t size(const T(&array)[N]) noexcept
	{
		return N;
	}

	template <class Container>
	constexpr auto empty(const Container& c) -> decltype(c.empty())
	{
		return c.empty();
	}

	template <class T, std::size_t N>
	constexpr bool empty(const T(&array)[N]) noexcept
	{
		return false;
	}

	template <class E>
	constexpr bool empty(std::initializer_list<E> il) noexcept
	{
		return il.size() == 0;
	}

	template <class Container>
	constexpr auto data(Container& c) -> decltype(c.data())
	{
		return c.data();
	}

	template <class Container>
	constexpr auto data(const Container& c) -> decltype(c.data())
	{
		return c.data();
	}

	template <class T, std::size_t N>
	constexpr T* data(T(&array)[N]) noexcept
	{
		return static_cast<T*>(array);
	}

	template <class E>
	constexpr const E* data(std::initializer_list<E> il) noexcept
	{
		return il.begin();
	}

	template <class I>
	class move_iterator
	{
	public:
		using iterator_type = I;
		using difference_type = typename iterator_traits<iterator_type>::difference_type;
		using pointer = iterator_type;
		using value_type = typename iterator_traits<iterator_type>::value_type;
		using iterator_category = typename iterator_traits<iterator_type>::iterator_category;
		using reference = conditional_t<
			is_reference<typename iterator_traits<iterator_type>::reference>::value,
			add_rvalue_reference_t<remove_reference_t<typename iterator_traits<iterator_type>::reference>>,
			typename iterator_traits<iterator_type>::reference>;



		move_iterator() = default;

		explicit move_iterator(I i)
			: current_{ i }
		{
		}

		template <class I2>
		move_iterator(const move_iterator<I2>& other)
			: current_{ other.current_ }
		{
		}

		template <class I2>
		move_iterator& operator=(const move_iterator<I2>& other)
		{
			current_ = other.current_;
			return *this;
		}

		iterator_type base() const
		{
			return current_;
		}

		reference operator*() const
		{
			return static_cast<reference>(*current_);
		}

		pointer operator->() const
		{
			return current_;
		}

		decltype(auto) operator[](difference_type n) const // todo check if return type is better specified
		{
			return mstd::move(current_[n]);
		}

		move_iterator& operator++()
		{
			++current_;
			return *this;
		}

		move_iterator& operator--()
		{
			--current_;
			return *this;
		}

		move_iterator operator++(int)
		{
			move_iterator ret = *this;
			++current_;
			return ret;
		}

		move_iterator operator--(int)
		{
			move_iterator ret = *this;
			--current_;
			return ret;
		}

		//only random acces iterators and higher

		move_iterator operator+(difference_type n) const
		{
			return{ current_ + n };
		}

		move_iterator operator-(difference_type n) const
		{
			return{ current_ - n };
		}

		move_iterator& operator+=(difference_type n)
		{
			current_ += n;
			return *this;
		}

		move_iterator& operator-=(difference_type n)
		{
			current_ -= n;
			return *this;
		}

	private:
		I current_;
	};

	template <class I1, class I2>
	bool operator==(const move_iterator<I1>& lhs, const move_iterator<I2>& rhs)
	{
		return lhs.base() == rhs.base();
	}

	template <class I1, class I2>
	bool operator!=(const move_iterator<I1>& lhs, const move_iterator<I2>& rhs)
	{
		return lhs.base() != rhs.base();
	}

	template <class I1, class I2>
	bool operator<(const move_iterator<I1>& lhs, const move_iterator<I2>& rhs)
	{
		return lhs.base() < rhs.base();
	}

	template <class I1, class I2>
	bool operator>(const move_iterator<I1>& lhs, const move_iterator<I2>& rhs)
	{
		return lhs.base() > rhs.base();
	}

	template <class I1, class I2>
	bool operator<=(const move_iterator<I1>& lhs, const move_iterator<I2>& rhs)
	{
		return lhs.base() <= rhs.base();
	}

	template <class I1, class I2>
	bool operator>=(const move_iterator<I1>& lhs, const move_iterator<I2>& rhs)
	{
		return lhs.base() >= rhs.base();
	}

	template <class I>
	move_iterator<I> operator+(typename move_iterator<I>::difference_type n, const move_iterator<I>& it)
	{
		return it + n;
	}

	template <class I1, class I2>
	auto operator-(const move_iterator<I1>& lhs, const move_iterator<I2>& rhs)
		-> decltype(lhs.base() - rhs.base())
	{
		return lhs.base() - rhs.base();
	}

	template <class I>
	move_iterator<I> make_move_iterator(I it)
	{
		return move_iterator<I>{ it };
	}

	template <class Container>
	class back_insert_iterator
	{
	public:
		using container_type = Container;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;
		using iterator_category = output_iterator_tag;

		explicit back_insert_iterator(container_type& c)
			: container{ mstd::addressof(c) }
		{
		}

		back_insert_iterator& operator=(const typename container_type::value_type& value)
		{
			container->push_back(value);
			return *this;
		}

		back_insert_iterator& operator=(typename container_type::value_type&& value)
		{
			container->push_back(mstd::move(value));
			return *this;
		}

		back_insert_iterator& operator*()
		{
			return *this;
		}

		back_insert_iterator& operator++()
		{
			return *this;
		}

		back_insert_iterator& operator++(int)
		{
			return *this;
		}

	protected:
		container_type* container;
	};

	template <class Container>
	back_insert_iterator<Container> back_inserter(Container& c)
	{
		return back_insert_iterator<Container>{ c };
	}
	
	template <class Container>
	class front_insert_iterator
	{
	public:
		using container_type = Container;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;
		using iterator_category = output_iterator_tag;

		explicit front_insert_iterator(container_type& c)
			: container{ mstd::addressof(c) }
		{
		}

		front_insert_iterator& operator=(const typename container_type::value_type& value)
		{
			container->push_front(value);
			return *this;
		}

		front_insert_iterator& operator=(typename container_type::value_type&& value)
		{
			container->push_front(mstd::move(value));
			return *this;
		}

		front_insert_iterator& operator*()
		{
			return *this;
		}

		front_insert_iterator& operator++()
		{
			return *this;
		}

		front_insert_iterator& operator++(int)
		{
			return *this;
		}

	protected:
		container_type* container;
	};

	template <class Container>
	front_insert_iterator<Container> front_inserter(Container& c)
	{
		return front_insert_iterator<Container>{ c };
	}

	template <class Container>
	class insert_iterator
	{
	public:
		using container_type = Container;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;
		using iterator_category = output_iterator_tag;

		explicit insert_iterator(container_type& c, typename container_type::iterator i)
			: container{ mstd::addressof(c) }, iter{ i }
		{
		}

		insert_iterator operator=(const typename container_type::value_type& value)
		{
			iter = container->insert(iter, value);
			++iter;
			return *this;
		}

		insert_iterator& operator=(typename container_type::value_type&& value)
		{
			iter = container->insert(iter, mstd::move(value));
			++iter;
			return *this;
		}

		insert_iterator& operator*()
		{
			return *this;
		}

		insert_iterator& operator++()
		{
			return *this;
		}

		insert_iterator& operator++(int)
		{
			return *this;
		}

	protected:
		container_type* container;
		typename container_type::iterator iter;
	};

	template <class Container>
	insert_iterator<Container> inserter(Container& c, typename Container::iterator i)
	{
		return insert_iterator<Container> { c, i };
	}
}
