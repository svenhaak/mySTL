#pragma once

#include <initializer_list>
#include <ios> //streamoff, streampos
#include "alloc_wrapper.hpp"
#include "array.hpp"
#include "cstring.h"
#include "compressed_pair.hpp"
#include <memory> //allocator
#include "iterator.hpp"

#ifndef EOF
#define EOF -1
#endif

namespace mstd
{
	namespace detail
	{
		template <class CharT, class IntT, class PosT>
		struct unicode_char_traits
		{
			using char_type = CharT;
			using int_type = IntT;
			using off_type = std::streamoff;
			using pos_type = PosT;
			using state_type = std::mbstate_t;

			static void assign(char_type& dest, const char_type& src) noexcept
			{
				dest = src;
			}

			static char_type* assign(char_type* p, std::size_t count, char_type a)
			{
				for (std::size_t i = 0; i < count; ++i)
				{
					assign(p[i], a);
				}
				return p;
			}

			static constexpr bool eq(char_type lhs, char_type rhs) noexcept
			{
				return lhs == rhs;
			}

			static constexpr bool lt(char_type lhs, char_type rhs) noexcept
			{
				return lhs < rhs;
			}

			static char_type* copy(char_type* dest, const char_type* src, std::size_t count)
			{
				for (size_t i = 0; i < count; ++i)
				{
					assign(dest[i], src[i]);
				}
				return dest;
			}

			static char_type* move(char_type* dest, const char_type* src, std::size_t count)
			{
				if (src < dest)
				{
					for (; count > 0; --count)
					{
						assign(dest[count - 1], src[count - 1]);
					}
				}
				else if (src > dest)
				{
					copy(dest, src, count);
				}
				return dest;
			}

			static int compare(const char_type* lhs, const char_type* rhs, std::size_t count)
			{
				for (; count > 0; --count, ++lhs, ++rhs)
				{
					if (!eq(*lhs, *rhs))
						return lt(*lhs, *rhs) ? -1 : +1;
				}
				return 0;
			}

			static std::size_t length(const char_type* s)
			{
				std::size_t result = 0;
				while (!eq(*s, char_type()))
				{
					++result; ++s;
				}
				return result;
			}

			static const char_type* find(const char_type* p, std::size_t count, const char_type& ch)
			{
				for (; count > 0; --count, ++p)
				{
					if (eq(*p, ch))
						return p;
				}
				return nullptr;
			}

			static constexpr char_type to_char_type(int_type c) noexcept
			{
				return static_cast<char_type>(c);
			}

			static constexpr int_type to_int_type(char_type c) noexcept
			{
				return static_cast<int_type>(c);
			}

			static constexpr bool eq_int_type(int_type lhs, int_type rhs) noexcept
			{
				return lhs == rhs;
			}

			static constexpr int_type eof() noexcept
			{
				return static_cast<int_type>(EOF);
			}

			static constexpr int_type not_eof(int_type e) noexcept
			{
				return !eq_int_type(e, eof()) ? e : eof() - 3;
			}
		};
	}

	template <class CharT>
	struct char_traits;

	template <>
	struct char_traits<char>
	{
		using char_type = char;
		using int_type = int;
		using off_type = std::streamoff;
		using pos_type = std::streampos;
		using state_type = std::mbstate_t;

		static void assign(char_type& dest, const char_type& src) noexcept
		{
			dest = src;
		}

		static char_type* assign(char_type* p, std::size_t count, char_type a)
		{
			for (size_t i = 0; i < count; ++i) //memset
			{
				assign(p[i], a);
			}
			return p;
		}

		static constexpr bool eq(char_type lhs, char_type rhs) noexcept
		{
			return lhs == rhs;
		}

		static constexpr bool lt(char_type lhs, char_type rhs) noexcept
		{
			return static_cast<unsigned char>(lhs) < static_cast<unsigned char>(rhs);
		}

		static char_type* move(char_type* dest, const char_type* src, std::size_t count)
		{
			return static_cast<char_type*>(mstd::memmove(dest, src, count));
		}

		static char_type* copy(char_type* dest, const char_type* src, std::size_t count)
		{
			return static_cast<char_type*>(mstd::memcpy(dest, src, count));
		}

		static int compare(const char_type* lhs, const char_type* rhs, std::size_t count)
		{
			return mstd::memcmp(lhs, rhs, count);
		}

		static std::size_t length(const char_type* s)
		{
			return mstd::strlen(s);
		}

		static const char_type* find(const char_type* p, std::size_t count, const char_type& ch)
		{
			return static_cast<const char*>(mstd::memchr(p, ch, count));
		}

		static constexpr char_type to_char_type(int_type c) noexcept
		{
			return static_cast<char_type>(c);
		}

		static constexpr int_type to_int_type(char_type c) noexcept
		{
			return static_cast<int_type>(c);
		}

		static constexpr bool eq_int_type(int_type lhs, int_type rhs) noexcept
		{
			return lhs == rhs;
		}

		static constexpr int_type eof() noexcept
		{
			return EOF;
		}

		static constexpr int_type not_eof(int_type e) noexcept
		{
			return !eq_int_type(e, eof()) ? e : eof() - 3;
		}
	};

	template <>
	struct char_traits<wchar_t> //TODO implement witch C functions (wmemcomp & co)
		: detail::unicode_char_traits<wchar_t, std::wint_t, std::wstreampos>
	{
	};

	template <>
	struct char_traits<char16_t>
		: detail::unicode_char_traits<char16_t, std::uint_least16_t, std::u16streampos>
	{
	};

	template <>
	struct char_traits<char32_t>
		: detail::unicode_char_traits<char32_t, std::uint_least32_t, std::u32streampos>
	{
	};


	template <class CharT, class Traits = char_traits<CharT>, class Allocator = allocator<CharT>>
	class basic_string
	{
	private:
		using wrapper = detail::alloc_wrapper<Allocator>;
	public:
		using traits_type = Traits;
		using value_type = typename traits_type::char_type;
		using allocator_type = Allocator;
		using size_type = typename wrapper::size_type;
		using difference_type = typename wrapper::difference_type;
		using reference = value_type&;
		using const_reference = const value_type&;
		using pointer = typename wrapper::pointer;
		using const_pointer = typename wrapper::const_pointer;
		using iterator = value_type*;
		using const_iterator = const value_type*;
		using reverse_iterator = mstd::reverse_iterator<iterator>;
		using const_reverse_iterator = mstd::reverse_iterator<const_iterator>;


		static const size_type npos = static_cast<size_type>(-1);

		basic_string() noexcept(noexcept(allocator_type{}))
			//: short_store_{}
		{
			_set_long(false);
			_set_new_length(0);
		}

		explicit basic_string(const allocator_type& alloc) noexcept(noexcept(allocator_type{}))
			: size_and_alloc_{ detail::construct_second, alloc }//, short_store_{}
		{
			_set_long(false);
			_set_new_length(0);
		}

		basic_string(size_type count, value_type ch, const allocator_type& alloc = allocator_type{})
			: size_and_alloc_{ detail::construct_both, count, alloc }//, short_store_{}  TODO required?
		{
			_init_grow(count);
			traits_type::assign(data(), count, ch);
			_set_new_length(count);
		}

		basic_string(const basic_string& other, size_type pos,
			size_type count = npos, const allocator_type& alloc = allocator_type{})
			: size_and_alloc_{ detail::construct_second, alloc }
		{
			if (pos > other.size())
				throw out_of_range{ "position is out of string" };
			if (count > other.size() - pos)
				count = other.size() - pos;
			_init_grow(count);
			traits_type::copy(data(), other.data() + pos, count);
			_set_new_length(count);
		}

		basic_string(const value_type* s, size_type count, const allocator_type& alloc = allocator_type{})
			: size_and_alloc_{ detail::construct_second, alloc }
		{
			_init_grow(count);
			traits_type::copy(data(), s, count);
			_set_new_length(count);
		}

		basic_string(const value_type* s, const allocator_type& alloc = allocator_type{})
			: basic_string{ s, traits_type::length(s), alloc }
		{
		}

		template <class InputIterator>
		basic_string(InputIterator first, InputIterator last, const allocator_type& alloc = allocator_type{})
			: size_and_alloc_{ detail::construct_second, alloc }
		{
			size_type count = static_cast<size_type>(mstd::distance(first, last));
			_init_grow(count);
			for (pointer p = data(); first != last; ++first, ++p)
			{
				traits_type::assign(*p, *first);
			}
			_set_new_length(count);
		}

		basic_string(const basic_string& other)
			: basic_string{ other, other.size_and_alloc_.second().select_on_container_copy_construction() }
		{
		}

		basic_string(const basic_string& other, const allocator_type& alloc)
			: size_and_alloc_{ detail::construct_second, alloc }
		{
			_init_grow(other.size());
			traits_type::copy(data(), other.data(), other.size());
			_set_new_length(other.size());
		}

		basic_string(basic_string&& other) noexcept
			: basic_string{ mstd::move(other), allocator_type{} }
		{
		}

		basic_string(basic_string&& other, const allocator_type& alloc) noexcept
			: size_and_alloc_{ mstd::move(other.size_and_alloc_) }
		{
			if (other._is_long())
			{
				new (&long_store_) long_type{ mstd::move(other.long_store_) };
				_set_long(true);
				other._set_long(false);
			}
			else
			{
				short_store_ = mstd::move(other.short_store_);
				_set_long(false);
			}
			other.clear();
		}

		basic_string(std::initializer_list<value_type> li, const allocator_type& alloc = allocator_type{})
			: basic_string{ li.begin(), li.end(), alloc }
		{
		}

		~basic_string()
		{
			if (_is_long())
			{
				_destroy_long();
				_set_long(false);
			}
			clear();
		}

		basic_string& operator=(const basic_string& str)
		{
			_grow(str.size());
			traits_type::copy(data(), str.data(), str.size());
			_set_new_length(str.size());
			return *this;
		}

		basic_string& operator=(basic_string&& str)
		{
			if (this->_is_long())
			{
				_destroy_long();
			}

			size_and_alloc_ = mstd::move(str.size_and_alloc_);

			if (str._is_long())
			{
				new (&long_store_) long_type{ mstd::move(str.long_store_) };
				_set_long(true);
				str._set_long(false);
			}
			else
			{
				new (&short_store_) decltype(short_store_){ mstd::move(str.short_store_) };
				_set_long(false);
			}
			str.clear();
			return *this;
		}

		basic_string& operator=(const value_type* str)
		{
			size_type sz = traits_type::length(str);
			_grow(sz);
			traits_type::copy(data(), str, sz);
			_set_new_length(sz);
			return *this;
		}

		basic_string& operator=(value_type ch)
		{
			//_grow(1); not required because of the sso
			traits_type::assign(*data(), ch);
			_set_new_length(1);
			return *this;
		}

		basic_string& operator=(std::initializer_list<value_type> ilist)
		{
			_grow(ilist.size());
			traits_type::copy(data(), ilist.begin(), ilist.size());
			_set_new_length(ilist.size());
			return *this;
		}

		basic_string& assign(size_type count, value_type ch)
		{
			_grow(count);
			traits_type::assign(data(), count, ch);
			_set_new_length(count);
			return *this;
		}

		basic_string& assign(const basic_string& str)
		{
			*this = str;
			return *this;
		}

		basic_string& assign(const basic_string& str, size_type pos, size_type count = npos)
		{
			if (pos > str.size())
				throw out_of_range{ "position is greater then string size" };
			count = (count == npos) ? size() : count;
			_grow(count);
			traits_type::copy(data(), str.data() + pos, count == npos ? size() : count);
			_set_new_length(count);
			return *this;
		}

		basic_string& assign(basic_string&& str)
		{
			*this = mstd::move(str);
			return *this;
		}

		basic_string& assign(const value_type* s, size_type count)
		{
			_grow(count);
			traits_type::copy(data(), s, count);
			_set_new_length(count);
			return *this;
		}

		basic_string& assign(const value_type* s)
		{
			return assign(s, traits_type::length(s));
		}

		template <class InputIt>
		basic_string& assign(InputIt first, InputIt last)
		{
			size_type count = static_cast<size_type>(mstd::distance(first, last));
			_grow(count);
			for (pointer p = data(); first != last; ++first, ++p)
			{
				traits_type::assign(*p, *first);
			}
			_set_new_length(count);
		}

		basic_string& assign(std::initializer_list<value_type> ilist)
		{
			*this = ilist;
			return *this;
		}

		allocator_type get_allocator() const
		{
			return size_and_alloc_.second();
		}

		reference at(size_type pos)
		{
			if (pos >= size())
				throw out_of_range{ "basic_string::at" };
			return data()[pos];
		}

		const_reference at(size_type pos) const
		{
			if (pos >= size())
				throw out_of_range{ "basic_string::at" };
			return data()[pos];
		}

		reference operator[](size_type pos)
		{
			return data()[pos];
		}

		const_reference operator[](size_type pos) const
		{
			return data()[pos];
		}

		reference front()
		{
			return (*this)[0];
		}

		const_reference front() const
		{
			return (*this)[0];
		}

		reference back()
		{
			return (*this)[size() - 1];
		}

		const_reference back() const
		{
			return (*this)[size() - 1];
		}
        
        value_type* data()
        {
            return _is_long() ? mstd::addressof(*long_store_.begin_) : short_store_.data();
        }

		const value_type* data() const
		{
			return _is_long() ? mstd::addressof(*long_store_.begin_) : short_store_.data();
		}

		const value_type* c_str() const
		{
			return data();
		}

		iterator begin() noexcept
		{
			return iterator{ data() };
		}

		const_iterator begin() const noexcept
		{
			return cbegin();
		}

		const_iterator cbegin() const noexcept
		{
			return const_iterator{ data() };
		}

		iterator end() noexcept
		{
			return iterator{ data() + size() };
		}

		const_iterator end() const noexcept
		{
			return cend();
		}

		const_iterator cend() const noexcept
		{
			return const_iterator{ data() + size() };
		}

		reverse_iterator rbegin() noexcept
		{
			return reverse_iterator{ end() };
		}

		const_reverse_iterator rbegin() const noexcept
		{
			return crbegin();
		}

		const_reverse_iterator crbegin() const noexcept
		{
			return reverse_iterator{ cend() };
		}

		reverse_iterator rend() noexcept
		{
			return reverse_iterator{ begin() };
		}

		const_reverse_iterator rend() const noexcept
		{
			return crend();
		}

		const_reverse_iterator crend() const noexcept
		{
			return reverse_iterator{ cbegin() };
		}

		bool empty() const noexcept
		{
			return size() == 0u;
		}

		size_type size() const noexcept
		{
			return size_and_alloc_.first();
		}

		size_type length() const noexcept
		{
			return size();
		}

		size_type max_size() const noexcept
		{
			//return npos; todo
			return 5000u;
		}

		size_type capacity() const noexcept
		{
			return (_is_long() ? long_store_.capacity_ : short_store_.size()) - 1;
		}

		void reserve(size_type new_cap = 0)
		{
			if (new_cap > capacity())
			{
				_grow(new_cap);
			}
			else if (new_cap < size())
			{
				shrink_to_fit();
			}
			else if (new_cap < capacity())
			{
				_shrink(new_cap);
			}
		}

		void shrink_to_fit()
		{
			_shrink(size());
		}

		void clear() noexcept
		{
			_set_new_length(0);
		}

		basic_string& insert(size_type index, size_type count, value_type ch)
		{
			_grow(size() + count);
			traits_type::move(data() + index + count, data() + index, size() - index);
			traits_type::assign(data() + index, count, ch);
			_set_new_length(size() + count);
			return *this;
		}

		basic_string& insert(size_type index, const value_type* s)
		{
			return insert(index, s, traits_type::length(s));
		}

		basic_string& insert(size_type index, const value_type* s, size_type count)
		{
			_grow(size() + count);
			traits_type::move(data() + index + count, data() + index, size() - index);
			traits_type::copy(data() + index, s, count);
			_set_new_length(size() + count);
			return *this;
		}

		basic_string& insert(size_type index, const basic_string& str)
		{
			return insert(index, str.data(), str.size());
		}

		basic_string& insert(size_type index, const basic_string& str, size_type index_str, size_type count = npos)
		{
			if (count > str.size())
				count = str.size();
			return insert(index, str.data() + index_str, count);
		}

		iterator insert(const_iterator pos, value_type ch)
		{
			return insert(pos - cbegin(), 1, ch);
		}

		iterator insert(const_iterator pos, size_type count, value_type ch)
		{
			const_iterator index = pos - cbegin();
			insert(index, count, ch);
			return begin() + index;
		}

		template <class InputIterator>
		iterator insert(const_iterator pos, InputIterator first, InputIterator last)
		{
			const_iterator index = pos - cbegin(); //TODO testen
			while (first != last)
				insert(pos++, *(first++));
			return cbegin() + index;
		}

		iterator insert(const_iterator pos, std::initializer_list<value_type> ilist)
		{
			return insert(pos, ilist.begin(), ilist.size());
		}

		basic_string& erase(size_type index = 0, size_type count = npos)
		{
			if (count < size() - index)
				count = size() - index;
			traits_type::move(data() + index, data() + index + count, size() - index - count);
			_set_new_length(size() - count);
			return *this;
		}

		iterator erase(const_iterator position)
		{
			size_type index = position - cbegin();
			//erase(index);
			traits_type::move(data() + index, data() + index + 1, size() - index - 1);
			_set_new_length(size() - 1);
			return begin() + index;
		}

		iterator erase(const_iterator first, const_iterator last)
		{
			size_type index = first - cbegin();
			size_type count = last - first;
			//erase(index, count);
			traits_type::move(data() + index, data() + index + count, size() - index - count);
			_set_new_length(size() - count);
			return begin() + index;
		}

		void push_back(value_type ch)
		{
			_grow(size() + 1);
			traits_type::assign(data()[size()], ch);
			_set_new_length(size() + 1);
		}

		void pop_back()
		{
			_set_new_length(size() - 1);
		}

		basic_string& append(size_type count, value_type ch)
		{
			_grow(size() + count);
			traits_type::assign(data() + size(), count, ch);
			_set_new_length(size() + count);
			return *this;
		}

		basic_string& append(const basic_string& str)
		{
			return append(str.data(), str.size());
		}

		basic_string& append(const basic_string& str, size_type pos, size_type count = npos)
		{
			if (pos >= str.size())
				throw out_of_range{ "position is out of basic_string" };
			count = mstd::min(count, size() - pos);
			return append(str.data() + pos, count);
		}

		basic_string& append(const value_type* s, size_type count)
		{
			_grow(size() + count);
			traits_type::copy(data() + size(), s, count);
			_set_new_length(size() + count);
			return *this;
		}

		basic_string& append(const value_type* s)
		{
			return append(s, traits_type::length(s));
		}

		template <class InputIterator>
		basic_string& append(InputIterator first, InputIterator last)
		{
			size_type count = mstd::distance(first, last);
			_grow(size() + count);
			value_type* p = data() + size();
			for (; first != last; (void)++first, ++p)
			{
				*p = *first;
			}
			_set_new_length(size() + count);
			return *this;
		}

		basic_string& append(std::initializer_list<value_type> ilist)
		{
			return append(ilist.begin(), ilist.end());
		}

		basic_string& operator+=(const basic_string& str)
		{
			return append(str.data(), str.size());
		}

		basic_string& operator+=(value_type ch)
		{
			return append(1, ch);
		}

		basic_string& operator+=(const value_type* s)
		{
			return append(s, traits_type::length(s));
		}

		basic_string& operator+=(std::initializer_list<value_type> ilist)
		{
			return append(ilist.begin(), ilist.end());
		}

		int compare(const basic_string& str) const
		{
			return compare(0, str.size(), str.data());
		}

		int compare(size_type pos, size_type count, const basic_string& str) const
		{
			return compare(pos, count, str.data());
		}

		int compare(size_type pos1, size_type count1, const basic_string& str,
			size_type pos2, size_type count2 = npos) const
		{
			return compare(pos1, count1, str.data(), mstd::min(str.size(), count2));
		}

		int compare(const value_type* s) const
		{
			return compare(0, size(), s, traits_type::length(s));
		}

		int compare(size_type pos, size_type count, const value_type* s) const
		{
			return compare(pos, count, s, traits_type::length(s));
		}

		int compare(size_type pos1, size_type count1, const value_type* s, size_type count2) const
		{
			if (pos1 > size())
				throw out_of_range{ "position is out of string" };

			if (size() - pos1 < count1)
				count1 = size() - pos1;

			int result = traits_type::compare(data() + pos1, s, mstd::min(count1, count2));
			if (result == 0)
			{
				if (count1 < count2)
					result = -1;
				else if (count1 > count2)
					result = +1;
			}
			return result;
		}

		basic_string& replace(size_type pos, size_type count, const basic_string& str)
		{
			return replace(pos, count, str.data(), str.size());
		}

		basic_string& replace(const_iterator first, const_iterator last, const basic_string& str)
		{
			return replace(first - cbegin(), last - first, str);
		}

		basic_string& replace(size_type pos, size_type count, const basic_string& str,
			size_type pos2, size_type count2 = npos)
		{
			if (pos2 > str.size())
				throw out_of_range{ "pos2 is out of string (str)" };
			if (count2 == npos || pos2 + count2 > str.size()) //comparison with npos to avoid integer overflow
				count2 = str.size() - pos2;
			return replace(pos, count, str.data() + pos2, count2);
		}

		template <class InputIterator>
		basic_string& replace(const_iterator first, const_iterator last, InputIterator first2, InputIterator last2)
		{
			return replace(first, last, basic_string{ first2, last2 }); //TODO slow implementation
		}

		basic_string& replace(size_type pos, size_type count, const value_type* cstr, size_type count2)
		{
			if (pos + count > size())
				count = size() - pos;
			size_type sz = size() + count2 - count;
			_grow(sz);
			if (pos + count < size())
				traits_type::move(data() + pos + count2, data() + pos + count, size() - pos - count);

			traits_type::copy(data() + pos, cstr, count2);
			_set_new_length(sz);
			return *this;
		}

		basic_string& replace(const_iterator first, const_iterator last, const value_type* cstr, size_type count2)
		{
			return replace(first - cbegin(), last - first, cstr, count2);
		}

		basic_string& replace(size_type pos, size_type count, const value_type* cstr)
		{
			return replace(pos, count, cstr, traits_type::length(cstr));
		}

		basic_string& replace(const_iterator first, const_iterator last, const value_type* cstr)
		{
			return replace(first, last, cstr, traits_type::length(cstr));
		}

		basic_string& replace(size_type pos, size_type count, size_type count2, value_type ch)
		{
			if (pos + count > size())
				count = size() - pos;
			size_type sz = size() + count2 - count;
			_grow(sz);
			if (pos + count < size())
				traits_type::move(data() + pos + count2, data() + pos + count, size() - pos - count);

			traits_type::assign(data() + pos, count2, ch);
			_set_new_length(sz);
			return *this;
		}

		basic_string& replace(const_iterator first, const_iterator last, size_type count2, value_type ch)
		{
			return replace(first - cbegin(), last - first, count2, ch);
		}

		basic_string& replace(const_iterator first, const_iterator last, std::initializer_list<CharT> ilist)
		{
			return replace(first, last, ilist.begin(), ilist.size());
		}

		basic_string substr(size_type pos = 0, size_type count = npos) const
		{
			return basic_string{ *this, pos, count, get_allocator() };
		}

		size_type copy(value_type* dest, size_type count, size_type pos = 0) const
		{
			if (pos > size())
				throw out_of_range{ "position is out of string" };
			if (count > size())
				count = size();
			traits_type::copy(dest, data() + pos, count);
			return count;
		}

		void resize(size_type count)
		{
			resize(count, value_type{});
		}

		void resize(size_type count, value_type ch)
		{
			if (count < size())
				_set_new_length(count);
			else if (count > size())
			{
				_grow(count);
				traits_type::assign(data() + size(), count - size(), ch);
				_set_new_length(count);
			}
		}

		void swap(basic_string& other)
		{
			if (_is_long())
			{
				if (other._is_long())
				{
					using mstd::swap;
					swap(long_store_, other.long_store_);
				}
				else
				{
					long_type tmp_long = mstd::move(long_store_);
					traits_type::copy(short_store_.data(), other.short_store_.data(), other.size() + 1);
					other.long_store_ = mstd::move(tmp_long);
					_set_long(false);
					other._set_long(true);
				}
			}
			else
			{
				if (other._is_long())
				{
					long_type tmp_long = mstd::move(other.long_store_);
					traits_type::copy(other.short_store_.data(), short_store_.data(), size() + 1);
					long_store_ = mstd::move(tmp_long);
					_set_long(true);
					other._set_long(false);
				}
				else
				{
					value_type tmp_char;
					for (size_type i = 0; i < short_store_.size(); ++i)
					{
						traits_type::assign(tmp_char, short_store_.data()[i]);
						traits_type::assign(short_store_.data()[i], other.short_store_.data()[i]);
						traits_type::assign(other.short_store_.data()[i], tmp_char);
					}
				}
			}
			using mstd::swap;
			swap(size_and_alloc_.first(), other.size_and_alloc_.first());
		}

		size_type find(const basic_string& str, size_type pos = 0) const noexcept
		{
			return find(str.data(), pos, str.size());
		}

		size_type find(const value_type* s, size_type pos, size_type count) const
		{
			size_type sz = size() - count + 1;
			const value_type * const d = data();
			for (size_type i = pos; i < sz; ++i)
			{
				if (traits_type::compare(d + i, s, count) == 0)
					return i;
			}
			return npos;
		}

		size_type find(const value_type* s, size_type pos = 0) const
		{
			return find(s, pos, traits_type::length(s));
		}

		size_type find(value_type ch, size_type pos = 0) const
		{
			const value_type * const d = data();
			for (size_type i = 0; i < size(); ++i)
			{
				if (traits_type::eq(d[i], ch))
					return i;
			}
			return npos;
		}

		size_type rfind(const basic_string& str, size_type pos = npos) const noexcept
		{

		}

		size_type rfind(const value_type* s, size_type pos, size_type count) const
		{
			const value_type * const d = data();
			if (pos > size())
				pos = size();

			for (size_type i = pos - count; i + 1 > 0; --i)
			{
				if (traits_type::compare(d + i, s, count) == 0)
					return i;
			}
			return npos;
		}

		size_type rfind(const value_type* s, size_type pos = npos) const
		{
			return rfind(s, pos, traits_type::length(s));
		}

		size_type rfind(value_type ch, size_type pos = npos) const
		{
			const value_type * const d = data();

			if (pos > size())
				pos = size();
			for (size_type i = pos - 1; i + 1 > 0; --i)
			{
				if (traits_type::eq(d[i], ch))
					return i;
			}
			return npos;
		}

		size_type find_first_of(const basic_string& str, size_type pos = 0) const noexcept
		{
			return find_first_of(str.data(), pos);
		}

		size_type find_first_of(const value_type* s, size_type pos, size_type count) const
		{
			const value_type * const d = data();
			for (size_type i = pos; i < size(); ++i)
			{
				if (_is_in(d[i], s, count))
					return i;
			}
			return npos;
		}

		size_type find_first_of(const value_type* s, size_type pos = 0) const
		{
			return find_first_of(s, pos, traits_type::length(s));
		}

		size_type find_first_of(value_type ch, size_type pos = 0) const
		{
			return find(ch, pos);
		}

		size_type find_first_not_of(const basic_string& str, size_type pos = 0) const noexcept
		{
			return find_first_not_of(str.data(), pos, str.size());
		}

		size_type find_first_not_of(const value_type* s, size_type pos, size_type count) const
		{
			const value_type * const d = data();
			for (size_type i = pos; i < size(); ++i)
			{
				if (!_is_in(d[i], s, count))
					return i;
			}
			return npos;
		}

		size_type find_first_not_of(const value_type* s, size_type pos = 0) const
		{
			return find_first_not_of(s, pos, traits_type::length(s));
		}

		size_type find_first_not_of(value_type ch, size_type pos = 0) const
		{
			const value_type * const d = data();
			for (size_type i = pos; i < size(); ++i)
			{
				if (!traits_type::eq(d[i], ch))
					return i;
			}
			return npos;
		}

		size_type find_last_of(const value_type& str, size_type pos = npos) const noexcept
		{
			return find_last_of(str.data(), pos, str.size());
		}

		size_type find_last_of(const value_type* s, size_type pos, size_type count) const
		{
			const value_type * const d = data();
			if (pos > size())
				pos = size();

			for (size_type i = pos - count; i + 1 > 0; --i)
			{
				if (_is_in(d[i], s, count))
					return i;
			}
			return npos;
		}

		size_type find_last_of(const value_type* s, size_type pos = npos) const
		{
			return find_last_of(s, pos, traits_type::length(s));
		}

		size_type find_last_of(value_type ch, size_type pos = npos) const
		{
			return rfind(ch, pos);
		}

		size_type find_last_not_of(const basic_string& str, size_type pos = npos) const noexcept
		{
			return find_last_not_of(str.data(), pos, str.size());
		}

		size_type find_last_not_of(const value_type* s, size_type pos, size_type count) const
		{
			const value_type * const d = data();
			if (pos > size())
				pos = size();

			for (size_type i = pos - count; i + 1 > 0; --i)
			{
				if (!_is_in(d[i], s, count))
					return i;
			}
			return npos;
		}

		size_type find_last_not_of(const value_type* s, size_type pos = npos) const
		{
			return find_last_not_of(s, pos, traits_type::length(s));
		}

		size_type find_last_not_of(value_type ch, size_type pos = npos) const
		{
			const value_type * const d = data();

			if (pos > size())
				pos = size();
			for (size_type i = pos - 1; i + 1 > 0; --i)
			{
				if (!traits_type::eq(d[i], ch))
					return i;
			}
			return npos;
		}

	private:
		bool _is_long() const noexcept
		{
			return long_;
		}

		void _set_long(bool b) noexcept
		{
			long_ = b;
		}

		size_type _next_cap(size_type required_cap) const
		{
			if (required_cap > max_size())
				throw length_error{ "string to long" };

			/*const size_type m3 = max_size() / 3;

			if (capacity() < m3)
			{
				return capacity() + mstd::max(3 * (capacity() + 1) / 5, required_cap);
			}

			if (capacity() < m3 * 2)
			{
				return capacity() + mstd::max((capacity() + 1) / 2, required_cap);
			}

			return max_size();*/
			return required_cap + 5;
		}

		void _set_new_length(size_type new_length) noexcept
		{
			traits_type::assign(data()[new_length], value_type{}); //set '\0'
			size_and_alloc_.first() = new_length;
		}

		void _init_grow(size_type new_cap)
		{
			if (new_cap <= short_store_.size())
			{
				_set_long(false);
			}
			else
			{
				long_store_.capacity_ = 0;
				new_cap = _next_cap(new_cap + 1);
				_set_long(true);
				long_store_.begin_ = size_and_alloc_.second().allocate(new_cap);
				long_store_.capacity_ = new_cap;
			}
		}

		void _grow(size_type new_cap)
		{
			if (new_cap <= capacity())
				return;

			new_cap = _next_cap(new_cap + 1);
			pointer tmp = size_and_alloc_.second().allocate(new_cap);

			if (!_is_long())
			{
				traits_type::copy(mstd::addressof(*tmp), &short_store_[0], size() + 1);
				_set_long(true);
			}
			else
			{
				traits_type::copy(mstd::addressof(*tmp), mstd::addressof(*long_store_.begin_), size() + 1);
				_destroy_long();
			}
			new(&long_store_) long_type{ tmp, new_cap };
		}

		void _shrink(size_type new_cap) //[[expects: new_cap >= size()]]
		{
			if (!_is_long() || new_cap >= capacity())
				return;

			pointer tmp = size_and_alloc_.second().allocate(new_cap + 1);

			traits_type::copy(mstd::addressof(*tmp), mstd::addressof(*long_store_.begin_), size());
			_destroy_long();

			_set_new_length(size());
			long_store_.begin_ = tmp;
			long_store_.capacity_ = new_cap + 1;
		}

		void _destroy_long() //[[expects: _is_long() == true]]
		{
			size_and_alloc_.second().deallocate(long_store_.begin_, long_store_.capacity_);
			long_store_.~long_type();
		}

		bool _is_in(value_type ch, const value_type* s, size_type count) const
		{
			for (size_type i = 0; i < count; ++i)
			{
				if (traits_type::eq(ch, s[i]))
					return true;
			}
			return false;
		}

	private:
		detail::compressed_pair<size_type, wrapper> size_and_alloc_;
		struct long_type
		{
			typename wrapper::pointer begin_;
			size_type capacity_;
		};
		union
		{
			long_type long_store_;
			array<value_type, 16> short_store_;
		};
		bool long_;
	};

	using string = basic_string<char>;
	using wstring = basic_string<wchar_t>;
	using u16string = basic_string<char16_t>;
	using u32string = basic_string <char32_t>;


	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(const basic_string<CharT, Traits, Alloc>& lhs,
			const basic_string<CharT, Traits, Alloc>& rhs)
	{
		basic_string<CharT, Traits, Alloc> result;
		result.reserve(lhs.size() + rhs.size());
		result += lhs;
		result += rhs;
		return result;
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(const CharT* lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		basic_string<CharT, Traits, Alloc> result;
		auto length = Traits::length(lhs);
		result.reserve(length + rhs.size());
		result.append(lhs, length);
		result += rhs;
		return result;
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc> operator+(CharT lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		basic_string<CharT, Traits, Alloc> result;
		result.reserve(1 + rhs.size());
		result += lhs;
		result += rhs;
		return result;
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(const basic_string<CharT, Traits, Alloc>& lhs, const CharT* rhs)
	{
		basic_string<CharT, Traits, Alloc> result;
		auto length = Traits::length(rhs);
		result.reserve(lhs.size() + length);
		result += lhs;
		result.append(rhs, length);
		return result;
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc> operator+(const basic_string<CharT, Traits, Alloc>& lhs, CharT rhs)
	{
		basic_string<CharT, Traits, Alloc> result;
		result.reserve(lhs.size() + 1);
		result += lhs;
		result += rhs;
		return result;
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(basic_string<CharT, Traits, Alloc>&& lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		return mstd::move(lhs.append(rhs));
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(const basic_string<CharT, Traits, Alloc>& lhs, basic_string<CharT, Traits, Alloc>&& rhs)
	{
		return mstd::move(rhs.insert(0, lhs));
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(basic_string<CharT, Traits, Alloc>&& lhs, basic_string<CharT, Traits, Alloc>&& rhs)
	{
		if (rhs.size() <= lhs.capacity() - lhs.size() || rhs.capacity() - rhs.size() < lhs.size())
			return mstd::move(lhs.append(rhs));
		else
			return mstd::move(rhs.insert(0, lhs));
	}

	template <class CharT, class Traits, class Alloc>
	basic_string <CharT, Traits, Alloc>
		operator+(const CharT* lhs, basic_string<CharT, Traits, Alloc>&& rhs)
	{
		return mstd::move(rhs.insert(0, lhs));
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(CharT lhs, basic_string<CharT, Traits, Alloc>&& rhs)
	{
		return mstd::move(rhs.insert(0u, 1u, lhs));
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(basic_string<CharT, Traits, Alloc>&& lhs, const CharT* rhs)
	{
		return mstd::move(lhs.append(rhs));
	}

	template <class CharT, class Traits, class Alloc>
	basic_string<CharT, Traits, Alloc>
		operator+(basic_string<CharT, Traits, Alloc>&& lhs, CharT rhs)
	{
		return mstd::move(lhs += rhs);
	}

	template< class CharT, class Traits, class Alloc >
	bool operator==(const basic_string<CharT, Traits, Alloc>& lhs, const basic_string<CharT, Traits, Alloc>& rhs) noexcept
	{
		return lhs.compare(rhs) == 0;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator!=(const basic_string<CharT, Traits, Alloc>& lhs, const basic_string<CharT, Traits, Alloc>& rhs) noexcept
	{
		return !(lhs == rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator<(const basic_string<CharT, Traits, Alloc>& lhs, const basic_string<CharT, Traits, Alloc>& rhs) noexcept
	{
		return lhs.compare(rhs) < 0;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator<=(const basic_string<CharT, Traits, Alloc>& lhs, const basic_string<CharT, Traits, Alloc>& rhs) noexcept
	{
		return !(lhs > rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator>(const basic_string<CharT, Traits, Alloc>& lhs, const basic_string<CharT, Traits, Alloc>& rhs) noexcept
	{
		return rhs < lhs;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator>=(const basic_string<CharT, Traits, Alloc>& lhs, const basic_string<CharT, Traits, Alloc>& rhs) noexcept
	{
		return !(lhs < rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator==(const CharT* lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		return rhs.compare(lhs) == 0;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator==(const basic_string<CharT, Traits, Alloc>& lhs, const CharT* rhs)
	{
		return lhs.compare(rhs) == 0;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator!=(const CharT* lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator!=(const basic_string<CharT, Traits, Alloc>& lhs, const CharT* rhs)
	{
		return !(lhs == rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator<(const CharT* lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		return rhs.compare(lhs) > 0;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator<(const basic_string<CharT, Traits, Alloc>& lhs, const CharT* rhs)
	{
		return lhs.compare(rhs) < 0;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator<=(const CharT* lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		return !(lhs > rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator<=(const basic_string<CharT, Traits, Alloc>& lhs, const CharT* rhs)
	{
		return !(lhs > rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator>(const CharT* lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		return rhs < lhs;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator>(const basic_string<CharT, Traits, Alloc>& lhs, const CharT* rhs)
	{
		return rhs < lhs;
	}

	template <class CharT, class Traits, class Alloc>
	bool operator>=(const CharT* lhs, const basic_string<CharT, Traits, Alloc>& rhs)
	{
		return !(lhs < rhs);
	}

	template <class CharT, class Traits, class Alloc>
	bool operator>=(const basic_string<CharT, Traits, Alloc>& lhs, const CharT* rhs)
	{
		return !(lhs < rhs);
	}

	template <class CharT, class Traits, class Alloc>
	void swap(basic_string<CharT, Traits, Alloc> &lhs, basic_string<CharT, Traits, Alloc> &rhs)
	{
		lhs.swap(rhs);
	}

	inline namespace literals
	{
		inline namespace string_literals
		{
			string operator ""_s(const char *str, std::size_t len)
			{
				return{ str, len };
			}

			wstring operator ""_s(const wchar_t* str, std::size_t len)
			{
				return{ str, len };
			}

			u16string operator ""_s(const char16_t* str, std::size_t len)
			{
				return{ str, len };
			}

			u32string operator ""_s(const char32_t* str, std::size_t len)
			{
				return{ str, len };
			}
		}
	}

#include <iosfwd>

	std::ostream& operator<<(std::ostream& out, const basic_string<char>& str)
	{
		out << str.c_str(); //not standard conforming
		return out;
	}

}
