#pragma once

#include <cstdint>
#include "type_traits.hpp"

namespace mstd
{
	namespace detail
	{
		template <class integer, class = enable_if_t<is_integral<integer>::value>>
		constexpr integer gcd(integer a, integer b) noexcept
		{
			return b == 0 ? a : gcd(b, a % b);
		}

		template <class integer, class = enable_if_t<is_integral<integer>::value>>
		constexpr integer abs(integer a) noexcept
		{
			return a < 0 ? -a : a;
		}

		template <class integer, class = enable_if_t<is_integral<integer>::value>>
		constexpr integer sign(integer a) noexcept
		{
			return a < 0 ? -1 : 1;
		}
	}

	template <std::intmax_t Num, std::intmax_t Denom = 1>
	class ratio
	{
	public:
		constexpr static std::intmax_t num =
			detail::sign(Num) * detail::sign(Denom) * detail::abs(Num) / detail::gcd(Num, Denom);
		constexpr static std::intmax_t den = detail::abs(Denom) / detail::gcd(Num, Denom);

		using type = ratio<num, den>;

		static_assert(den != 0, "ratio::den could not be zero");
		static_assert(den != std::numeric_limits<std::intmax_t>::min(),
			"ratio::den could not be the most negative");
		static_assert(num != std::numeric_limits<std::intmax_t>::min(),
			"ratio::num could not be the most negative");
	};



	//TODO ratio_add, ratio_subtract, ratio_multiply, ratio_divide

	template <class R1, class R2>
	struct ratio_equal : bool_constant<R1::num == R2::num && R1::den == R2::den>
	{
	};

	template <class R1, class R2>
	struct ratio_not_equal : bool_constant<!ratio_equal<R1, R2>::value>
	{
	};


	template <class R1, class R2>
	struct ratio_less
		: bool_constant < (R1::num / detail::gcd<std::intmax_t>(R1::num, R2::num) * R2::den / detail::gcd<std::intmax_t>(R1::den, R2::den))
		< (R2::num / detail::gcd<std::intmax_t>(R1::num, R2::num) * R1::den / detail::gcd<std::intmax_t>(R1::den, R2::den)) >
	{
	};

	template <class R1, class R2>
	struct ratio_greater : bool_constant<ratio_less<R2, R1>::value>
	{
	};

	template <class R1, class R2>
	struct ratio_less_equal : bool_constant<!ratio_less<R2, R1>::value>
	{
	};

	template <class R1, class R2>
	struct ratio_greater_equal : bool_constant<!ratio_less<R1, R2>::value>
	{
	};




	using atto = ratio<1, 1000000000000000000>;
	using femto = ratio<1, 1000000000000000>;
	using pico = ratio<1, 1000000000000>;
	using nano = ratio<1, 1000000000>;
	using micro = ratio<1, 1000000>;
	using milli = ratio<1, 1000>;
	using centi = ratio<1, 100>;
	using deci = ratio<1, 10>;
	using deca = ratio<10, 1>;
	using hecto = ratio<100, 1>;
	using kilo = ratio<1000, 1>;
	using mega = ratio<1000000, 1>;
	using giga = ratio<1000000000, 1>;
	using tera = ratio<1000000000000, 1>;
	using peta = ratio<1000000000000000, 1>;
	using exa = ratio<1000000000000000000, 1>;
}
