#pragma once

#include "move_and_forward.hpp"
#include "../type_traits.hpp"

namespace mstd
{
	template <class T>
	void swap(T& lhs, T& rhs)
		noexcept (is_nothrow_move_constructible<T>::value
			&& is_nothrow_move_assignable<T>::value)
	{
		T temp{ mstd::move(lhs) };
		lhs = mstd::move(rhs);
		rhs = mstd::move(temp);
	}

	template <class T, std::size_t N>
	void swap(T(&lhs)[N], T(&rhs)[N])
		noexcept (noexcept(swap(*lhs, *rhs)))
	{
		for (size_t i = 0; i < N; ++i)
		{
			swap(lhs[i], rhs[i]);
		}
	}

	template <class T, class U = T>
	T exchange(T& obj, U&& new_value)
	{
		T old{ mstd::move(obj) };
		obj = mstd::forward<U>(new_value);
		return old;
	}
}
