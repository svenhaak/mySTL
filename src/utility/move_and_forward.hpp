#pragma once

#include "../type_traits.hpp"

namespace mstd
{
	template <class T>
	constexpr remove_reference_t<T>&& move(T&& arg) noexcept
	{
		return static_cast<remove_reference_t<T>&&>(arg);
	}

	template <class T>
	constexpr conditional_t<is_nothrow_move_constructible<T>::value ||
		!is_copy_constructible<T>::value, T&&, const T&>
		move_if_noexcept(T& arg) noexcept
	{
		return mstd::move(arg);
	}

	template <class T>
	constexpr T&& forward(remove_reference_t<T>& arg) noexcept
	{
		return static_cast<T&&>(arg);
	}

	template <class T>
	constexpr T&& forward(remove_reference_t<T>&& arg) noexcept
	{
		static_assert(!is_lvalue_reference<T>::value, "wrong used template parameter for forward function");
		return static_cast<T&&>(arg);
	}
}
