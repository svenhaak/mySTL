#pragma once

#include <initializer_list>
#include <random>
#include "functional/operator_functors.hpp" //less<>, greater<>
#include "iterator.hpp"
#include "utility.hpp" //pair
#include "type_traits.hpp"

namespace mstd
{
	template <class InputIterator, class UnaryPredicate>
	bool all_of(InputIterator first, InputIterator last, UnaryPredicate pred)
	{
		while (first != last)
		{
			if (!pred(*first))
				return false;
			++first;
		}
		return true;
	}

	template <class InputIterator, class UnaryPredicate>
	bool any_of(InputIterator first, InputIterator last, UnaryPredicate pred)
	{
		while (first != last)
		{
			if (pred(*first))
				return true;
			++first;
		}
		return false;
	}

	template <class InputIterator, class UnaryPredicate>
	bool none_of(InputIterator first, InputIterator last, UnaryPredicate pred)
	{
		while (first != last)
		{
			if (pred(*first))
				return false;
			++first;
		}
		return true;
	}

	template <class InputIterator, class Function>
	Function for_each(InputIterator first, InputIterator last, Function fn)
	{
		while (first != last)
		{
			fn(*first);
			++first;
		}
		return fn;
	}

	template <class InputIterator, class T>
	InputIterator find(InputIterator first, InputIterator last, const T& val)
	{
		while (first != last)
		{
			if (*first == val)
				return first;
		}
		return last;
	}

	template <class InputIterator, class UnaryPredicate>
	InputIterator find_if(InputIterator first, InputIterator last, UnaryPredicate pred)
	{
		while (first != last)
		{
			if (pred(*first))
				return first;
			++first;
		}
		return last;
	}

	template <class InputIterator, class UnaryPredicate>
	InputIterator find_if_not(InputIterator first, InputIterator last, UnaryPredicate pred)
	{
		while (first != last)
		{
			if (!pred(*first))
				return first;
			++first;
		}
		return last;
	}

	template <class ForwardIterator1, class ForwardIterator2>
	ForwardIterator1 find_end(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2, ForwardIterator2 last2)
	{
		if (first2 == last2)
			return last1;

		ForwardIterator1 ret = last1;

		while (first1 != last1)
		{
			ForwardIterator1 it1 = first1;
			ForwardIterator2 it2 = first2;

			while (*it1 == *it2)
			{
				++it1; ++it2;

				if (it2 == last2)
				{
					ret = first1;
					break;
				}

				if (it1 == last1)
					return ret;
			}
			++first1;
		}
		return ret;
	}

	template <class ForwardIterator1, class ForwardIterator2, class BinaryPredicate>
	ForwardIterator1 find_end(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2, ForwardIterator2 last2,
		BinaryPredicate pred)
	{
		if (first2 == last2)
			return last1;

		ForwardIterator1 ret = last1;

		while (first1 != last1)
		{
			ForwardIterator1 it1 = first1;
			ForwardIterator2 it2 = first2;

			while (pred(*it1, *it2))
			{
				++it1; ++it2;

				if (it2 == last2)
				{
					ret = first1;
					break;
				}

				if (it1 == last1)
					return ret;
			}
			++first1;
		}
		return ret;
	}

	template<class InputIterator, class ForwardIterator>
	InputIterator find_first_of(InputIterator first1, InputIterator last1,
		ForwardIterator first2, ForwardIterator last2)
	{
		while (first1 != last1)
		{
			for (ForwardIterator it = first2; it != last2; ++it)
			{
				if (*it == *first1)
					return first1;
			}
			++first1;
		}
		return last1;
	}

	template <class InputIterator, class ForwardIterator, class BinaryPredicate>
	InputIterator find_first_of(InputIterator first1, InputIterator last1,
		ForwardIterator first2, ForwardIterator last2,
		BinaryPredicate pred)
	{
		while (first1 != last1)
		{
			for (ForwardIterator it = first2; it != last2; ++it)
			{
				if (pred(*it, *first1))
					return first1;
			}
			++first1;
		}
		return last1;
	}

	template <class ForwardIterator>
	ForwardIterator adjacent_find(ForwardIterator first, ForwardIterator last)
	{
		if (first != last)
		{
			ForwardIterator next = first;
			++next;

			while (next != last)
			{
				if (*first == *next)
					return first;

				++first;
				++next;
			}
		}
		return last;
	}

	template <class ForwardIterator, class BinaryPredicate>
	ForwardIterator adjacent_find(ForwardIterator first, ForwardIterator last,
		BinaryPredicate pred)
	{
		if (first != last)
		{
			ForwardIterator next = first;
			++next;

			while (next != last)
			{
				if (pred(*first, *next))
					return first;

				++first;
				++next;
			}
		}
		return last;
	}

	template <class InputIterator, class T>
	typename iterator_traits<InputIterator>::difference_type
		count(InputIterator first, InputIterator last, const T& val)
	{
		typename iterator_traits<InputIterator>::difference_type ret = 0;

		while (first != last)
		{
			if (*first == val)
				++ret;
			++first;
		}
		return ret;
	}

	template <class InputIterator, class UnaryPredicate>
	typename iterator_traits<InputIterator>::difference_type
		count_if(InputIterator first, InputIterator last, UnaryPredicate pred)
	{
		typename iterator_traits<InputIterator>::difference_type ret = 0;

		while (first != last)
		{
			if (pred(*first))
				++ret;
			++first;
		}
		return ret;
	}

	template <class InputIterator1, class InputIterator2>
	pair<InputIterator1, InputIterator2>
		mismatch(InputIterator1 first1, InputIterator1 last1,
			InputIterator2 first2)
	{
		while ((first1 != last1) && (*first1 == *first2))
		{
			++first1;
			++first2;
		}
		return mstd::make_pair(first1, first2);
	}

	template <class InputIterator1, class InputIterator2, class BinaryPredicate>
	pair<InputIterator1, InputIterator2>
		mismatch(InputIterator1 first1, InputIterator1 last1,
			InputIterator2 first2, BinaryPredicate pred)
	{
		while ((first1 != last1) && pred(*first1, *first2))
		{
			++first1;
			++first2;
		}
		return mstd::make_pair(first1, first2);
	}

	template <class InputIterator1, class InputIterator2>
	bool equal(InputIterator1 first1, InputIterator1 last1,
		InputIterator2 first2)
	{
		while (first1 != last1)
		{
			if (!(*first1 == *first2))
				return false;
			++first1;
			++first2;
		}
		return true;
	}

	template <class InputIterator1, class InputIterator2, class BinaryPredicate>
	bool equal(InputIterator1 first1, InputIterator1 last1,
		InputIterator2 first2, BinaryPredicate pred)
	{
		while (first1 != last1)
		{
			if (!pred(*first1, *first2))
				return false;
			++first1;
			++first2;
		}
		return true;
	}

	template <class ForwardIterator1, class ForwardIterator2>
	bool is_permutation(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2)
	{
		tie(first1, first2) = mismatch(first1, last1, first2);
		if (first1 == last1)
			return true;

		ForwardIterator2 last2 = first2;
		advance(last2, distance(first1, last1));

		for (ForwardIterator1 it1 = first1; it1 != last1; ++it1) {
			if (find(first1, it1, *it1) == it1) {
				auto n = count(first2, last2, *it1);
				if (n == 0 || count(it1, last1, *it1) != n)
					return false;
			}
		}
		return true;
	}

	//TODO
	template <class ForwardIterator1, class ForwardIterator2, class BinaryPredicate>
	bool is_permutation(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2, BinaryPredicate pred);

	template <class ForwardIterator1, class ForwardIterator2>
	ForwardIterator1 search(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2, ForwardIterator2 last2)
	{
		if (first2 == last2)
			return first1;

		while (first1 != last1)
		{
			ForwardIterator1 it1 = first1;
			ForwardIterator2 it2 = first2;

			while (*it1 == *it2)
			{
				++it1;
				++it2;
				if (it2 == last2) return first1;
				if (it1 == last1) return last1;
			}
			++first1;
		}
		return last1;
	}

	template <class ForwardIterator1, class ForwardIterator2, class BinaryPredicate>
	ForwardIterator1 search(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2, ForwardIterator2 last2,
		BinaryPredicate pred)
	{
		if (first2 == last2)
			return first1;

		while (first1 != last1)
		{
			ForwardIterator1 it1 = first1;
			ForwardIterator2 it2 = first2;

			while (pred(*it1, *it2))
			{
				++it1;
				++it2;
				if (it2 == last2) return first1;
				if (it1 == last1) return last1;
			}
			++first1;
		}
		return last1;
	}

	template <class ForwardIterator, class Size, class T>
	ForwardIterator search_n(ForwardIterator first, ForwardIterator last,
		Size count, const T& val)
	{
		ForwardIterator it, limit;
		Size i;

		limit = first;
		mstd::advance(limit, mstd::distance(first, last) - count);

		while (first != limit)
		{
			it = first;
			i = 0;

			while (*it == val)
			{
				++it;
				if (++i == count) return first;
			}
			++first;
		}
		return last;
	}

	template <class ForwardIterator, class Size, class T, class BinaryPredicate>
	ForwardIterator search_n(ForwardIterator first, ForwardIterator last,
		Size count, const T& val, BinaryPredicate pred)
	{
		ForwardIterator it, limit;
		Size i;

		limit = first;
		mstd::advance(limit, mstd::distance(first, last) - count);

		while (first != limit)
		{
			it = first;
			i = 0;

			while (pred(*it, val))
			{
				++it;
				if (++i == count) return first;
			}
			++first;
		}
		return last;
	}

	template <class InputIterator, class OutputIterator>
	OutputIterator copy(InputIterator first, InputIterator last, OutputIterator result)
	{
		while (first != last)
		{
			*result = *first;
			++first;
			++result;
		}
		return result;
	}

	template <class InputIterator, class Size, class OutputIterator>
	OutputIterator copy_n(InputIterator first, Size n, OutputIterator result)
	{
		while (n > 0)
		{
			*result = *first;
			++first;
			++result;
			--n;
		}
		return result;
	}

	template <class InputIterator, class OutputIterator, class UnaryPredicate>
	OutputIterator copy_if(InputIterator first, InputIterator last,
		OutputIterator result, UnaryPredicate pred)
	{
		while (first != last)
		{
			if (pred(*first))
			{
				*result = *first;
				++result;
			}
			++first;
		}
		return result;
	}

	template <class BidirectionalIterator1, class BidirectionalIterator2>
	BidirectionalIterator2 copy_backward(BidirectionalIterator1 first,
		BidirectionalIterator1 last,
		BidirectionalIterator2 result)
	{
		while (last != first)
		{
			*(--result) = *(--last);
		}
		return result;
	}

	template <class InputIterator, class OutputIterator>
	OutputIterator move(InputIterator first, InputIterator last, OutputIterator result)
	{
		while (first != last)
		{
			*result = mstd::move(*first);
			++first;
			++result;
		}
		return result;
	}

	template <class BidirectionalIterator1, class BidirectionalIterator2>
	BidirectionalIterator2 move_backward(BidirectionalIterator1 first,
		BidirectionalIterator1 last,
		BidirectionalIterator2 result)
	{
		while (last != first)
		{
			*(--result) = mstd::move(*(--last));
		}
		return result;
	}

	template <class ForwardIterator1, class ForwardIterator2>
	void iter_swap(ForwardIterator1 a, ForwardIterator2 b)
	{
		swap(*a, *b);
	}

	template <class ForwardIterator1, class ForwardIterator2>
	ForwardIterator2 swap_ranges(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2)
	{
		while (first1 != last1)
		{
			mstd::iter_swap(first1++, first2++);
		}
		return first2;
	}

	template <class InputIterator, class OutputIterator, class UnaryOperation>
	OutputIterator transform(InputIterator first1, InputIterator last1,
		OutputIterator result, UnaryOperation op)
	{
		while (first1 != last1)
		{
			*result = op(*first1);
			++first1;
			++result;
		}
		return result;
	}

	template <class InputIterator1, class InputIterator2,
	class OutputIterator, class BinaryOperation>
		OutputIterator transform(InputIterator1 first1, InputIterator1 last1,
			InputIterator2 first2, OutputIterator result,
			BinaryOperation binary_op)
	{
		while (first1 != last1)
		{
			*result = binary_op(*first1, *first2);
			++first1;
			++first2;
			++result;
		}
		return result;
	}

	template <class ForwardIterator, class T>
	void replace(ForwardIterator first, ForwardIterator last,
		const T& old_value, const T& new_value)
	{
		while (first != last)
		{
			if (*first == old_value)
				*first = new_value;
			++first;
		}
	}

	template <class ForwardIterator, class UnaryPredicate, class T>
	void replace_if(ForwardIterator first, ForwardIterator last,
		UnaryPredicate pred, const T& new_value)
	{
		while (first != last)
		{
			if (pred(*first))
				*first = new_value;
			++first;
		}
	}

	template <class InputIterator, class OutputIterator, class T>
	OutputIterator replace_copy(InputIterator first, InputIterator last,
		OutputIterator result,
		const T& old_value, const T& new_value)
	{
		while (first != last)
		{
			*result = (*first == old_value) ? new_value : *first;
			++first;
			++result;
		}
		return result;
	}

	template <class InputIterator, class OutputIterator, class UnaryPredicate, class T>
	OutputIterator replace_copy_if(InputIterator first, InputIterator last,
		OutputIterator result, UnaryPredicate pred,
		const T& new_value)
	{
		while (first != last)
		{
			*result = pred(*first) ? new_value : *first;
			++first;
			++result;
		}
		return result;
	}

	template <class ForwardIterator, class T>
	void fill(ForwardIterator first, ForwardIterator last, const T& val)
	{
		while (first != last)
		{
			*first = val;
			++first;
		}
	}

	template <class OutputIterator, class Size, class T>
	OutputIterator fill_n(OutputIterator first, Size n, const T& val)
	{
		while (n > 0)
		{
			*first = val;
			++first; --n;
		}
		return first;
	}

	template <class ForwardIterator, class Generator>
	void generate(ForwardIterator first, ForwardIterator last, Generator gen)
	{
		while (first != last)
		{
			*first = gen();
			++first;
		}
	}

	template <class OutputIterator, class Size, class Generator>
	void generate_n(OutputIterator first, Size n, Generator gen)
	{
		while (n > 0)
		{
			*first = gen();
			++first;
			--n;
		}
	}

	template <class ForwardIterator, class T>
	ForwardIterator remove(ForwardIterator first, ForwardIterator last, const T& val)
	{
		ForwardIterator result = first;
		while (first != last)
		{
			if (!(*first == val))
			{
				*result = mstd::move(*first);
				++result;
			}
			++first;
		}
		return result;
	}

	template <class ForwardIterator, class UnaryPredicate>
	ForwardIterator remove_if(ForwardIterator first, ForwardIterator last,
		UnaryPredicate pred)
	{
		ForwardIterator result = first;
		while (first != last)
		{
			if (!pred(*first))
			{
				*result = mstd::move(*first);
				++result;
			}
			++first;
		}
		return result;
	}

	template <class InputIterator, class OutputIterator, class T>
	OutputIterator remove_copy(InputIterator first, InputIterator last,
		OutputIterator result, const T& val)
	{
		while (first != last)
		{
			if (!(*first == val))
			{
				*result = *first;
				++result;
			}
			++first;
		}
		return result;
	}

	template <class InputIterator, class OutputIterator, class UnaryPredicate>
	OutputIterator remove_copy_if(InputIterator first, InputIterator last,
		OutputIterator result, UnaryPredicate pred)
	{
		while (first != last)
		{
			if (!pred(*first))
			{
				*result = *first;
				++result;
			}
			++first;
		}
		return result;
	}

	template <class ForwardIterator>
	ForwardIterator unique(ForwardIterator first, ForwardIterator last)
	{
		if (first == last)
			return last;

		ForwardIterator result = first;
		while (++first != last)
		{
			if (!(*result == *first))
				*(++result) = *first;
		}
		return ++result;
	}

	template <class ForwardIterator, class BinaryPredicate>
	ForwardIterator unique(ForwardIterator first, ForwardIterator last,
		BinaryPredicate pred)
	{
		if (first == last)
			return last;

		ForwardIterator result = first;
		while (++first != last)
		{
			if (!pred(*result, *first))
				*(++result) = *first;
		}
		return ++result;
	}

	template <class InputIterator, class OutputIterator>
	OutputIterator unique_copy(InputIterator first, InputIterator last,
		OutputIterator result)
	{
		if (first == last) return result;

		*result = *first;
		while (++first != last)
		{
			auto val = *first;
			if (!(*result == val))
				*(++result) = val;
		}
		return ++result;
	}

	template <class BidirectionalIterator>
	void reverse(BidirectionalIterator first, BidirectionalIterator last)
	{
		while (first != last && first != --last)
		{
			mstd::iter_swap(first, last);
			++first;
		}
	}

	template <class BidirectionalIterator, class OutputIterator>
	OutputIterator reverse_copy(BidirectionalIterator first,
		BidirectionalIterator last, OutputIterator result)
	{
		while (last != first)
		{
			*result = *(--last);
			++result;
		}
		return result;
	}

	template <class ForwardIterator>
	ForwardIterator rotate(ForwardIterator first, ForwardIterator middle, ForwardIterator last)
	{
		ForwardIterator next = middle;
		while (first != next)
		{
			mstd::iter_swap(first++, next++);

			if (next == last)
				next = middle;
			else if (first == middle)
				middle = next;
		}
		return next;
	}


	template <class ForwardIterator, class OutputIterator>
	OutputIterator rotate_copy(ForwardIterator first, ForwardIterator middle,
		ForwardIterator last, OutputIterator result)
	{
		result = mstd::copy(middle, last, result);
		return mstd::copy(first, middle, result);
	}

	template< class RandomIt, class URNG >
	void shuffle(RandomIt first, RandomIt last, URNG&& g)
	{
		using diff_t = typename iterator_traits<RandomIt>::difference_type;
		using udiff_t = typename make_unsigned<diff_t>::type;
		using distr_t = typename std::uniform_int_distribution<udiff_t>;
		using param_t = typename distr_t::param_type;

		distr_t D;
		diff_t n = last - first;
		for (diff_t i = n - 1; i > 0; --i)
		{
			using mstd::swap;
			swap(first[i], first[D(g, param_t(0, i))]);
		}
	}

	template <class T>
	constexpr const T& min(const T& a, const T& b)
	{
		return mstd::min(a, b, less<>{});
	}

	template <class T, class Compare>
	constexpr const T& min(const T& a, const T& b, Compare comp)
	{
		return comp(b, a) ? b : a;
	}

	template <class T>
	/*constexpr*/ T min(std::initializer_list<T> il)
	{
		return mstd::min(il, less<>{});
	}

	template <class T, class Compare>
	/*constexpr*/ T min(std::initializer_list<T> il, Compare comp)
	{
		auto first = il.begin();
		T ret = *first;
		++first;
		while (first != il.end())
		{
			if (comp(*first, ret))
				ret = *first;
			++first;
		}
		return ret;
	}

	template <class T, class Compare>
	constexpr const T& max(const T& a, const T& b, Compare comp)
	{
		return comp(b, a) ? b : a;
	}

	template <class T>
	constexpr const T& max(const T& a, const T& b)
	{
		return mstd::max(a, b, greater<>{});
	}

	template <class T>
	/*constexpr*/ T max(std::initializer_list<T> il)
	{
		return mstd::max(il, less<>{});
	}

	template <class T, class Compare>
	/*constexpr*/ T max(std::initializer_list<T> il, Compare comp)
	{
		auto first = il.begin();
		T ret = *first;

		++first;
		while (first != il.end())
		{
			if (comp(ret, *first))
				ret = *first;
			++first;
		}
		return ret;
	}

	template <class InputIterator1, class InputIterator2, class Compare>
	bool lexicographical_compare(InputIterator1 first1, InputIterator1 last1,
		InputIterator2 first2, InputIterator2 last2, Compare comp)
	{
		while (first1 != last1)
		{
			if (first2 == last2 || *first2 < *first1)
				return false;
			else if (comp(*first1, *first2))
				return true;
			++first1; ++first2;
		}
		return first2 != last2;
	}

	template <class InputIterator1, class InputIterator2>
	bool lexicographical_compare(InputIterator1 first1, InputIterator1 last1,
		InputIterator2 first2, InputIterator2 last2)
	{
		return lexicographical_compare(first1, last1, first2, last2, less<>{});
	}
}
