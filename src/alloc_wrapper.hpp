#pragma once

#include "memory.hpp"
#include "type_traits.hpp"

namespace mstd
{
	namespace detail
	{
		struct raw_ptr_tag_t
		{ };

		constexpr raw_ptr_tag_t raw_ptr_tag{ };


		template <class Alloc>
		class alloc_wrapper : private Alloc
		{
		public:
			using traits = std::allocator_traits<Alloc>;
			using allocator_type = Alloc;
			using value_type = typename traits::value_type;
			using pointer = typename traits::pointer;
			using const_pointer = typename traits::const_pointer;
			using void_pointer = typename traits::void_pointer;
			using const_void_pointer = typename traits::const_void_pointer;
			using difference_type = typename traits::difference_type;
			using size_type = typename traits::size_type;
			using propagate_on_container_copy_assignment = typename traits::propagate_on_container_copy_assignment;
			using propagate_on_container_move_assignment = typename traits::propagate_on_container_move_assignment;
			using propagate_on_container_swap = typename traits::propagate_on_container_swap;
#if __cplusplus > 201701
			using is_always_equal = typename traits::is_always_equal;
#endif

			template <class T>
			using rebind_alloc = typename traits::template rebind_alloc<T>;

			template <class T>
			using rebind_traits = typename traits::template rebind_traits<T>;

			template <class T>
			using rebind_wrapper = alloc_wrapper<rebind_alloc<T>>;

			alloc_wrapper() = default;

			alloc_wrapper(const allocator_type& a) : allocator_type{ a }
			{ }

			pointer allocate(size_type n)
			{
				return traits::allocate(*this, n);
			}

			pointer allocate(size_type n, const_void_pointer hint)
			{
				return traits::allocate(*this, n, hint);
			}

			void deallocate(pointer p, size_type n)
			{
				traits::deallocate(*this, p, n);
			}

			template <class... Args>
			void construct(pointer p, Args&&... args)
			{
				traits::construct(*this, mstd::addressof(*p), mstd::forward<Args>(args)...);
			}

			template <class... Args>
			void construct(raw_ptr_tag_t, value_type* p, Args&&... args)
			{
				traits::construct(*this, p, mstd::forward<Args>(args)...);
			}

			void destroy(pointer p)
			{
				traits::destroy(*this, mstd::addressof(*p));
			}

			void destroy(raw_ptr_tag_t, value_type* p)
			{
				traits::destroy(*this, p);
			}

			size_type max_size() const noexcept
			{
				return traits::max_size(*this);
			}

			Alloc select_on_container_copy_construction() const
			{
				return traits::select_on_container_copy_construction(*this);
			}

			allocator_type& get_allocator() noexcept
			{
				return static_cast<allocator_type&>(*this);
			}

			const allocator_type& get_allocator() const noexcept
			{
				return static_cast<const allocator_type&>(*this);
			}
		};
	}
}
