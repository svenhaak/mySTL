#pragma once

#include "exception.hpp"
#include "functional/operator_functors.hpp"
#include "experimental/type_traits.hpp"
#include "utility.hpp"
#include <atomic>

namespace mstd
{
	using nullptr_t = decltype(nullptr);

	template <class T>
	T* addressof(T& arg)
	{
		return reinterpret_cast<T*>(
			&const_cast<char&>(
				reinterpret_cast<const volatile char&>(arg)));
	}

	namespace detail
	{
		template <class T>
		using elemtent_type_helper = typename T::element_type;

		template <class T, bool = experimental::is_detected<elemtent_type_helper, T>::value>
		struct get_pointer_traits_element_type;

		template <class T>
		struct get_pointer_traits_element_type<T, true>
		{
			using type = typename T::element_type;
		};

		template <template<class, class...>class Ptr, class T, class... Args>
		struct get_pointer_traits_element_type<Ptr<T, Args...>, false>
		{
			using type = T;
		};

		template <class T>
		using diff_t_helper = typename T::difference_type;

		template <class T, class U>
		using rebind_helper = typename T::template rebind<U>;

		template <class Ptr, class U, bool = experimental::is_detected<rebind_helper, Ptr, U>::value>
		struct get_pointer_traits_rebind
		{
			using type = typename Ptr::template rebind<U>;
		};

		template <template <class, class...> class Ptr, class T, class... Args, class U>
		struct get_pointer_traits_rebind<Ptr<T, Args...>, U, true>
		{
			using type = typename Ptr<T, Args...>::template rebind<U>;
		};

		template <template <class, class...> class Ptr, class T, class... Args, class U>
		struct get_pointer_traits_rebind<Ptr<T, Args...>, U, false>
		{
			using type = Ptr<U, Args...>;
		};
	}

	template <class Ptr>
	struct pointer_traits
	{
		using pointer = Ptr;
		using element_type = typename detail::get_pointer_traits_element_type<Ptr>::type;
		using difference_type = experimental::detected_or_t<std::ptrdiff_t, detail::diff_t_helper, Ptr>;
		template <class U>
		using rebind = typename detail::get_pointer_traits_rebind<Ptr, U>::type;

	private:
		struct error_type;

	public:
		static pointer pointer_to(conditional_t<is_void<element_type>::value, error_type, add_lvalue_reference_t<element_type>> r) noexcept(noexcept(Ptr::pointer_to(r)))
		{
			return Ptr::pointer_to(r);
		}
	};

	template <class T>
	struct pointer_traits<T*>
	{
		using pointer = T*;
		using element_type = T;
		using difference_type = std::ptrdiff_t;
		template <class U> using rebind = U*;

	private:
		struct error_type;

	public:
		static pointer pointer_to(conditional_t<is_void<element_type>::value, error_type, add_lvalue_reference_t<element_type>> r) noexcept
		{
			return mstd::addressof(r);
		}
	};

	namespace detail
	{
		template <class T>
		using ptr_t_helper = typename T::pointer;

		template <class T>
		using const_ptr_t_helper = typename T::const_pointer;

		template <class T>
		using void_ptr_t_helper = typename T::void_pointer;

		template <class T>
		using const_void_ptr_t_helper = typename T::const_void_pointer;

		template <class T>
		using size_t_helper = typename T::size_type;

		template <class T>
		using pocca_t_helper = typename T::propagate_on_container_copy_assignment;

		template <class T>
		using pocma_t_helper = typename T::propagate_on_container_move_assignment;

		template <class T>
		using pocs_t_helper = typename T::propagate_on_container_swap;

		template <class T, class U>
		using rebind_other_t_helper = typename T::template rebind<U>::other;

		template <class T, class U, bool = experimental::is_detected<rebind_other_t_helper, T, U>::value>
		struct allocator_traits_rebind_other
		{
			using type = typename T::template rebind<U>::other;
		};

		template <template <class, class...> class Alloc, class T, class... Args, class U>
		struct allocator_traits_rebind_other<Alloc<T, Args...>, U, true>
		{
			using type = typename Alloc<T, Args...>::template rebind<U>::other;
		};

		template <template <class, class...> class Alloc, class T, class... Args, class U>
		struct allocator_traits_rebind_other<Alloc<T, Args...>, U, false>
		{
			using type = Alloc<U, Args...>;
		};

		template <class T>
		using always_equal_helper = typename T::is_always_equal;

		template <class Alloc, class size_type, class hint>
		using has_alloc_hint = decltype(mstd::declval<Alloc&>().allocate(mstd::declval<size_type>(), mstd::declval<hint>()));

		template <class Alloc, class P, class... Args>
		using has_alloc_constr = decltype(mstd::declval<Alloc&>().construct(mstd::declval<P*>(), mstd::declval<Args>()...));

		template <class Alloc>
		using has_alloc_max_sz = decltype(mstd::declval<Alloc&>().max_size());
	}

	template <class Alloc>
	struct allocator_traits
	{
		using allocator_type = Alloc;
		using value_type = typename Alloc::value_type;
		using pointer = experimental::detected_or_t<value_type*, detail::ptr_t_helper, Alloc>;
		using const_pointer = experimental::detected_or_t<typename pointer_traits<pointer>::template rebind<const value_type>, detail::const_ptr_t_helper, Alloc>;
		using void_pointer = experimental::detected_or_t<typename pointer_traits<pointer>::template rebind<void>, detail::void_ptr_t_helper, Alloc>;
		using const_void_pointer = experimental::detected_or_t<typename pointer_traits<pointer>::template rebind<const void>, detail::const_void_ptr_t_helper, Alloc>;
		using difference_type = experimental::detected_or_t<typename pointer_traits<pointer>::difference_type, detail::diff_t_helper, Alloc>;
		using size_type = experimental::detected_or_t<make_unsigned_t<difference_type>, detail::size_t_helper, Alloc>;
		using propagate_on_container_copy_assignment = experimental::detected_or_t<false_type, detail::pocca_t_helper, Alloc>;
		using propagate_on_container_move_assignment = experimental::detected_or_t<false_type, detail::pocma_t_helper, Alloc>;
		using propagate_on_container_swap = experimental::detected_or_t<false_type, detail::pocs_t_helper, Alloc>;

		template <class T>
		using rebind_alloc = typename detail::allocator_traits_rebind_other<Alloc, T>::type;

		template <class T>
		using rebind_traits = allocator_traits<rebind_alloc<T>>;

		using is_always_equal = experimental::detected_or_t<typename is_empty<Alloc>::type, detail::always_equal_helper, Alloc>;

		static pointer allocate(allocator_type& alloc, size_type n)
		{
			return alloc.allocate(n);
		}

		static pointer allocate(allocator_type& alloc, size_type n, const_void_pointer hint)
		{
			return alloc_hint(experimental::is_detected<detail::has_alloc_hint, allocator_type, size_type, const_void_pointer>{}, alloc, n, hint);
		}

		static void deallocate(allocator_type& alloc, pointer p, size_type n)
		{
			return alloc.deallocate(p, n);
		}

		template <class T, class... Args>
		static void construct(allocator_type& a, T* p, Args&&... args)
		{
			constr(experimental::is_detected<detail::has_alloc_constr, allocator_type, Args...>{}, a, p, args...);
		}

		template <class T>
		static void destroy(Alloc& a, T* p)
		{
			//destr(detail::has_alloc_destr<Alloc, T*>{})
		}

		static size_type max_size(const Alloc& a) noexcept
		{
			return max_sz(experimental::is_detected<detail::has_alloc_max_sz, Alloc>{}, a);
		}

	private:
		static pointer alloc_hint(true_type, allocator_type& a, size_type n, const_void_pointer hint)
		{
			return alloc.allocate(n, hint);
		}

		static pointer alloc_hint(false_type, allocator_type& a, size_type n, const_void_pointer)
		{
			return alloc.allocate(n);
		}

		template <class T, class... Args>
		static void constr(true_type, allocator_type& a, T* p, Args&&... args)
		{
			a.construct(pointer_traits<pointer>::pointer_to(*p), mstd::forward<Args>(args)...);
		}

		template <class T, class... Args>
		static void constr(false_type, allocator_type&, T* p, Args&&... args)
		{
			new (p) T{ mstd::forward<Args>(args)... };
		}

		template <class T>
		static void destr(true_type, const Alloc& a, T* p)
		{
			a.destroy(p);
		}

		template <class T>
		static void destr(false_type, const Alloc& a, T* p)
		{
			p->~T();
		}

		static size_type max_sz(true_type, const Alloc& a) noexcept
		{
			return a.max_size();
		}

		static size_type max_sz(false_type, const Alloc& a) noexcept
		{
			return std::numeric_limits<size_type>::max() / sizeof(value_type);
		}
	};

	struct allocator_arg_t
	{ };

	constexpr allocator_arg_t allocator_arg{ };

	namespace detail
	{
		template <class T>
		using alloc_t = typename T::allocator_type;
	}

	template <class T, class Alloc>
	struct uses_allocator : experimental::is_detected_convertible<Alloc, detail::alloc_t, T>::type
	{ };

	template <class T, class Alloc>
	constexpr bool uses_allocator_v = uses_allocator<T, Alloc>::value;

	template <class T>
	struct allocator
	{
		using value_type = T;
		using pointer = T*;
		using const_pointer = const T*;
		using reference = T&;
		using const_reference = const T&;
		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;
		using propagate_on_container_move_assignment = true_type;
		using is_always_equal = true_type;

		template <class U>
		struct rebind
		{
			using other = allocator<U>;
		};

		pointer address(reference x) const
		{
			return mstd::addressof(x);
		}

		const_pointer address(const_reference x) const
		{
			return mstd::addressof(x);
		}

		pointer allocate(size_type n, const void* hint = 0)
		{
			return static_cast<pointer>(::operator new(n * sizeof(T)));
		}

		void deallocate(pointer p, size_type n)
		{
			::operator delete(p);
		}

		size_type max_size() const
		{
			return std::numeric_limits<size_type>::max();
		}

		template <class U,class... Args>
		void construct(U* p, Args&&... args)
		{
			::new((void*)p) U(mstd::forward<Args>(args)...);
		}

		template <class U>
		void destroy(U* p)
		{
			p->~U();
		}
	};

	template <>
	struct allocator<void>
	{
		using value_type = void;
		using pointer = void*;
		using const_pointer = const void*;
		using propagate_on_container_copy_construction = true_type;
		using is_always_equal = true_type;
		
		template <class U>
		struct rebind
		{
			using other = allocator<U>;
		};
	};

	template <class T, class U>
	bool operator==(const allocator<T>& lhs, const allocator<U>& rhs) noexcept
	{
		return true;
	}

	template <class T, class U>
	bool operator!=(const allocator<T>& lhs, const allocator<U>& rhs) noexcept
	{
		return false;
	}

	template <class T>
	struct default_delete
	{
		constexpr default_delete() noexcept = default;

		template <class U, class = enable_if_t<is_convertible<U*, T*>::value>>
		default_delete(const default_delete<U>&  del) noexcept
		{ }

		void operator()(T* ptr) const
		{
			delete ptr;
		}

		template <class U>
		void operator()(U* ptr) const = delete;
	};

	template <class T>
	struct default_delete<T[]>
	{
		using pointer = T*;

		constexpr default_delete() noexcept = default;

		template <class U, class = enable_if_t<is_convertible<T, U>::value>>
		default_delete(const default_delete<U[]>& d) noexcept
		{ }

		template <class U>
		void operator()(U* ptr) const noexcept
		{
			delete[] ptr;
		}
	};

	namespace detail
	{
		template <class T>
		using ptr_t = typename T::pointer;
	}

	template <class T, class D = default_delete<T>>
	class unique_ptr
	{
		template <class U, class E>
		friend class unique_ptr;
	public:
		using element_type = T;
		using deleter_type = D;
		using pointer = experimental::detected_or_t<element_type*, detail::ptr_t, D>;

		constexpr unique_ptr() noexcept
			: ptr_{ }, del_{ }
		{ }
		constexpr unique_ptr(nullptr_t) noexcept : ptr_{ }, del_{ }
		{ }

		explicit unique_ptr(pointer p) noexcept : ptr_{ p }
		{ }

		unique_ptr(pointer p,
				   conditional_t<is_reference<D>::value, deleter_type, const deleter_type&> del) noexcept
			: ptr_{ p }, del_{ del }
		{ }

		unique_ptr(pointer p, remove_reference_t<D>&& del) noexcept
			: ptr_{ p }, del_{ mstd::move(del) }
		{ }

		unique_ptr(unique_ptr&& x) noexcept
			: ptr_{ x.release() }, del_{ mstd::move(x.del_) }
		{ }

		template <class U, class E>
		unique_ptr(unique_ptr<U, E>&& x) noexcept
			: ptr_{ x.release() }, del_{ mstd::move(x.del_) }
		{ }

		unique_ptr(const unique_ptr&) = delete;

		~unique_ptr()
		{
			clean_this();
		}

		unique_ptr& operator=(unique_ptr&& x) noexcept
		{
			if (this != &x)
			{
				clean_this();
				ptr_ = x.ptr_;
				del_ = mstd::forward<deleter_type>(x.del_);
			}
			return *this;
		}

		unique_ptr& operator=(nullptr_t) noexcept
		{
			clean_this();
			return *this;
		}

		template <class U, class E>
		unique_ptr& operator=(unique_ptr<U, E>&& x) noexcept
		{
			clean_this();
			ptr_ = x.ptr_;
			del_ = mstd::forward<deleter_type>(x.del_);
			x.ptr_ = nullptr;
			return *this;
		}

		unique_ptr& operator=(const unique_ptr&) = delete;

		pointer get() const noexcept
		{
			return ptr_;
		}

		deleter_type& get_deleter() noexcept
		{
			return del_;
		}

		const deleter_type& get_deleter() const noexcept
		{
			return del_;
		}

		explicit operator bool() const noexcept
		{
			return ptr_ != nullptr;
		}

		pointer release() noexcept
		{
			return mstd::exchange(ptr_, nullptr);
		}

		void reset(pointer p = pointer{ }) noexcept
		{
			clean_this();
			ptr_ = p;
		}

		void swap(unique_ptr& x) noexcept
		{
			swap(del_, x.del_);
			swap(ptr_, x.ptr_);
		}

		add_lvalue_reference_t<element_type> operator*() const
		{
			return *ptr_;
		}

		pointer operator->() const noexcept
		{
			return ptr_;
		}

	private:
		inline void clean_this() noexcept
		{
			if (ptr_ != pointer{ })
			{
				del_(ptr_);
				ptr_ = nullptr;
			}
		}

	private:
		pointer ptr_;
		deleter_type del_;
	};

	template <class T, class D>
	class unique_ptr<T[], D>
	{

	public:
		using element_type = T;
		using deleter_type = D;
		using pointer = experimental::detected_or_t<element_type*, detail::ptr_t, D>;

		constexpr unique_ptr() noexcept
			: ptr_{ }, del_{ }
		{ }

		constexpr unique_ptr(nullptr_t) noexcept : ptr_{ }, del_{ }
		{ }

		explicit unique_ptr(pointer p) noexcept : ptr_{ p }
		{ }

		unique_ptr(pointer p,
				   conditional_t<is_reference<D>::value, deleter_type, const deleter_type&> del) noexcept
			: ptr_{ p }, del_{ del }
		{ }

		unique_ptr(pointer p, remove_reference_t<D>&& del) noexcept
			: ptr_{ p }, del_{ mstd::move(del) }
		{ }

		unique_ptr(unique_ptr&& x) noexcept
			: ptr_{ x.release() }, del_{ mstd::move(x.del_) }
		{ }

		template <class U, class E>
		unique_ptr(unique_ptr<U, E>&& x) noexcept
			: ptr_{ x.release() }, del_{ mstd::move(x.del_) }
		{ }

		unique_ptr(const unique_ptr&) = delete;

		~unique_ptr()
		{
			clean_this();
		}

		unique_ptr& operator=(unique_ptr&& x) noexcept
		{
			if (this != &x)
			{
				clean_this();
				ptr_ = x.ptr_;
				del_ = mstd::forward<deleter_type>(x.del_);
			}
			return *this;
		}

		unique_ptr& operator=(nullptr_t) noexcept
		{
			clean_this();
			return *this;
		}

		template <class U, class E>
		unique_ptr& operator=(unique_ptr<U, E>&& x) noexcept
		{
			clean_this();
			ptr_ = x.ptr_;
			del_ = mstd::forward<deleter_type>(x.del_);
			return *this;
		}

		unique_ptr& operator=(const unique_ptr&) = delete;

		pointer get() const noexcept
		{
			return ptr_;
		}

		deleter_type& get_deleter() noexcept
		{
			return del_;
		}

		const deleter_type& get_deleter() const noexcept
		{
			return del_;
		}

		explicit operator bool() const noexcept
		{
			return ptr_ != nullptr;
		}

		pointer release() noexcept
		{
			auto tmp = ptr_;
			ptr_ = nullptr;
			return tmp;
		}

		void reset(pointer p = pointer{ }) noexcept
		{
			clean_this();
			ptr_ = p;
		}

		void swap(unique_ptr& x) noexcept
		{
			swap(ptr_, x.ptr_);
			swap(del_, x.del_);
		}

		add_lvalue_reference_t<element_type> operator[](std::size_t i) const
		{
			return ptr_[i];
		}

	private:
		inline void clean_this() noexcept
		{
			if (ptr_ != pointer{ })
				del_(ptr_);
		}

	private:
		pointer ptr_;
		deleter_type del_;
	};

	template <class T, class... Args, class = enable_if_t<!is_array<T>::value>>
	unique_ptr<T> make_unique(Args&&... args)
	{
		return unique_ptr<T>{ new T{ mstd::forward<Args>(args)... } };
	}

	template <class T, class = enable_if_t<is_array<T>::value && extent<T>::value == 0>>
	unique_ptr<T> make_unique(std::size_t size)
	{
		using element_type = remove_extent_t<T>;
		return unique_ptr<T>{ new element_type[size]{ } };
	}

	template <class T, class... Args, class = enable_if_t<is_array<T>::value && extent<T>::value != 0>>
	void make_unique(Args&&...) = delete;

	template <class T1, class D1, class T2, class D2>
	bool operator== (const unique_ptr<T1, D1>& lhs, const unique_ptr<T2, D2>& rhs)
	{
		return lhs.get() == rhs.get();
	}

	template <class T, class D>
	bool operator==(const unique_ptr<T, D>& lhs, nullptr_t) noexcept
	{
		return lhs.get() == nullptr;
	}

	template <class T, class D>
	bool operator==(nullptr_t, const unique_ptr<T, D>& rhs) noexcept
	{
		return nullptr == rhs.get();
	}

	template <class T1, class D1, class T2, class D2>
	bool operator!=(const unique_ptr<T1, D1>& lhs, const unique_ptr<T2, D2>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class T, class D>
	bool operator!=(const unique_ptr<T, D>& lhs, nullptr_t) noexcept
	{
		return !(lhs == nullptr);
	}

	template <class T, class D>
	bool operator!=(nullptr_t, const unique_ptr<T, D>& rhs) noexcept
	{
		return !(rhs == nullptr);
	}

	template <class T1, class D1, class T2, class D2>
	bool operator<(const unique_ptr<T1, D1>& lhs, const unique_ptr<T2, D2>& rhs)
	{
		/*using common_t = common_type_t<typename unique_ptr<T1, D1>::pointer, typename unique_ptr<T2, D2>::pointer>;
		return less<common_t>{}(lhs.get(), rhs.get());*/
		return less<>{}(lhs.get(), rhs.get());
	}

	template <class T, class D> bool operator<(const unique_ptr<T, D>& lhs, nullptr_t) noexcept
	{
		//return less<typename unique_ptr<T, D>::pointer>{}(lhs.get(), nullptr);
		return less<>{}(lhs.get(), nullptr);
	}

	template <class T, class D>
	bool operator<(nullptr_t, const unique_ptr<T, D>& rhs) noexcept
	{
		//return less<typename unique_ptr<T, D>::pointer>{}(nullptr, rhs.get());
		return less<>{}(nullptr, rhs.get());
	}

	template <class T1, class D1, class T2, class D2>
	bool operator<=(const unique_ptr<T1, D1>& lhs, const unique_ptr<T2, D2>& rhs)
	{
		return !(rhs < lhs);
	}

	template <class T, class D> bool operator<=(const unique_ptr<T, D>& lhs, nullptr_t) noexcept
	{
		return !(nullptr < lhs);
	}

	template <class T, class D> bool operator<=(nullptr_t, const unique_ptr<T, D>& rhs) noexcept
	{
		return !(rhs < nullptr);
	}

	template <class T1, class D1, class T2, class D2>
	bool operator>(const unique_ptr<T1, D1>& lhs, const unique_ptr<T2, D2>& rhs)
	{
		return rhs < lhs;
	}

	template <class T, class D>
	bool operator>(const unique_ptr<T, D>& lhs, nullptr_t) noexcept
	{
		return nullptr < lhs;
	}

	template <class T, class D> bool operator>(nullptr_t, const unique_ptr<T, D>& rhs) noexcept
	{
		return rhs < nullptr;
	}

	template <class T1, class D1, class T2, class D2>
	bool operator>=(const unique_ptr<T1, D1>& lhs, const unique_ptr<T2, D2>& rhs)
	{
		return !(lhs < rhs);
	}

	template <class T, class D>
	bool operator>=(const unique_ptr<T, D>& lhs, nullptr_t) noexcept
	{
		return !(lhs < nullptr);
	}

	template <class T, class D>
	bool operator>=(nullptr_t, const unique_ptr<T, D>& rhs) noexcept
	{
		return !(nullptr < rhs);
	}

	template <class T>
	class shared_ptr;

	namespace detail
	{
		template <class T>
		class ref_count_base
		{
		public:
			using value_type = T;
			using counter_type = std::atomic<long>;

			virtual value_type* getPtr() noexcept = 0;
			virtual void destroy() noexcept = 0;
			virtual void delete_this() noexcept = 0;


			virtual void* getDeleter(const std::type_info& info) const noexcept
			{
				return nullptr;
			}

			counter_type strong_count() const noexcept
			{
				return strong_count_;
			}

			bool isExpired() const noexcept
			{
				return strong_count_ == 0;
			}

			void incstrong() noexcept
			{
				++strong_count_;
			}

			void incweak() noexcept
			{
				++weak_count_;
			}

			void decstrong() noexcept
			{
				if (--strong_count_ == 0)
				{
					destroy();
					decweak();
				}
			}

			void decweak() noexcept
			{
				if (--weak_count_ == 0)
				{
					delete_this();
				}
			}

			virtual ~ref_count_base() = default;

		private:
			counter_type strong_count_{ 1 };
			counter_type weak_count_{ 1 };
		};

		template <class T>
		class ref_count final : public ref_count_base<T>
		{
			using value_type = typename ref_count_base<T>::value_type;
			value_type* ptr_;

		public:
			ref_count(T* p) : ptr_{ p }
			{ }

		private:
			virtual value_type* getPtr() noexcept override
			{
				return ptr_;
			}

			virtual void destroy() noexcept override
			{
				delete ptr_;
			}

			virtual void delete_this() noexcept override
			{
				delete this;
			}
		};

		template <class T, class D>
		class ref_count_del : public ref_count_base<T>
		{
			using value_type = typename ref_count_base<T>::value_type;
			value_type* ptr_;
			D delter_;
		public:
			ref_count_del(value_type* p, D del) : ptr_{ p }, delter_{ del }
			{ }

		private:
			virtual value_type* getPtr() noexcept override
			{
				return ptr_;
			}

			virtual void destroy() noexcept override
			{
				delter_(ptr_);
			}

			virtual void delete_this() noexcept override
			{
				delete this;
			}
		};

		template <class T, class D, class Alloc>
		class ref_count_del_alloc final : public ref_count_base<T>
		{
			using value_type = typename ref_count_base<T>::value_type;
			using real_alloc = typename std::allocator_traits<Alloc>::template rebind_alloc<ref_count_del_alloc>;
			value_type* ptr_;
			D delter_;
			real_alloc alloc_;

		public:
			ref_count_del_alloc(value_type* p, D del, real_alloc allo) : ptr_{ p }, delter_{ del }, alloc_{ allo }
			{ }

			virtual value_type* getPtr() noexcept override
			{
				return ptr_;
			}

		private:
			virtual void destroy() noexcept override
			{
				delter_(ptr_);
			}

			virtual void delete_this() noexcept override
			{
				std::allocator_traits<real_alloc>::destroy(alloc_, this);
				std::allocator_traits<real_alloc>::deallocate(alloc_, this, 1);
			}
		};

		template <class T>
		class obj_ref_count final : public ref_count_base<T>
		{
			using value_type = typename ref_count_base<T>::value_type;
			aligned_union<1, T> storage_;
		public:
			template<class... Args>
			obj_ref_count(Args&&... args)
			{
				new (getPtr()) T{ mstd::forward<Args>(args)... };
			}

			virtual value_type* getPtr() noexcept override
			{
				return reinterpret_cast<T*>(&storage_);
			}

		private:
			virtual void destroy() noexcept override
			{
				getPtr()->~T();
			}

			virtual void delete_this() noexcept override
			{
				delete this;
			}
		};

		template <class T, class Alloc>
		class obj_ref_count_alloc final : public ref_count_base<T>
		{
			using value_type = typename ref_count_base<T>::value_type;
			using real_alloc = typename std::allocator_traits<Alloc>::template rebind_alloc<obj_ref_count_alloc>;
			aligned_union<1, T> storage_;
			real_alloc alloc_;
		public:
			template <class... Args>
			obj_ref_count_alloc(const Alloc& alloc, Args&&... args)
			{
				new (getPtr()) T{ mstd::forward<Args>(args)... };
			}

			virtual value_type* getPtr() noexcept override
			{
				return reinterpret_cast<T*>(&storage_);
			}

		private:
			virtual void destroy() noexcept override
			{
				getPtr()->~T();
			}

			virtual void delete_this() noexcept override
			{
				std::allocator_traits<real_alloc>::destroy(alloc_, this);
				std::allocator_traits<real_alloc>::deallocate(alloc_, this, 1);
			}
		};

		template<class T>
		inline void make_and_allocate_shared_helper(ref_count_base<T>*& count, shared_ptr<T>& sptr)
		{
			sptr.count_ = count;
			sptr.ptr_ = count->getPtr();
		}
	}

	template <class T>
	class weak_ptr;

	template <class T>
	class shared_ptr
	{
	public:
		using element_type = T;

		template<class U>
		friend void detail::make_and_allocate_shared_helper(detail::ref_count_base<U>*& count, shared_ptr<U>& sptr);

		template <class>
		friend class weak_ptr;

	private:
		element_type* ptr_;
		detail::ref_count_base<T>* count_;

	public:
		constexpr shared_ptr() noexcept : count_{ nullptr }, ptr_{ nullptr }
		{ }

		constexpr shared_ptr(nullptr_t) : shared_ptr{ }
		{ }

		template <class U>
		explicit shared_ptr(U* p)
		{
			try
			{
				count_ = new detail::ref_count<T>{ p };
				ptr_ = p;
			}
			catch (...)
			{
				delete p;
				throw;
			}
		}

		template <class U, class D>
		shared_ptr(U* p, D del)
		{
			try
			{
				count_ = new detail::ref_count_del<T, D>{ p , del };
				ptr_ = p;
			}
			catch (...)
			{
				del(p);
				throw;
			}
		}

		template <class D>
		shared_ptr(nullptr_t p, D del)
			: count_{ new detail::ref_count_del<T, D>{ p, del } }, ptr_{ p }
		{ }

		template <class U, class D, class Alloc>
		shared_ptr(U* p, D del, Alloc alloc)
		{
			using real_alloc = typename std::allocator_traits<Alloc>::template rebind_alloc<detail::ref_count_del_alloc<U, D, Alloc>>;
			real_alloc alloc_;
			try
			{
				count_ = std::allocator_traits<real_alloc>::allocate(alloc_, 1);
				std::allocator_traits<real_alloc>::construct(alloc_, static_cast<detail::ref_count_del_alloc<U, D, Alloc>*>(count_), p, del, alloc_);
				ptr_ = p;
			}
			catch (...)
			{
				del(p);
				throw;
			}
		}

		template <class D, class Alloc>
		shared_ptr(nullptr_t p, D del, Alloc alloc) : ptr_{ p }
		{
			using real_alloc = typename std::allocator_traits<Alloc>::template rebind_alloc<detail::ref_count_del_alloc<T, D, Alloc>>;
			real_alloc alloc_;

			count_ = std::allocator_traits<real_alloc>::allocate(alloc_, 1);
			std::allocator_traits<real_alloc>::construct(alloc_, static_cast<detail::ref_count_del_alloc<T, D, Alloc>*>(count_), p, del, alloc_);
		}

		shared_ptr(const shared_ptr& x) noexcept
			: count_{ x.count_ }, ptr_{ x.ptr_ }
		{
			count_->incstrong();
		}

		template <class U>
		shared_ptr(const shared_ptr<U>& x) noexcept
			: count_{ x.count_ }, ptr_{ x.ptr_ }
		{
			count_->incstrong();
		}

		template <class U>
		explicit shared_ptr(const weak_ptr<U>& x)
			: ptr_{ x.ptr_ }, count_{ x.count_ }
		{
			if (count_)
			{
				count_->incstrong();
			}
		}

		shared_ptr(shared_ptr&& x) noexcept
			: count_{ x.count_ }, ptr_{ x.ptr_ }
		{
			x.count_ = nullptr;
			x.ptr_ = nullptr;
		}

		template <class U>
		shared_ptr(shared_ptr<U>&& x) noexcept
			: count_{ x.count_ }, ptr_{ x.ptr_ }
		{
			x.count_ = nullptr;
			x.ptr_ = nullptr;
		}

		template <class U, class D>
		shared_ptr(unique_ptr<U, D>&& x)
		{
			try
			{
				ptr_ = x.release();
				count_ = new detail::ref_count_del<T, D>{ ptr_, x.get_deleter() };
			}
			catch (...)
			{
				x.get_deleter()(ptr_);
				throw;
			}
		}

		//TODO aliasing ctor may not working by copying because count_.ptr_ is != ptr_ 
		template <class U>
		shared_ptr(const shared_ptr<U>& other, element_type* p) noexcept
			: count_{ other.count_ }, ptr_{ other.ptr_ }
		{
			if (count_)
				count_->incstrong();
		}

		shared_ptr& operator=(const shared_ptr& x) noexcept
		{
			shared_ptr{ x }.swap(*this);
			return *this;
		}

		template <class U>
		shared_ptr& operator=(const shared_ptr<U>& x) noexcept
		{
			shared_ptr{ x }.swap(*this);
			return *this;
		}

		shared_ptr& operator=(shared_ptr&& x) noexcept
		{
			shared_ptr{ mstd::move(x) }.swap(*this);
			return *this;
		}

		template <class U>
		shared_ptr& operator=(shared_ptr<U>&& x) noexcept
		{
			shared_ptr{ mstd::move(x) }.swap(*this);
			return *this;
		}

		template <class U, class D>
		shared_ptr& operator=(unique_ptr<U, D>&& x)
		{
			shared_ptr{ mstd::move(x) }.swap(*this);
			return *this;
		}

		void swap(shared_ptr& x) noexcept
		{
			swap(ptr_, x.ptr_);
			swap(count_, x.count_);
		}

		void reset() noexcept
		{
			shared_ptr{ }.swap(*this);
		}

		template <class U>
		void reset(U* p)
		{
			shared_ptr{ p }.swap(*this);
		}

		template <class U, class D>
		void reset(U* p, D del)
		{
			shared_ptr{ p, del }.swap(*this);
		}

		template <class U, class D, class Alloc>
		void reset(U* p, D del, Alloc alloc)
		{
			shared_ptr{ p, del, alloc }.swap(*this);
		}

		element_type* get() const noexcept
		{
			return ptr_;
		}

		element_type& operator*() const noexcept
		{
			return *ptr_;
		}

		element_type* operator->() const noexcept
		{
			return ptr_;
		}

		long use_count() const noexcept
		{
			return count_->strong_count();
		}

		bool unique() const noexcept
		{
			return count_->strong_count() == 1;
		}

		explicit operator bool() const noexcept
		{
			return ptr_ != nullptr;
		}

		~shared_ptr()
		{
			if (count_)
			{
				count_->decstrong();
			}
		}

		template <class U>
		bool owner_before(const shared_ptr<U>& x) const
		{
			return count_ < x.count_;
		}

		template <class U>
		bool owner_before(const weak_ptr<U>& x) const
		{
			return count_ < x.use_count();
		}
	};

	template <class T, class... Args>
	shared_ptr<T> make_shared(Args&&... args)
	{
		detail::ref_count_base<T>* count_ = new detail::obj_ref_count<T>(mstd::forward<Args>(args)...);

		shared_ptr<T> sptr_;
		detail::make_and_allocate_shared_helper<T>(count_, sptr_);
		return sptr_;
	}

	template <class T, class Alloc, class... Args>
	shared_ptr<T> allocate_shared(const Alloc& alloc, Args&&... args)
	{
		detail::ref_count_base<T>* count_ = new detail::obj_ref_count_alloc<T, Alloc>(alloc, mstd::forward<Args>(args)...);

		shared_ptr<T> sptr_;
		detail::make_and_allocate_shared_helper<T>(count_, sptr_);
		return sptr_;
	}

	template <class T, class U>
	bool operator==(const shared_ptr<T>& lhs, const shared_ptr<U>& rhs) noexcept
	{
		return lhs.get() == rhs.get();
	}

	template <class T>
	bool operator==(const shared_ptr<T>& lhs, nullptr_t) noexcept
	{
		return lhs.get() == nullptr;
	}

	template <class T>
	bool operator==(nullptr_t, const shared_ptr<T>& rhs) noexcept
	{
		return nullptr == rhs.get();
	}

	template <class T>
	bool operator!=(const shared_ptr<T>& lhs, nullptr_t) noexcept
	{
		return !(lhs == nullptr);
	}

	template <class T>
	bool operator!=(nullptr_t, const shared_ptr<T>& rhs) noexcept
	{
		return !(nullptr == rhs);
	}
	/*
		template <class T, class U>
		bool operator<(const shared_ptr<T>& lhs, const shared_ptr<U>& rhs) noexcept
		{
			return less < //????????????????????
		}

		template <class T>
		bool operator<  (const shared_ptr<T>& lhs, nullptr_t) noexcept
		{
			//?????????
		}

		template <class T>
		bool operator<  (nullptr_t, const shared_ptr<T>& rhs) noexcept
		{
			//???????
		}

		template <class T>
		bool operator<= (const shared_ptr<T>& lhs, nullptr_t) noexcept
		{
			//???????
		}

		template <class T>
		bool operator<= (nullptr_t, const shared_ptr<T>& rhs) noexcept
		{

		}

		template <class T> bool operator>(const shared_ptr<T>& lhs, nullptr_t) noexcept;
		template <class T> bool operator>(nullptr_t, const shared_ptr<T>& rhs) noexcept;

		template <class T> bool operator>=(const shared_ptr<T>& lhs, nullptr_t) noexcept;
		template <class T> bool operator>=(nullptr_t, const shared_ptr<T>& rhs) noexcept;
		*/

	struct bad_weak_ptr : exception
	{
		using exception::exception;
	};

	template <class T>
	class weak_ptr
	{
		template <class U>
		friend class shared_ptr;
	private:
		T* ptr_;
		detail::ref_count_base<T>* count_;

	public:
		using element_type = T;

		constexpr weak_ptr() noexcept : ptr_{ nullptr }, count_{ nullptr }
		{ }

		weak_ptr(const weak_ptr& x) noexcept
			: count_{ x.count_ }, ptr_{ x.ptr_ }
		{
			if (x.count_)
			{
				x.count_->incweak();
			}
		}

		template <class U>
		weak_ptr(const weak_ptr<U>& x) noexcept
			: count_{ x.count_ }, ptr_{ x.ptr_ }
		{
			if (x.count_)
			{
				x.count_->incweak();
			}
		}

		template <class U>
		weak_ptr(const shared_ptr<U>& x) noexcept
			: count_{ x.count_ }, ptr_{ x.ptr_ }
		{
			if (x.count_)
			{
				x.count_->incweak();
			}
		}

		weak_ptr(weak_ptr&& x) noexcept
			: ptr_{ x.ptr_ }, count_{ x.count_ }
		{
			x.ptr_ = nullptr;
			x.count_ = nullptr;
		}

		template <class U>
		weak_ptr(weak_ptr<U>&& x) noexcept
			: ptr_{ x.ptr_ }, count_{ x.count_ }
		{
			x.ptr_ = nullptr;
			x.count_ = nullptr;
		}

		~weak_ptr()
		{
			if (count_)
			{
				count_->decweak();
			}
		}

		weak_ptr& operator=(const weak_ptr& x) noexcept
		{
			weak_ptr{ x }.swap(*this);
		}

		template <class U>
		weak_ptr& operator=(const weak_ptr<U>& x) noexcept
		{
			weak_ptr{ x }.swap(*this);
		}

		template <class U>
		weak_ptr& operator=(const shared_ptr<U>& x) noexcept
		{
			if (x.count_)
			{
				x.count_->incweak();
			}
			count_ = x.count_;
			ptr_ = x.ptr_;
		}

		weak_ptr& operator=(weak_ptr&& x) noexcept
		{
			weak_ptr{ mstd::move(x) }.swap(*this);
		}

		template <class U>
		weak_ptr& operator=(weak_ptr<U>&& x) noexcept
		{
			weak_ptr{ mstd::move(x) }.swap(*this);
		}

		void swap(weak_ptr& x) noexcept
		{
			swap(ptr_, x.ptr_);
			swap(count_, x.count_);
		}

		void reset() noexcept
		{
			weak_ptr{ }.swap(*this);
		}

		long use_count() const noexcept
		{
			return count_->strong_count();
		}

		bool expired() const noexcept
		{
			return count_ || (count_->strong_count() == 0);
		}

		shared_ptr<element_type> lock() const noexcept
		{
			shared_ptr<element_type> sptr;
			if (count_)
			{
				count_->incstrong();
			}
			sptr.count_ = count_;
			sptr.ptr_ = ptr_;
			return sptr;
		}
	};
}
