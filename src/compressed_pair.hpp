#pragma once

#include "utility.hpp" //piecewise_construct, index_sequence
#include "tuple.hpp"
#include "type_traits.hpp"

namespace mstd
{
	namespace detail
	{
		struct construct_first_t
		{
		};

		struct construct_second_t
		{
		};

		struct construct_both_t
		{
		};

		constexpr construct_first_t construct_first{};
		constexpr construct_second_t construct_second{};
		constexpr construct_both_t construct_both{};

		template <class T1, class T2, bool = is_empty<T1>::value && !is_final<T1>::value, bool = is_empty<T2>::value && !is_final<T2>::value>
		class compressed_pair final : private T1
		{
		public:
			constexpr compressed_pair()
				noexcept(is_nothrow_default_constructible<T1>::value
					&& is_nothrow_default_constructible<T2>::value) = default;

			template <class... Args>
			constexpr compressed_pair(construct_first_t, Args&&... args)
				noexcept(is_nothrow_constructible<T1, Args...>::value
					&& is_nothrow_default_constructible<T2>::value)
				: T1{ mstd::forward<Args>(args)... }, second_{}
			{
			}

			template <class... Args>
			constexpr compressed_pair(construct_second_t, Args&&... args)
				noexcept(is_nothrow_default_constructible<T1>::value
					&& is_nothrow_constructible<T2, Args...>::value)
				: second_{ mstd::forward<Args>(args)... }
			{
			}

			template <class U1, class U2>
			constexpr compressed_pair(construct_both_t, U1&& first, U2&& second)
				noexcept(is_nothrow_constructible<T1, U2>::value && is_nothrow_constructible<T2, U2>::value)
				: T1{ mstd::forward<U1>(first) }, second_{ mstd::forward<U2>(second) }
			{
			}

		private:
			template <class... Args1, class... Args2, std::size_t... I1, std::size_t... I2>
			constexpr compressed_pair(piecewise_construct_t, tuple<Args1...>& args1, tuple<Args2...>& args2,
				index_sequence<I1...>, index_sequence<I2...>)
				noexcept(is_nothrow_constructible<T1, Args1...>::value
					&& is_nothrow_constructible<T2, Args2...>::value)
				: T1{ mstd::get<I1>(mstd::move(args1))... }, second_{ mstd::get<I2>(mstd::move(args2))... }
			{
			}

		public:
			template <class... Args1, class... Args2>
			constexpr compressed_pair(piecewise_construct_t, tuple<Args1...> args1, tuple<Args2...> args2)
				noexcept(is_nothrow_constructible<T1, Args1...>::value
					&& is_nothrow_constructible<T2, Args2...>::value)
				: compressed_pair{ piecewise_construct, args1, args2, mstd::make_index_sequence<sizeof...(Args1)>(), mstd::make_index_sequence<sizeof...(Args2)>() }

			{
			}

			/*constexpr*/ T1& first() noexcept
			{
				return static_cast<T1&>(*this);
			}

			constexpr const T1& first() const noexcept
			{
				return static_cast<const T1&>(*this);
			}

			/*constexpr*/ volatile T1& first() volatile noexcept
			{
				return static_cast<volatile T1&>(*this);
			}

			constexpr const volatile T1& first() const volatile noexcept
			{
				return static_cast<const volatile T1&>(*this);
			}

			/*constexpr*/ T2& second() noexcept
			{
				return second_;
			}

			constexpr const T2& second() const noexcept
			{
				return second_;
			}

			/*constexpr*/ volatile T2& second() volatile noexcept
			{
				return second_;
			}

			constexpr const volatile T2& second() const volatile noexcept
			{
				return second_;
			}

		private:
			T2 second_;
		};

		template <class T1, class T2>
		class compressed_pair<T1, T2, false, true> final : private T2
		{
		public:
			constexpr compressed_pair()
				noexcept(is_nothrow_default_constructible<T1>::value
					&& is_nothrow_default_constructible<T2>::value) = default;

			template <class... Args>
			constexpr compressed_pair(construct_first_t, Args&&... args)
				noexcept(is_nothrow_constructible<T1, Args...>::value
					&& is_nothrow_default_constructible<T2>::value)
				: T2{}, first_{ mstd::forward<Args>(args)... }
			{
			}

			template <class... Args>
			constexpr compressed_pair(construct_second_t, Args&&... args)
				noexcept(is_nothrow_default_constructible<T1>::value
					&& is_nothrow_constructible<T2, Args...>::value)
				: T2{ mstd::forward<Args>(args)... }, first_{}
			{
			}

			template <class U1, class U2>
			constexpr compressed_pair(construct_both_t, U1&& first, U2&& second)
				noexcept(is_nothrow_constructible<T1, U2>::value && is_nothrow_constructible<T2, U2>::value)
				: T2{ mstd::forward<U2>(second) }, first_{ mstd::forward<U1>(first) }
			{
			}

		private:
			template <class... Args1, class... Args2, std::size_t... I1, std::size_t... I2>
			constexpr compressed_pair(piecewise_construct_t, tuple<Args1...>& args1, tuple<Args2...>& args2,
				index_sequence<I1...>, index_sequence<I2...>)
				noexcept(is_nothrow_constructible<T1, Args1...>::value
					&& is_nothrow_constructible<T2, Args2...>::value)
				: T2{ mstd::get<I2>(mstd::move(args2))... }, first_{ mstd::get<I1>(mstd::move(args1))... }
			{
			}

		public:
			template <class... Args1, class... Args2>
			constexpr compressed_pair(piecewise_construct_t, tuple<Args1...> args1, tuple<Args2...> args2)
				noexcept(is_nothrow_constructible<T1, Args1...>::value
					&& is_nothrow_constructible<T2, Args2...>::value)
				: compressed_pair{ piecewise_construct, args1, args2, mstd::make_index_sequence<sizeof...(Args1)>(), mstd::make_index_sequence<sizeof...(Args2)>() }

			{
			}

			/*constexpr*/ T1& first() noexcept
			{
				return first_;
			}

			constexpr const T1& first() const noexcept
			{
				return first_;
			}

			/*constexpr*/ volatile T1& first() volatile noexcept
			{
				return first_;
			}

			constexpr const volatile T1& first() const volatile noexcept
			{
				return first_;
			}

			/*constexpr*/ T2& second() noexcept
			{
				return static_cast<T2&>(*this);
			}

			constexpr const T2& second() const noexcept
			{
				return static_cast<const T2&>(*this);
			}

			/*constexpr*/ volatile T2& second() volatile noexcept
			{
				return static_cast<volatile T2&>(*this);
			}

			constexpr const volatile T2& second() const volatile noexcept
			{
				return static_cast<const volatile T2&>(*this);
			}

		private:
			T1 first_{};
		};

		template <class T1, class T2>
		class compressed_pair<T1, T2, false, false> final
		{
		public:
			constexpr compressed_pair()
				noexcept(is_nothrow_default_constructible<T1>::value
					&& is_nothrow_default_constructible<T2>::value) = default;

			template <class... Args>
			constexpr compressed_pair(construct_first_t, Args&&... args)
				noexcept(is_nothrow_constructible<T1, Args...>::value
					&& is_nothrow_default_constructible<T2>::value)
				: first_{ mstd::forward<Args>(args)... }, second_{}
			{
			}

			template <class... Args>
			constexpr compressed_pair(construct_second_t, Args&&... args)
				noexcept(is_nothrow_default_constructible<T1>::value
					&& is_nothrow_constructible<T2, Args...>::value)
				: first_{}, second_{ mstd::forward<Args>(args)... }
			{
			}

			template <class U1, class U2>
			constexpr compressed_pair(construct_both_t, U1&& first, U2&& second)
				noexcept(is_nothrow_constructible<T1, U2>::value && is_nothrow_constructible<T2, U2>::value)
				: first_{ mstd::forward<U1>(first) }, second_{ mstd::forward<U2>(second) }
			{
			}

		private:
			template <class... Args1, class... Args2, std::size_t... I1, std::size_t... I2>
			constexpr compressed_pair(piecewise_construct_t, tuple<Args1...>& args1, tuple<Args2...>& args2,
				index_sequence<I1...>, index_sequence<I2...>)
				noexcept(is_nothrow_constructible<T1, Args1...>::value
					&& is_nothrow_constructible<T2, Args2...>::value)
				: first_{ mstd::get<I1>(mstd::move(args1))... }, second_{ mstd::get<I2>(mstd::move(args2))... }
			{
			}

		public:
			template <class... Args1, class... Args2>
			constexpr compressed_pair(piecewise_construct_t, tuple<Args1...> args1, tuple<Args2...> args2)
				noexcept(is_nothrow_constructible<T1, Args1...>::value
					&& is_nothrow_constructible<T2, Args2...>::value)
				: compressed_pair{ piecewise_construct, args1, args2, mstd::make_index_sequence<sizeof...(Args1)>(), mstd::make_index_sequence<sizeof...(Args2)>() }

			{
			}

			/*constexpr*/ T1& first() noexcept
			{
				return first_;
			}

			constexpr const T1& first() const noexcept
			{
				return first_;
			}

			/*constexpr*/ volatile T1& first() volatile noexcept
			{
				return first_;
			}

			constexpr const volatile T1& first() const volatile noexcept
			{
				return first_;
			}

			/*constexpr*/ T2& second() noexcept
			{
				return second_;
			}

			constexpr const T2& second() const noexcept
			{
				return second_;
			}

			/*constexpr*/ volatile T2& second() volatile noexcept
			{
				return second_;
			}

			constexpr const volatile T2& second() const volatile noexcept
			{
				return second_;
			}

		private:
			T1 first_;
			T2 second_;
		};
	}
}
