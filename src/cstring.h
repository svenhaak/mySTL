#pragma once
#include <cstddef>

namespace mstd
{
	char* strcpy(char* dest, const char* src);
	char* strncpy(char* dest, const char* src, std::size_t count);
	char* strcat(char* dest, const char* src);
	char* strncat(char* dest, const char* src, std::size_t count);
	//TODO std::size_t strxfrm(char* dest, const char* src, std::size_t count);

	std::size_t strlen(const char* str);
	int strcmp(const char* lhs, const char* rhs);
	int strncmp(const char* lhs, const char* rhs, std::size_t count);
	//TODO int strcoll(const char* lhs, const char* rhs);
	char* strchr(char* str, int ch);
	const char* strchr(const char* str, int ch);
	char* strrchr(char* str, int ch);
	const char* strrchr(const char* str, int ch);
	std::size_t strspn(const char* dest, const char* src);
	std::size_t strcspn(const char* dest, const char* src);
	char* strpbrk(char* dest, const char* breakset);
	const char* strpbrk(const char* dest, const char* breakset);
	const char* strstr(const char* str, const char* target);
	char* strtok(char* str, const char* delim);

	const void* memchr(const void* ptr, int ch, std::size_t count);
	void* memchr(void* ptr, int ch, std::size_t count);
	int memcmp(const void* lhs, const void* rhs, std::size_t count);
	void* memset(void* dest, int ch, std::size_t count);
	void* memcpy(void* dest, const void* src, std::size_t count);
	void* memmove(void* dest, const void* src, std::size_t count);

	//TODO char* strerror(int errnum);
}
