#pragma once

#include "../utility/move_and_forward.hpp"

namespace mstd
{
	template <class T = void>
	struct plus
	{
		using result_type = T;
		using first_argument_type = T;
		using secons_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const secons_argument_type& rhs) const
		{
			return lhs + rhs;
		}
	};

	template <>
	struct plus<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) + mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) + mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct minus
	{
		using result_type = T;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs - rhs;
		}
	};

	template <>
	struct minus<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) - mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) - mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct multiplies
	{
		using result_type = T;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs * rhs;
		}
	};

	template <>
	struct multiplies<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) * mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) * mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct divides
	{
		using result_type = T;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs / rhs;
		}
	};

	template <>
	struct divides<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) / mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) / mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct modulus
	{
		using result_type = T;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs % rhs;
		}
	};

	template <>
	struct modulus<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) % mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) % mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct negate
	{
		using  result_type = T;
		using argument_type = T;

		constexpr result_type operator()(const argument_type& arg) const
		{
			return -arg;
		}
	};

	template <>
	struct negate<void>
	{
		template <class T>
		constexpr auto operator()(T&& arg) const -> decltype(-mstd::forward<T>(arg))
		{
			return -mstd::forward<T>(arg);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct less
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs < rhs;
		}
	};

	template <>
	struct less<void>
	{
		template <class T, class U>
		auto operator()(T&& lhs, U&& rhs) const
			-> decltype(mstd::forward<T>(lhs) < mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) < mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct greater
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs > rhs;
		}
	};

	template <>
	struct greater<void>
	{
		template <class T, class U>
		auto operator()(T&& lhs, U&& rhs) const
			-> decltype(mstd::forward<T>(lhs) > mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) > mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct equal_to
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs == rhs;
		}
	};

	template <>
	struct equal_to<void>
	{
		template <class T, class U>
		auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) == mstd::forward<U>(lhs))
		{
			return mstd::forward<T>(lhs) == mstd::forward<U>(lhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct less_equal
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs <= rhs;
		}
	};

	template <>
	struct less_equal<void>
	{
		template <class T, class U>
		auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) <= mstd::forward<U>(lhs))
		{
			return mstd::forward<T>(lhs) <= mstd::forward<U>(lhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct greater_equal
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs >= rhs;
		}
	};

	template <>
	struct greater_equal<void>
	{
		template <class T, class U>
		auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) >= mstd::forward<U>(lhs))
		{
			return mstd::forward<T>(lhs) >= mstd::forward<U>(lhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct not_equal_to
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs != rhs;
		}
	};

	template <>
	struct not_equal_to<void>
	{
		template <class T, class U>
		auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) != mstd::forward<U>(lhs))
		{
			return mstd::forward<T>(lhs) != mstd::forward<U>(lhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct logical_and
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs && rhs;
		}
	};

	template <>
	struct logical_and<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) && mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) && mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct logical_or
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs || rhs;
		}
	};

	template <>
	struct logical_or<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) || mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) || mstd::forward<U>(rhs);
		}

		using is_transparent = void;
	};

	template <class T = void>
	struct logical_not
	{
		using result_type = bool;
		using argument_type = T;

		constexpr result_type operator()(const argument_type& arg) const
		{
			return !arg;
		}
	};

	template <>
	struct logical_not<void>
	{
		template <class T>
		constexpr auto operator()(T&& arg) const -> decltype(!arg)
		{
			return !arg;
		}

		using is_transparend = void;
	};

	template <class T = void>
	struct bit_and
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs & rhs;
		}
	};

	template <>
	struct bit_and<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) & mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) & mstd::forward<U>(rhs);
		}

		using is_transparend = void;
	};

	template <class T>
	struct bit_or
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs | rhs;
		}
	};

	template <>
	struct bit_or<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) | mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) | mstd::forward<U>(rhs);
		}

		using is_transparend = void;
	};

	template <class T>
	struct bit_xor
	{
		using result_type = bool;
		using first_argument_type = T;
		using second_argument_type = T;

		constexpr result_type operator()(const first_argument_type& lhs, const second_argument_type& rhs) const
		{
			return lhs ^ rhs;
		}
	};

	template <>
	struct bit_xor<void>
	{
		template <class T, class U>
		constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(mstd::forward<T>(lhs) ^ mstd::forward<U>(rhs))
		{
			return mstd::forward<T>(lhs) ^ mstd::forward<U>(rhs);
		}

		using is_transparend = void;
	};
}
