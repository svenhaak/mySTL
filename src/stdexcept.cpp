#include "stdexcept.hpp"

namespace mstd
{
	logic_error::logic_error(const char* what_arg)
		: message{ what_arg }
	{
	}

	logic_error::logic_error(const std::string& what_arg)
		: message{ what_arg.c_str() }
	{
	}

	const char* logic_error::what() const noexcept
	{
		return message;
	}

	runtime_error::runtime_error(const char* what_arg)
		: message{ what_arg }
	{
	}

	runtime_error::runtime_error(const std::string& what_arg)
		: message{ what_arg.c_str() }
	{
	}

	const char* runtime_error::what() const noexcept
	{
		return message;
	}
}
