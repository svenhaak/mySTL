#pragma once
#include "memory.hpp"
#include "iterator.hpp"
#include <memory>

namespace mstd
{
	/*namespace detail
	{
		template <class T, class void_ptr>
		struct list_node;
		
		template <class T, class void_ptr>
		struct list_node_base
		{
			using node_base_ptr = typename pointer_traits<void_ptr>::template rebind<list_node_base<T, void_ptr>>;
			using node_ptr = typename pointer_traits<void_ptr>::template rebind<list_node<T, void_ptr>>;

			node_ptr next_ = nullptr;
			node_ptr prev_ = nullptr;
		};

		template <class T, class void_ptr>
		struct list_node : list_node_base<T, void_ptr>
		{
			using value_type = T;

			value_type value_;
		};

		template <class T, class void_ptr>
		class list_const_iterator
		{
		private:
			using node_base_ptr = typename pointer_traits<void_ptr>::template rebind<list_node_base<T, void_ptr>>;
			using node_ptr = typename pointer_traits<void_ptr>::template rebind<list_node<T, void_ptr>>;

		public:
			using iterator_category = bidirectional_iterator_tag;
			using value_type = T;
			using pointer = typename pointer_traits<void_ptr>::template rebind<const value_type>;
			using reference = const value_type&;
			using difference_type = std::ptrdiff_t;

			list_const_iterator() noexcept = default;

			list_const_iterator(node_base_ptr p) : current_{ p }
			{
			}

			reference operator*() const noexcept
			{
				return static_cast<node_ptr>(current_)->value_;
			}

			pointer operator->() const noexcept
			{
				return pointer_traits<pointer>::pointer_to(**this);
			}

			list_const_iterator& operator++() noexcept
			{
				current_ = current_->next_;
				return *this;
			}

			list_const_iterator operator++(int) noexcept
			{
				list_const_iterator ret = *this;
				++*this;
				return ret;
			}

			list_const_iterator& operator--() noexcept
			{
				current_ = current_->prev_;
				return *this;
			}

			list_const_iterator operator--(int) noexcept
			{
				list_const_iterator ret = *this;
				--*this;
				return ret;
			}

			bool operator==(const list_const_iterator& rhs) const noexcept
			{
				return pointer_traits<pointer>::pointer_to(**this) == pointer_traits<pointer>::pointer_to(*rhs);
			}

			bool operator!=(const list_const_iterator& rhs) const noexcept
			{
				return !(*this == rhs);
			}
			
		protected:
			node_base_ptr current_;
		};

		template <class T, class void_ptr>
		class list_iterator : public list_const_iterator<T, void_ptr>
		{
		public:
			using iterator_category = bidirectional_iterator_tag;
			using value_type = T;
			using pointer = typename pointer_traits<void_ptr>::template rebind<value_type>;
			using reference = value_type&;
			using difference_type = std::ptrdiff_t;

			using list_const_iterator<T, void_ptr>::list_const_iterator;

			reference operator*() const noexcept
			{
				return static_cast<node_ptr>(current_)->value_;
			}

			pointer operator->() const noexcept
			{
				return pointer_traits<pointer>::pointer_to(**this);
			}

			list_iterator& operator++() noexcept
			{
				current_ = current_->next_;
				return *this;
			}

			list_iterator operator++(int) noexcept
			{
				list_const_iterator ret = *this;
				++*this;
				return ret;
			}

			list_iterator& operator--() noexcept
			{
				current_ = current_->prev_;
				return *this;
			}

			list_iterator operator--(int) noexcept
			{
				list_iterator ret = *this;
				--*this;
				return ret;
			}

			bool operator==(const list_iterator& rhs) const noexcept
			{
				return pointer_traits<pointer>::pointer_to(**this) == pointer_traits<pointer>::pointer_to(*rhs);
			}

			bool operator!=(const list_iterator& rhs) const noexcept
			{
				return !(*this == rhs);
			}
		};
	}

	template <class T, class Alloc = std::allocator<T>>
	class list
	{
	private:
		using void_ptr = typename std::allocator_traits<Alloc>::void_pointer;
		using node_alloc = typename std::allocator_traits<Alloc>::template rebind_alloc<detail::list_node<T, void_ptr>>;
		using node_traits = std::allocator_traits<node_alloc>;
		using node_ptr = typename node_traits::pointer;
		using node_base_ptr = typename pointer_traits<node_ptr>::template rebind<detail::list_node_base<T, void_ptr>>;

	public:
		using value_type = T;
		using allocator_type = Alloc;
		using reference = value_type&;
		using const_reference = const value_type&;
		using pointer = typename std::allocator_traits<Alloc>::pointer;
		using const_pointer = typename std::allocator_traits<Alloc>::const_pointer;
		using iterator = detail::list_iterator<T, void_ptr>;
		using const_iterator = detail::list_const_iterator<T, void_ptr>;
		using reverse_iterator = mstd::reverse_iterator<iterator>;
		using const_reverse_iterator = mstd::reverse_iterator<const_iterator>;
		using difference_type = typename iterator_traits<const_iterator>::difference_type; //todo remove const_
		using size_type = make_unsigned_t<difference_type>;


		





	private:
		node_base_ptr beg_ = nullptr;
		node_base_ptr end_ = nullptr;
		size_type size_ = 0;
		node_alloc alloc_;
	};*/
}
